## THIS IS AN UPGRADED VERSION OF XARA CBS FROM 4.2 TO 5.1

#Steps
1. Copy all packages from the old application to your composer.json
2. run the composer update commmand on your command line
3. Change the relevant files to match your current version of laravel
 - this may also include upgrading or downgrading your php version accordingly
 - to make this easier i opted to use docker
 
## CHECK THAT THE IMPORTED DEPENDENCIES ARE SUPPORTED ON L5
- if you are using [Zizaco/Confide] this package is deprecated and opt to using laravels default [Auth::class] for any authentification functionality

4. Copy your routes to your app/routes.php file, i would recommend starting with the login and signup routes then move from there.
5. Import your models 
6. At the top of your model files add the namespace App; so that laravel can recognize your files
7. After this run [php artisan config:cache] or [composer dump-autoload] and refresh your application on the browser.
8. After everything is working import your UserController, make sure it extends laravels Controller class and add the [namespace App\Http\Controller]
9. import the relevant models 

