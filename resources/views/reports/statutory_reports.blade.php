@extends('layouts.ports')
@section('content')
<br/>
<div class="row">
	<div class="col-lg-12">
  <h3> Statutory Reports</h3>
<hr>
</div>
</div>

<div class="row">
	<div class="col-lg-12">
    <ul>
      <li>
        <a href="{{ URL::to('statutory_reports/license_application_form') }}" target="_blank"> Application form for a license</a>
      </li>
      <li>
        <a href="{{ URL::to('statutory_reports/fitpropertest') }}" target="_blank"> Fit and Proper test</a>
      </li>
      <li>
        <a href="{{ URL::to('statutory_reports/capital_adequacy') }}" target="_blank">Capital adequacy return</a>
      </li>
      <li>
        <a href="{{ URL::to('statutory_reports/liquidity_statement') }}" target="_blank">Liquidity statement</a>
      </li>
      <li>
        <a href="{{ URL::to('statutory_reports/deposit_return') }}" target="_blank">Statement of deposit return</a>
      </li> 
      <li>
        <a href="{{ URL::to('statutory_reports/assets_classification') }}" target="_blank">Risk classification of assets and provisioning</a>
      </li> 
      <li>
        <a href="{{ URL::to('statutory_reports/investment_return') }}" target="_blank">Investment Return</a>
      </li> 
      <li>
        <a href="{{ URL::to('statutory_reports/other_disclosures') }}" target="_blank">Other disclosures</a>
      </li> 
      <!--<li>
        <a href="{{ URL::to('statutory_reports/financial_position') }}" target="_blank">Statement of financial position</a>
      </li> -->
    </ul>
  </div>
</div>

@stop
