@extends('layouts.main')
@section('content')
    <?php use App\Organization; 
    //use App\Branch; 
    $organization = Organization::find(1);
    $installationdate=date('Y-m-d',strtotime($organization->installation_date));

    $splitdate = explode('-', $installationdate);
    //split to obtain month and day from the installation date
    $day=$splitdate[2];
    $month=$splitdate[1];
    $year=date('Y');
    //get the due date for annual subscription fee.
    $date =date('d-F-Y',strtotime($day.'-'.$month.'-'.$year));
    //$date =date('d-F-Y',strtotime('21-01-2020'));
    $todaydate=date('d-F-Y');
    ?>
    <style>
        #alert {
            padding: 20px;
            background-color: #f44336; /* Red */
            color: white;
            margin-bottom: 15px;
        }
        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }
        .main_dashboard {
            text-align: center;
        }

    </style>
    @if($todaydate==$date && Auth::user()->user_type == 'admin')
        <div>
            <div class="panel panel-default" id="alert" >
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                <h4>Please Pay your Annual Subscription Fee whch is due on <b>{{$date}}</b> to continue enjoying the support services<br><b>IGNORE IF ALREADY PAID</b></h4>
            </div>
        </div>

    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="main_dashboard">
                <div class="col-lg-12">
                    @if(Session::get('notice'))
                        <div class="alert alert-info">{{{ Session::get('notice') }}}</div>
                    @endif
                    <div class="panel panel-success">
                        <div class="panel-body table-responsive">
                            <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
                                <thead>
                                <th>#</th>
                                <th>{{ Lang::get('messages.table.number') }}</th>
                                <th>{{ Lang::get('messages.table.name') }}</th>
                                <th>{{ Lang::get('messages.table.branch') }}</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($members as $member)
                                    <tr>
                                        <td> {{ $i }}</td>
                                        <td>{{ $member->membership_no }}</td>
                                        <td>{{ $member->name }}</td>
                                        <td>{{ $member->branch->name }}</td>
                                        <td>
                                            <a href="{{ URL::to('members/summary/'.$member->id) }}"
                                               class="btn btn-info btn-sm">Summary</a>
                                        </td>
                                        <td>
                                            <div class="btn-group pull-right">
                                                <button type="button"
                                                        class="btn btn-info btn-sm dropdown-toggle dropdown-menu-left"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="{{ URL::to('member/savingaccounts/'.$member->id) }}">
                                                            {{{ Lang::get('messages.savings') }}}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{  URL::to('members/loanaccounts/'.$member->id) }}">
                                                            {{{ Lang::get('messages.loans') }}}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ URL::to('sharetransactions/show/'.$member->shareaccount->id) }}">
                                                            {{{ Lang::get('messages.shares') }}}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ URL::to('members/show/'.$member->id) }}">
                                                            {{{ Lang::get('messages.manage') }}}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop