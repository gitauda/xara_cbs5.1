@extends('layouts.ports')
@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3>Members Listing Report</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form target="blank" method="POST" action="{{{ URL::to('reports/listing') }}}" accept-charset="UTF-8">
                <fieldset>
                    <div class="form-group">
                        <label for="type">Select Members <span style="color:red">*</span></label>
                        <select class="form-control" id="type" name="type" required>
                            <option value="all">All</option>
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                        </select>
                    </div>

                    <div class="form-group">
                    <label for="username">Format</label>
                    <select class="form-control" name="format" id="format" required>
                    <option value="">Select format</option>
                    <option value="pdf">PDF</option>
                    <option value="excel">Excel</option>
                    </select>
                </div>
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-primary btn-sm">Select</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop
