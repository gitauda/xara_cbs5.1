@extends('layouts.ports')
@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3> Combined Member Reports</h3>

            <hr>
        </div>
    </div>
  @if ($errors->has())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}<br>        
        @endforeach
    </div>
    @endif
    </div>
    <div class="row">
        <div class="col-lg-8">
            <form method="post" action="{{URL::to('reports/combinedstatement')}}">
                <div class="form-group">
                    <label for="username">Member
                        <select class="form-control" name="member" required>
                            <option value="">-----select member-------</option>
                           <!--<option>-----------------------------------------</option>-->
                            @foreach($members as $member)
                                <option value="{{$member->id}}">{{$member->membership_no}}
                                    &nbsp;&nbsp; {{ ucwords($member->name)}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-primary btn-sm">View Report</button>
                </div>
            </form>
        </div>
    </div>
@stop
