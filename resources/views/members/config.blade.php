@extends('layouts.member')
@section('content')
    <style>
        input{border-radius:3px; outline:none; margin:2px;}
        .subiduration{border-radius:3px; border:none;}
    </style>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3>Member configuration</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(Session::get('notice'))
                <div class="alert alert-success">{{ Session::get('notice') }}</div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div>
            <form method="POST" action="{{ url('member_config') }}" accept-charset="UTF-8" enctype="multipart/form-data">{{csrf_field()}}
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="username">Inactivity duration in months </label>
                    <input class="form-control numbers"  placeholder="months" type="text" name="inactivity_duration" id="" value="{{$inactivity_duration}}">
                    <input type='submit' class='subiduration' name='subiduration' value='update'>
                </div>
            </div>
            </form>
            </div>
        </div>
    </div>

                <!-- Nav tabs -->
                
@stop