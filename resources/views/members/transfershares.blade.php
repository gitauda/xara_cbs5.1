@extends('layouts.main')
@section('content')
<br/>
<h3> Transfer shares  of a member</h3>
<hr>
 <div class="row">
    <div class="col-lg-12">
<hr>
</div>  
</div>

<div class="row">
    <div class="col-lg-4">

    
        
         @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif

         <form method="POST" action="{{{ URL::to('shares/transfer') }}}" accept-charset="UTF-8">



   
    <fieldset>
    <div class="form-group">
                    <label for="username">FROM
                        <select class="form-control" name="member1" required>
                            <option value="">select member from</option>
                            <option>-----------------------------------------</option>
                            @foreach($members as $memberr)
                                <option value="{{$memberr->id}}">{{$memberr->membership_no}}
                                    &nbsp;&nbsp; {{ ucwords($memberr->name)}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
    <div class="form-group">
                    <label for="username">TO:
                        <select class="form-control" name="member2" required>
                            <option value="">select member to </option>
                            <option>-----------------------------------------</option>
                            @foreach($members as $memberr)
                                <option value="{{$memberr->id}}">{{$memberr->membership_no}}
                                    &nbsp;&nbsp; {{ ucwords($memberr->name)}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
        
        
        
        
        

        <div class="form-group">
            <label for="username"> Date</label>
            <div class="right-inner-addon ">
            <i class="glyphicon glyphicon-calendar"></i>
            <input class="form-control datepicker" placeholder="" readonly type="text" name="date" id="date" value="{{{ Input::old('date') }}}" required>
        </div>
        </div>


        <div class="form-group">
            <label for="username"> Amount</label>
            <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount" value="{{{ Input::old('amount') }}}" required>
        </div>


         <div class="form-group">
            <label for="username"> Description</label>
            <textarea class="form-control" name="description">{{{ Input::old('description') }}}</textarea>
            
        </div>



        
      
        
        <div class="form-actions form-group">
        
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        </div>
        

    </fieldset>
</form>
        </div>
        </div>






 @stop