@extends('layouts.savings')
@section('content')
    <br><br>
    <div class="row">
        <div class="col-lg-12">
            SAVINGS TRANSACTIONS MIGRATION
            <hr>
            @if (Session::get('notice'))
                <div class="alert alert-success">{{ Session::get('notice') }}</div>
            @endif
            @if (Session::get('warning'))
                <div class="alert alert-danger">{{ Session::get('warning') }}</div>
            @endif
            @if($errors->any())
                <ul class="alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            @if(!empty(Session::get('umessage')))
                <p class="alert-success">{{ Session::get('umessage') }}</p>
        @endif
        <!-- ############################################################  -->
            <div class="col-lg-12">
                <form method="post" action="{{ url('migration/import/savings') }}" accept-charset="UTF-8"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Upload Transactions (CSV File)</label>
                        <input type="file" name="file" required/>
                    </div>
                    <button type="submit" class="btn btn-primary">Import Transactions</button>
                    &nbsp;
                    <a href="{{ URL::to('templates/savings') }}" class="btn btn-success">Download Template</a>
                </form>
            </div>
            <div class="col-lg-12">
                <hr>
            </div>
        </div>
    </div>
@stop
