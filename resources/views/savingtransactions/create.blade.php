@extends('layouts.main')
@section('content')

<!--<script>
  $(document).ready(function(){
    $('#member').change(function(){
        $.get("{{ url('api/dropdownlist')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#regno').empty(); 
            $('#regno').append("<option>----------------Select REG No.-------------------</option>");
            for (var i = 0; i < data.length; i++) {
            $('#invoice').append("<option value='" + data[i].id +"'>" + data[i].vehicle   + "</option>");
            };
        });
    });
});
</script>-->
<br/>

<?php


function asMoney($value) {
  return number_format($value, 2);
}

?>

<div class="row">
	<div class="col-lg-12">
  <strong>Member: {{ $member->name }}</strong><br>
  <strong>Member #: {{ $member->membership_no }}</strong><br>
<strong>Savings Account #: {{ $savingaccount->account_number }}</strong><br>
<strong>Account Balance #: {{ asMoney($balance) }}</strong><br>
@if($balance > App\Savingtransaction::getWithdrawalCharge($savingaccount))
<strong>Available Balance #: {{ asMoney($balance - App\Savingtransaction::getWithdrawalCharge($savingaccount)) }}</strong><br>
@endif
<hr>
</div>
</div>
<div class="row">
	<div class="col-lg-4">
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        @endif
		 <form method="POST" action="{{{ url('savingtransactions') }}}" accept-charset="UTF-8">{{ csrf_field() }}
    <fieldset>
        <div class="form-group">
            <label for="username">Transaction </label>
           <select name="type" class="form-control" required>
            <option></option>
            <option value="credit"> Deposit</option>
            <option value="debit"> Withdraw</option>
           </select>
        </div>
         <input type="hidden" name="account_id" value="{{ $savingaccount->id}}">
        <div class="form-group">
            <label for="username"> Date</label>
            <div class="right-inner-addon ">
            <i class="fa fa-calendar"></i>
            <input class="form-control datepicker" readonly placeholder="" type="text" name="date" id="date" value="{{{ Input::old('date') }}}" required>
        </div>
        </div>
        <div class="form-group">
            <label for="username"> Saving Amount</label>
            <input class="form-control numbers" placeholder="" type="text" name="saving_amount" id="saving_amount" value="{{{ Input::old('saving_amount') }}}" required>
        </div>
            <div class="form-group">
            <label for="username"> Management Fee</label>
            <input class="form-control numbers" placeholder="" type="text" name="management_fee" id="management_fee" value="{{{ Input::old('management_fee') }}}" required>
        </div>

         <div class="form-group">
                        <label for="username">Vehicle Registration No.</label>
                        <select name="vehicle_reg" class="form-control">
                            <option></option>
                            @foreach($vehicles as $vehicle)
                            <option value="{{$vehicle->id }}"> {{ $vehicle->regno }}:{{ $vehicle->make }}</option>
                            @endforeach

                        </select>

                    </div>

         <div class="form-group">
            <label for="username"> Description</label>
            <textarea class="form-control" name="description">{{{ Input::old('description') }}}</textarea>

        </div>
         <div class="form-group">
            <label for="username"> Bank Reference</label>
            <textarea class="form-control" name="bank_reference">{{{ Input::old('bank_reference') }}}</textarea>

        </div>

        <div class="form-group">
            <label for="username">Transaction Method </label>
           <select name="pay_method" class="form-control" required>
            <option value="bank"> Bank</option>
            <option value="cash"> Cash</option>
           </select>
        </div>


         <div class="form-group">
            <label for="username"> Transacted by</label>
             <input class="form-control" placeholder="" type="text" name="transacted_by" id="transacted_by" value="{{{ Auth::user()->username }}}" readonly='readonly' required>

        </div>
        <div class="form-actions form-group">

          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        </div>

    </fieldset>
</form>


  </div>

</div>
@stop
