@include('includes.head')
<?php use App\Organization;
 $organization = Organization::find(1); 
 //echo "<pre>"; print_r($organization); "</pre>"; die();
 ?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-body">
                    <div style="height: auto; overflow: hidden; text-align: center;">
                        <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="logo"
                             style="height: 100px; margin: 10px 0;">
                    </div>
                    <form role="form" method="POST" action='/users/login'>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <fieldset>
                            {{-- TODO ADD i8n feature --}}
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input class="form-control" tabindex="1" placeholder="Enter your Email" type="text" name="email" id="email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                            <label for="password">
                                Password
                            </label>
                            <input class="form-control" tabindex="2" placeholder="Enter your password" type="password" name="password" id="password">
                            <p class="help-block">
                                <a href="/users/forgot_password">Forgot password?</a>
                            </p>
                            </div>
                            <div class="checkbox">
                                <label for="remember">
                                    <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> remember me
                                </label>
                            </div>
                            @if (Session::get('error'))
                                <div class="alert alert-error alert-danger">{{ Session::get('error') }}</div>
                            @endif
                    
                            @if (Session::get('notice'))
                                <div class="alert">{{ Session::get('notice') }}</div>
                            @endif
                            <div class="form-group">
                                <button tabindex="3" type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </fieldset>
                    </form>
                    {{-- {{ Confide::makeLoginForm()->render() }} --}}
                </div>
            </div>
        </div>
    </div>
</div>