@extends('layouts.loans')
@section('content')
    <br><br>
    <div class="row">
        <div class="col-lg-12">
            LOAN REPAYMENTS MIGRATION
            <hr>
            @if (Session::get('notice'))
                <div class="alert alert-success">{{ Session::get('notice') }}</div>
            @endif
            @if (Session::get('warning'))
                <div class="alert alert-danger">{{ Session::get('warning') }}</div>
        @endif
        <!-- ############################################################  -->
            <div class="col-lg-12">
                <p><strong>Import Loan Repayments </strong></p>
                <p>&nbsp;</p>
                <form method="post" action="{{URL::to('import_repayments')}}" accept-charset="UTF-8"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Upload Repayment (Excel Sheet)</label>
                        <input type="file" class="" name="repayments" value="{{asset('/Excel/banks.xls')}}" required/>
                    </div>
                    <button type="submit" class="btn btn-primary">Import Repayments</button>
                    &nbsp;
                    <a href="{{ URL::to('repayments_template') }}" class="btn btn-success">Download Template</a>
                </form>
            </div>
            <div class="col-lg-12">
                <hr>
            </div>
        </div>
    </div>
@stop
