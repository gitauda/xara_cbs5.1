@extends('layouts.member')
@section('content')
<br/>
<?php
function asMoney($value) {
  return number_format($value, 2);
}
?>
<style>
  .hide{display:none;}
</style>
<div class="row">
  <div class="col-lg-5">
  @if(Session::has('none'))
        <div class="alert alert-warning alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>{{{ Session::get('none') }}}</strong>
      </div>
   @endif
   </div>
	<div class="col-lg-12">
  <h3>Loan Payment</h3>
  <hr>
</div>
</div>
<div class="row">
	<div class="col-lg-5">
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        @endif
       <table class="table table-condensed table-bordered">
        <tr>
          <td>Member</td><td>{{$loanaccount->member->name}}</td>
        </tr>
        <tr>
          <td>Loan Account</td><td>{{$loanaccount->account_number}}</td>
        </tr>
        <tr>
          <td>Loan Amount</td><td>{{ asMoney($loanaccount->amount_disbursed + $interest) }}</td>
        </tr>
        <tr>
          <td>Loan Balance</td><td>{{ asMoney($loanbalance) }}</td>
        </tr>
       </table>
		 <form method="POST" action="{{{ URL::to('loanrepayments') }}}" accept-charset="UTF-8">
    <fieldset>
       <table class="table table-condensed table-bordered">
        <tr>
          <td>Principal Due</td><td>{{ asMoney($principal_due) }}</td>
        </tr>
        <tr>
          <td>Interest Due</td><td>{{ asMoney($interest_due) }}</td>
        </tr>
          <td>Installment</td><td>{{ asMoney($principal_due + $interest_due)}}</td>
        </tr>
        </table>
        <table class="table table-condensed table-bordered">
        <tr>
          <td>Installment</td><td>{{ asMoney($principal_due + $interest_due)}}</td>
        </tr>
       
          <?php
            $install=$principal_due + $interest_due;
            $ext=Loantransaction::getLoanExtra($loanaccount); $tdclass="";
            $arrears=Loantransaction::getExtraAmount($loanaccount,'arrears');
            $overpayments=Loantransaction::getExtraAmount($loanaccount,'overpayments');
            $amount_unpaid=Loantransaction::getAmountUnpaid($loanaccount); $unpaid_hide='hide';
            if($ext=='arrears'){
              $amount_due=(float)$install+(float)$arrears+(float)$amount_unpaid; $extra_amount=$arrears; $extra_name='arrears';
            }else if($ext='over_payment'){
              if($overpayments>=$amount_unpaid){$overpayments=$overpayments-$amount_unpaid;
                $amount_due=(float)$install-(float)$overpayments; $extra_amount=$overpayments; $extra_name='over_payment';
              }else{
                $arrears=(float)$amount_unpaid-(float)$overpayments; $overpayments=0;
                $amount_due=(float)$install+$arrears; $extra_amount=$arrears; $extra_name='arrears';
              } 
            }else{
              $amount_due=$install+(float)$amount_unpaid; $unpaid_hide=''; $tdclass='hide'; $extra_name='extra amount'; $extra_amount=0;
            }
            if((float)$amount_unpaid<=0){$unpaid_hide='hide';} if($amount_due<1){$amount_due=0;}
          ?>
        <tr class={{$tdclass}}>
          <td>{{$extra_name}}</td><td>{{ asMoney($extra_amount) }}</td>
        </tr>
        <tr class={{$unpaid_hide}}>
          <td>Amount unpaid</td><td>{{ asMoney($amount_unpaid) }}</td>
        </tr>
        <tr>
          <td>Amount Due</td><td>{{ asMoney($amount_due)}}</td>
        </tr>
        </table>
        <input class="form-control" placeholder="" type="hidden" name="loanaccount_id" id="loanaccount_id" value="{{ $loanaccount->id }}">
         <div class="form-group">
            <label for="username">Repayment Date <span style="color:red">*</span></label>
            <div class="right-inner-addon ">
            <i class="fa fa-calendar"></i>
            <input required class="form-control " readonly="readonly" placeholder="" type="text" name="date" id="date" value="{{{date('Y-m-d')}}}">
        </div>
       </div>
       <!--BEGIN VERBOTEN datepicker-->
       <input type="hidden" name="principal" value="{{$principal_due}}">
       <input type="hidden" name="interest" value="{{$interest_due}}">
       <!--END VERBOTEN-->
        <div class="form-group">
            <label for="username">Amount</label>
            <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount"
            value="{{{ Input::old('date') }}}">
        </div>
        <div class="form-actions form-group">
          <button type="submit" class="btn btn-primary btn-sm">Submit Payment</button>
        </div>
    </fieldset>
</form>
</div>
</div>
@stop
