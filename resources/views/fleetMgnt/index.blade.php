@extends('layouts.fleet')
@section('content')
<br/>
<div class="row">
	<div class="col-lg-12">
    <h3></h3>
    <hr>
  </div>	
</div>

<div class="row">
	<div class="col-lg-12">

    <div class="panel panel-default">
      <div class="panel-heading">
         <h3>Fuel consumption</h3>
        </div>
        <div class="panel-body">
        <div class="chart-container" style="position: relative; height:auto; width:100%;" >     
            <canvas id="chart" style="width: auto; height: 65vh; background: #222; border: 1px solid #555652; margin-top: 10px;"></canvas>
          </div>
            <?php $data1="2,4,6,8,9,11,20"; $data2="16,18,19,21,13,30,23";
            $data3="'Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange','Indigo'";
            ?>
        </div>
      </div>
    </div>
</div>

<script>
				var ctx = document.getElementById("chart").getContext('2d');
    			var myChart = new Chart(ctx, {
        		type: 'bar',
		        data: {
		            labels: [<?php echo $data3; ?>],
		            datasets:
		            [{
		                label: 'Data 1',
		                data: [<?php echo $data1; ?>],
		                backgroundColor: 'transparent',
		                borderColor:'rgba(255,99,132)',
		                borderWidth: 3
		            },

		            {
		            	  label: 'Data 2',
		                data: [<?php echo $data2; ?>],
		                backgroundColor: 'transparent',
		                borderColor:'rgba(0,255,255)',
		                borderWidth: 3
		            }]
		        },
            
		        options: {
		            scales: {scales:{yAxes: [{beginAtZero: false}], xAxes: [{autoskip: true, maxTicketsLimit: 20}]}},
		            tooltips:{mode: 'index'},
		            legend:{display: true, position: 'top', labels: {fontColor: 'rgb(255,255,255)', fontSize: 16}}
		        }
		      });
			</script>
	    
 

@stop