@extends('layouts.fleet')
@section('content')
<br/>

<div class="row">
	<div class="col-lg-12">
  <h3>New collector</h3>

<hr>
</div>	
</div>


<div class="row">
	<div class="col-lg-5">

    
		
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif


       

    <form method="POST" action="{{{ URL::to('fleetCashCollectors') }}}" accept-charset="UTF-8">

        <fieldset>

        <div class="form-group">
                <label for="username">Collector</label>
                <input class="form-control" placeholder="" type="text" name="collector" id="collector2" value="" required>
            </div>

            <div class="form-group" >
                <label for="date">Registration date <span style="color:red"></span></label>
                <div class="right-inner-addon " >
                    <input class="form-control"  placeholder="" type="date" name="reg_date" id="date"  value="{{date('Y-m-d')}}" required>
                </div>
            </div>

            <div class="form-actions form-group">
                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            </div>

        </fieldset>

    </form>
		
  </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        /*$('#vehicleInpu').change(function(){
            $.get("{{ url('ajaxCollector')}}", 
                { option: $(this).val() },
                function(data){
                    $('#collector2').val(data[1]); 
                    $('#collector1').val(data[0]);
                });
        });*/
   });
</script>
@stop