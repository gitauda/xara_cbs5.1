@extends('layouts.fleet')
@section('content')
<br/>
<div class="row">
	<div class="col-lg-12">
  <h3>Cash Collection</h3> 
    <hr>
    @if(Session::has('success'))
      <div class="alert alert-success">
          {{ Session::get('success'); }}
      </div>
    @endif
  </div>	
</div>

<div class="row">
	<div class="col-lg-12">

    <div class="panel panel-default">
        <div class="panel-heading">         
            <a class="btn btn-info btn-sm" href="{{ URL::to('fleetCashCollection/create')}}">Add collection</a>
        </div>
        <div class="panel-body">
    <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Vehicle</th>
        <th>Collector</th>
        <th>Amount</th>
        <th>Date</th>
        <th></th>
      </thead>
      <tbody>

        <?php $i = 1; ?>
            @foreach($collections as $collection)
        <tr>
          <td> {{ $i }}</td>
          <td>{{ $collection->vehicle->make." ( ".$collection->vehicle->regno." )" }}</td>
          <td>{{ $collection->collector->name }}</td>
          <td>{{ $collection->amount }}</td>
          <td>{{ $collection->date }}</td>
          <td>
            <div class="btn-group">
              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  Action <span class="caret"></span></button>
                  <ul class="dropdown-menu" role="menu">
                      <li><a href="{{URL::to('fleetCashCollection/'.$collection->id.'/edit')}}">Update</a></li>
                      <li><a href="{{URL::to('fleetCashCollection/delete/'.$collection->id)}}">Delete</a></li>
                  </ul>
            </div>
          </td> 
        </tr>

        <?php $i++; ?>
        @endforeach

      </tbody>
    </table>
  </div>
	    
  </div>
</div>

@stop