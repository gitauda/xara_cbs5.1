@extends('layouts.fleet')
@section('content')
<br/>
<div class="row">
	<div class="col-lg-12">
  <h3>Maintenance Records</h3> 
    <hr>
    @if(Session::has('success'))
      <div class="alert alert-success">
          {{ Session::get('success'); }}
      </div>
    @endif
    @if(Session::has('failure'))
      <div class="alert alert-danger">
          {{ Session::get('failure'); }}
      </div>
    @endif
  </div>	
</div>

<div class="row">
	<div class="col-lg-12">

    <div class="panel panel-default">
        <div class="panel-heading">         
            <a class="btn btn-info btn-sm" href="{{ URL::to('fleetMaintRecords/create')}}">Add record</a>
        </div>
        <div class="panel-body">
    <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Garage</th>
        <th>Mechanic</th>
        <th>Vehicle</th>
        <th>Description</th>
        <th>Cost (Kshs)</th>
        <th>Date</th>
        <th></th>
      </thead>
      <tbody>

        <?php $i = 1; ?>
            @foreach($maintRecords as $record)
        <tr>
          <td> {{ $i }}</td>
          <td>{{ $record->garage }}</td>
          <td>{{ $record->mechanic }}</td>
          <td>{{ $record->vehicle->make." ( ".$record->vehicle->regno." )" }}</td>
          <td>{{ nl2br($record->description) }}</td>
          <td>{{ $record->cost }}</td>
          <td>{{ $record->date }}</td>
          <td>
            <div class="btn-group">
              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  Action <span class="caret"></span></button>
                  <ul class="dropdown-menu" role="menu">
                      <li><a href="{{URL::to('fleetMaintRecords/'.$record->id.'/edit')}}">Update</a></li>
                      <li><a href="{{URL::to('fleetMaintRecords/delete/'.$record->id)}}">Delete</a></li>
                  </ul>
            </div>
          </td> 
        </tr>

        <?php $i++; ?>
        @endforeach

      </tbody>
    </table>
  </div>
	    
  </div>
</div>

@stop