@extends('layouts.fleet')
@section('content')
<br/>

<div class="row">
	<div class="col-lg-12">
  <h3>New Record</h3>

<hr>
</div>	
</div>


<div class="row">
	<div class="col-lg-5">

    
		
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif
        @if(Session::has('failure'))
      <div class="alert alert-danger">
          {{ Session::get('failure'); }}
      </div>
    @endif

       

    <form method="POST" action="{{{ URL::to('fleetMaintRecords') }}}" accept-charset="UTF-8">

        <fieldset>

            <div class="form-group">
                <label for="garage">Garage</label>
                <input class="form-control" placeholder="" type="text" name="garage" id="garage" value="" required>
            </div>
            <div class="form-group">
                <label for="mechanic">Mechanic</label>
                <input class="form-control" placeholder="" type="text" name="mechanic" id="mechanic" value="" required>
            </div>
            <div class="form-group">
                <label for="vehicle">Vehicle:</label>
                <select class="form-control" type="hidden" name="vehicle_id" id="vehicleInpu" required>
                    @foreach($vehicles as $vehicle) 
                        <option value="{{$vehicle->id}}">{{$vehicle->make." ( ".$vehicle->regno." )"}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" placeholder="Give more info about the maintenance." name="description" id="description" value="" required></textarea>
            </div>
            <div class="form-group">
                <label for="cost">Cost</label>
                <input class="form-control" placeholder="" type="number" name="cost" id="cost" value="" required>
            </div>
            <div class="form-group" >
                <label for="maint_date">Maintenance date</label>
                <div class="right-inner-addon">
                    <input class="form-control"  placeholder="" type="date" name="maint_date" id="maint_date"  value="{{date('Y-m-d')}}" required>
                </div>
            </div>

            <div class="form-actions form-group">
                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            </div>

        </fieldset>

    </form>
		
  </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        /*$('#vehicleInpu').change(function(){
            $.get("{{ url('ajaxCollector')}}", 
                { option: $(this).val() },
                function(data){
                    $('#collector2').val(data[1]); 
                    $('#collector1').val(data[0]);
                });
        });*/
   });
</script>
@stop