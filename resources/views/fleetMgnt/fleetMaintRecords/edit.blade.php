@extends('layouts.fleet')
@section('content')
<br/> 

<div class="row">
	<div class="col-lg-12">
        <h3>Update record {{$record->code}}</h3>
        <hr>
    </div>	
</div>

<div class="row">
	<div class="col-lg-5">
		
		@if ($errors->has())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br>    
                @endforeach
            </div>
        @endif

    <form method="POST" action="{{{ URL::to('fleetMaintRecords/update/'.$record->id) }}}" accept-charset="UTF-8">
        <fieldset>

            <div class="form-group">
                <label for="garage">Garage</label>
                <input class="form-control" placeholder="" type="text" name="garage" id="garage" value="{{$record->garage}}" required>
            </div>
            <div class="form-group">
                <label for="mechanic">Mechanic</label>
                <input class="form-control" placeholder="" type="text" name="mechanic" id="mechanic" value="{{$record->mechanic}}" required>
            </div>
            <div class="form-group">
                <label for="username">Vehicle:</label>
                <select class="form-control" type="hidden" name="vehicle_id" id="vehicleInpu" required>
                    <option value="{{$record->vehicle_id}}">{{$record->vehicle->make." ( ".$record->vehicle->regno." )"}}</option>
                    @foreach($vehicles as $vehicle) 
                        <option value="{{$vehicle->id}}">{{$vehicle->make." ( ".$vehicle->regno." )"}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control"  name="description" id="description" value="" required>{{$record->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="cost">Cost</label>
                <input class="form-control" placeholder="" type="number" name="cost" id="cost" value="{{$record->cost}}" required>
            </div>
            <div class="form-group" >
                <label for="maint_date">Maintenance date</label>
                <div class="right-inner-addon">
                    <input class="form-control"  placeholder="" type="date" name="maint_date" id="maint_date"  value="{{$record->date}}" required>
                </div>
            </div>
            <div class="form-actions form-group">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>

        </fieldset>

    </form>
		
  </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        /*$('#vehicleInpu').change(function(){
            $.get("{{ url('ajaxCollector')}}", 
                { option: $(this).val() },
                function(data){
                    $('#collector2').val(data[1]); 
                    $('#collector1').val(data[0]);
                });
        });*/
   });
</script>
@stop