@extends('layouts.fleet')
@section('content')
<br/>

<div class="row">
	<div class="col-lg-12">
  <h4>Collector {{$collector->name}}</h4>

<hr>
</div>	
</div>

<div class="row">
	<div class="col-lg-5">
		
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif

    <form method="POST" action="{{{ URL::to('fleetCashCollectors/update/'.$collector->id) }}}" accept-charset="UTF-8">
        <fieldset>

            <div class="form-group">
                <label for="username">Vehicle:</label>
                <select class="form-control" type="hidden" name="vehicle_id" id="vehicleInpu" required>
                    <option value="{{$collector->vehicle->id}}">{{$collector->vehicle->make." ( ".$collector->vehicle->regno." )"}}</option>
                    <?php $i=0; $col_id=1; $col_name=0; ?>
                    @foreach($vehicles as $vehicle) <?php $i++; ?>
                        <option value="{{$vehicle->id}}">{{$vehicle->make." ( ".$vehicle->regno." )"}}</option>
                        @if($i==1) <?php  $col_id=$vehicle->cash_collector; $col=FleetCashCollector::find($col_id); $col_name=$vehicle->collector->name; ?> @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="username">Cash collector</label>
                <input class="form-control" placeholder="" type="text" name="col" readonly id="collector2" value="{{$collector->name}}" required>
                <input class="form-control" placeholder="" type="hidden" name="collector" id="collector1" value="{{$collector->id}}" required>
            </div>

            <!--<div class="form-group">
                <label for="username">Amount</label>
                <input class="form-control" placeholder="" type="text" name="amount" id="amount" value="{{$collector->id}}" required>
            </div>-->

            <div class="form-group" >
                <label for="date">Date <span style="color:red"></span></label>
                <div class="right-inner-addon " >
                    <input class="form-control"  placeholder="" type="date" name="date" id="date"  value="{{date('Y-m-d')}}" required>
                </div>
            </div>

            <div class="form-actions form-group">
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>

        </fieldset>

    </form>
		
  </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#vehicleInpu').change(function(){
            $.get("{{ url('ajaxCollector')}}", 
                { option: $(this).val() },
                function(data){
                    $('#collector2').val(data[1]); 
                    $('#collector1').val(data[0]);
                });
        });
   });
</script>
@stop