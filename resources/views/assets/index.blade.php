<?php
	function asMoney($value){
		return number_format($value, 2);
	}
?>
@extends('layouts.assets')
@section('content')
<style>
	.form-group{padding:4px;}
	.toprow{border-bottom:1px solid; margin-bottom:5px; padding:6px;}
	.toprow span{font-size:19px; font-weight:; font-family:Times;}
</style>
<div class="toprow">
		<span><font color="grey">Assets</font></span>
</div>
@if (\Session::has('success'))
    <div class="row alert alert-success">
            {{Session::get('success')}}
    </div>
@endif
@if (Session::has('flash_message'))
	<div class="alert alert-success">
		{{ Session::get('flash_message') }} 
	</div>
@endif
<div class="right" style="float:right">
	<a href="{{ URL::to('assetpdf') }}" class="btn btn-info btn-sm"><i class="fa fa-plus fa-fw"></i> Generate report</a>
	<hr>
</div>

<div class="row">
	<!-- QUICK LINK BUTTONS -->
	<div class="col-lg-12">
		<a href="{{ URL::to('assets/create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus fa-fw"></i> New Asset</a>&emsp;
		
		<hr>
	</div><!-- ./END -->

	<!-- FIXED ASSETS BODY SECTION -->
	<div class="col-lg-12">
		<!-- TAB LINKS -->
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#registeredAssets">Active ({{ Asset::where('status','<>','Disposed')->where('purchase_approved','=',1)->count() }})</a></li>
			<li><a data-toggle="tab" href="#soldDisposedAssets">Sold & Disposed ({{ Asset::where('status', 'Disposed')->where('purchase_approved','=',1)->count() }})</a></li>
		</ul>

		<!-- TAB CONTENT -->
		<div class="tab-content">
			<!-- REGISTERED ASSETS -->
			<div id="registeredAssets" class="tab-pane fade in active">
				<table class="table table-condensed table-bordered table-responsive table-hover users">
					<thead>
						<tr>
							<th>#</th>
							<th>Asset Name</th>
							<th>Station</th>
							<th>Asset Number</th>
							<th>Quantity</th>
							<th>Purchase Date</th> 
							<th>Purchase Price</th>
							<th>Last Depreciated</th>
							<th>Book Value</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $count=1; //<a href="{{ URL::to('assets/dispose/'.$asset->id.'/dispose') }}"> ?>
						@if(count($assets) > 0)
						@foreach($assets as $asset)
						@if($asset->status != 'Disposed' && $asset->purchase_approved==1)
						<?php $member=Member::findorFail($asset->member_id); $branch=Member::findorFail($asset->branch_id); ?>
						<tr>
							<td>{{ $count }}</td>
							<td>{{ $asset->asset_name }}</td>
							<td>{{ $branch->name }}</td>
							<td>{{ $asset->asset_number }}</td>
							<td>{{$asset->quantity}}</td>
							<td>{{ date('jS M, Y', strtotime($asset->purchase_date)) }}</td>
							<td>{{ asMoney($asset->purchase_price) }}</td>

							@if($asset->last_depreciated == '0000-00-00')
								<td>Never</td>
							@else
								<td>{{ date('jS M, Y', strtotime($asset->last_depreciated)) }}</td>
							@endif

							<td>{{ asMoney($asset->book_value) }}</td>

							<td>
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										Action <i class="fa fa-caret-down fa-fw"></i>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ URL::to('assets/'.$asset->id) }}">View</a></li>
										@if(Confide::user()->user_type == 'admin')
											<li><a href="{{ URL::to('assets/'.$asset->id.'/edit') }}">Edit</a></li> 
										@endif
										<li class='disposeli' data-toggle="modal" data-target="#disposeModal" lang='{{$asset->id}}'> <a href='#'>Dispose</a></li> 
										<!--<li><a href="{{ URL::to('assets/delete/'.$asset->id) }}" onclick="return (confirm('Are you sure you want to delete this item?'))">Delete</a></li> -->
									</ul>
								</div>
							</td>
						</tr>
						<?php $count++; ?>

						@endif
						@endforeach
						@endif

					</tbody>
				</table>
			</div><!-- ./End of registered assets -->
			<div id="disposeModal" class="modal fade" role="dialog">
  				<div class="modal-dialog"><div class="modal-content">  
				  <div class="modal-body">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <form class="form-inline disposeform" role="form" action="" method="POST">
						<div class="form-group">
							<label>Price for each: </label><br>
							<input type="number" class="form-control input-sm" name="price" placeholder="" style="width: 300px" required>
						</div><br>
						<div class="form-group">
							<label>Quantity: </label><br> <?php $requester=Confide::user()->id; ?>
							<input type="number" class="form-control input-sm" name="quantity" placeholder="" style="width: 300px" required>
							<input type="hidden" class="form-control input-sm" name="requester" value={{$requester}} required>
						</div><br>
						<div class="form-group">
							<input type="submit" class="btn btn-primary btn-sm" name="btnSubmit" value="Request approval">
						</div>
					</form>
					</div>
				  </div></div>
			</div>
			<!-- SOLD/DISPOSED ASSETS -->
			<div id="soldDisposedAssets" class="tab-pane fade in">
				<!-- SOLD/DISPOSED ASSETS -->
				<table class="table table-condensed table-bordered table-responsive table-hover users">
					<thead>
						<tr>
							<th>#</th>
							<th>Asset Name</th>
							<th>Asset Number</th>
							<th>Purchase Date</th>
							<th>Purchase Price</th>
							<th>Status</th>
							<th>Book Value</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $count=1; ?>
						@if(count($assets) > 0)
						@foreach($assets as $asset)
						@if($asset->status == 'Disposed' && $asset->purchase_approved==1)

						<tr>
							<td>{{ $count }}</td>
							<td>{{ $asset->asset_name }}</td>
							<td>{{ $asset->asset_number }}</td>
							<td>{{ date('jS M, Y', strtotime($asset->purchase_date)) }}</td>
							<td>{{ asMoney($asset->purchase_price) }}</td>
							<td>{{ $asset->status }}</td>
							<td>{{ asMoney($asset->book_value) }}</td>
							<td>
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										Action <i class="fa fa-caret-down fa-fw"></i>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ URL::to('assets/'.$asset->id) }}">View</a></li>
										@if(Confide::user()->user_type == 'admin')
											<!--<li><a href="{{ URL::to('assets/'.$asset->id.'/edit') }}">Edit</a></li>-->
										@endif
										<!--<li><a href="{{ URL::to('assets/dispose/'.$asset->id.'/undispose')}}">Undispose</a></li>
										<li><a href="{{ URL::to('assets/delete/'.$asset->id) }}" onclick="return (confirm('Are you sure you want to delete this item?'))">Delete</a></li>-->
									</ul>
								</div>
							</td>
						</tr>
						<?php $count++; ?>

						@endif
						@endforeach
						@endif

					</tbody>
				</table>
			</div><!-- ./End of disposed assets -->
		</div>
		<!-- ./End of tab cotent -->

	</div><!-- ./End of body section -->

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.disposeli').on('click', function(){
			var id=$(this).attr('lang'); 
			var link="assets/disposal_request/"+id+"/dispose"; 
			$('.disposeform').attr('action',link);
		});
	});
</script>
@stop
