<?php
	function asMoney($value){
		return number_format($value, 2);
	}
?>
@extends('layouts.assets')
@section('content')
<style type="text/css" media="screen">
	.toprow{border-bottom:1px solid; margin-bottom:5px; padding:6px;}
	.toprow span{font-size:18px; font-weight:; font-family:Times;}
</style>

<div class="toprow">
		<span><font color="grey">Allocated assets</font></span>
</div>

<div class="row">
	<!-- QUICK LINK BUTTONS -->
	<div class="col-lg-12">
		<a href="{{ URL::to('assets_allocation/create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus fa-fw"></i> Allocate</a>&emsp;
		<hr>
	</div><!-- ./END -->

	<!-- FIXED ASSETS BODY SECTION -->
	<div class="col-lg-12">
		<!-- TAB LINKS -->
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#registeredAssets">Registered ({{ Asset::where('status','<>','Disposed')->where('purchase_approved','=',1)->count() }})</a></li>
		    <!-- <li><a data-toggle="tab" href="#soldDisposedAssets">Sold & Disposed ({{ Asset::where('status', 'Disposed')->count() }})</a></li>-->
		</ul>

		<!-- TAB CONTENT -->
		<div class="tab-content">
			<!-- REGISTERED ASSETS -->
			<div id="registeredAssets" class="tab-pane fade in active">
				<table class="table table-condensed table-bordered table-responsive table-hover users">
					<thead>
						<tr>
							<th>#</th>
							<th>Asset Name</th>
							<th>Member allocated</th>
							<th>Amount</th>
                            <th>Purchase price</th>
							<th>Date allocated</th>
							<th>Date of submission</th>
							<th>Book value</th>
						</tr>
					</thead>
					<tbody>
						<?php $count=1; ?>
						@if(count($allocations) > 0)
						@foreach($allocations as $allocation)
                            <?php $member=Member::findorFail($allocation->member_id); 
                                    $asset=Asset::findorFail($allocation->asset);
                            ?>
						<tr>
							<td>{{ $count }}</td>
							<td>{{ $asset->asset_name }}</td>
							<td>{{ $member->name }}</td>
							<td>{{ $allocation->amount }}</td>
                            <td>{{ asMoney($asset->purchase_price) }}</td>
							<td>{{$allocation->date_allocated}}</td>
							<td>{{$allocation->submission_date}}</td>
							<td>{{ asMoney($asset->book_value) }}</td>
							<td>
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										Action <i class="fa fa-caret-down fa-fw"></i>
									</button>
									<ul class="dropdown-menu" role="menu">
										<!-- <li><a href="{{ URL::to('assets/'.$asset->id) }}">View</a></li>
										<li><a href="{{ URL::to('assets/'.$asset->id.'/edit') }}">Edit</a></li>-->
										<li><a href="{{ URL::to('assets_allocation/disallocate/'.$allocation->id) }}">Disallocate</a></li>
										<!--<li><a href="{{ URL::to('assets/delete/'.$asset->id) }}" onclick="return (confirm('Are you sure you want to delete this item?'))">Delete</a></li>-->
									</ul>
								</div>
							</td>
						</tr>
						<?php $count++; ?>
						@endforeach
						@endif

					</tbody>
				</table>
			</div><!-- ./End of registered assets -->

			<!-- SOLD/DISPOSED ASSETS -->
			<div id="soldDisposedAssets" class="tab-pane fade in">
				<!-- SOLD/DISPOSED ASSETS -->
				<table class="table table-condensed table-bordered table-responsive table-hover users">
					
				</table>
			</div><!-- ./End of disposed assets -->
		</div><!-- ./End of tab cotent -->

	</div><!-- ./End of body section -->

</div>

@stop
