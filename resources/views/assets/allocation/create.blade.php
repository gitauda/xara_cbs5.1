<?php
	function asMoney($value){
		return number_format($value, 2);
	}
	$date=date('Y-m-d');
?>

@extends('layouts.assets')
@section('content')

<style type="text/css" media="screen">
	//hr{ border-color: #fff !important; }
	select,input[type=text],input[type=number],input[type=date]{
		width:250px; padding:5px;  
	}
	.form-group{padding:5px; box-sizing:border-box;} h4{padding:5px 2px; }
</style>

<div class="row">
	<div class="col-lg-12">
		<form class="form-inline" role="form" action="{{ URL::to('assets_allocation') }}" method="POST">
			<h4 style="color: black">Asset allocation</h4>
			<div class="form-group">
				<label>Asset: </label><br>
				<select class=" asset_select" name="asset" id="station" style="" required>
					<option> </option>
					@foreach($assets as $asset)
                        <?php 
                            $allocs=AssetsAllocation::where('asset',$asset->id); $amou=0; 
                            if(count($allocs)>0){ $amou=0;
                                foreach($allocs as $alloc){
									$amou+=(int)$alloc->amount;
								}
							}
							$amountdiff=(int)$asset->quantity-$amou;
                        ?>
						@if($asset->quantity>$amou)
							<option value="{{$asset->id}}">{{$asset->asset_name}}</option>
						@endif
				    @endforeach
				</select>
			</div>&emsp;

			<div class="form-group">
				<label>Allocate to: </label><br>
				<select class="" name="member" id="station" style="" required>
					<option></option>
					@foreach($members as $member)
						<option value="{{$member->id}}">{{$member->name}}</option>
				    @endforeach
				</select>
			</div>
			<div class="form-group">
				<label>Quantity: </label><br>
				<input type="number" class="amou_select" name="quantity"  max="" min=0 style="" required>
			</div>
			<div class="form-group">
				<label>Submission date: </label><br>
				<input type="date"  class="form-control" name="sub_date" value={{$date}} style="" required>
			</div><br>
			<div class="col-lg-12 form-group text-left">
				<input type="submit" class="btn btn-primary btn-sm" name="btnSubmit" value="Allocate">
			</div><br><hr>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#rateRadio').on('click', function(){
			$('#rateRadio').prop('checked', true);
			$('#lifeYears').prop('checked', false);
			$('#rate').prop('disabled', false);
			$('#lifeYears').prop('disabled', true);
		});

		$('#rateYears').on('click', function(){
			$('#rateYears').prop('checked', true);
			$('#rateRadio').prop('checked', false);
			$('#lifeYears').prop('disabled', false);
			$('#rate').prop('disabled', true);
		});

		$('.asset_select').change(function(){
                var lpvalue=$(this).val(); var hii=$(this);
                $.get("{{ url('ajaxmaxassetamount')}}",
                    {lpvalue:lpvalue},
                    function(data){
						$('.amou_select').attr('max',parseInt(data));
                    }); alert(data);
            });
		$('.amou_select').keyup(function(){
			var max=$('.amou_select').attr('max'); var val=$(this).val();
			var asset=$('.asset_select').val();  if(asset==""){$('.amou_select').val(0);}
			if(parseInt(val)>parseInt(max)){$('.amou_select').val(parseInt(max));}
			if(parseInt(val)<0){$('.amou_select').val(1);}
		});	
	});
</script>

@stop
