 <nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="{{ URL::to('loans') }}">
                        <i class="fa fa-pencil"></i>
                         Loan Applications
                     </a>
                </li>
            </ul>
            <!-- /#side-menu -->
        </div>
        <!-- /.sidebar-collapse -->
</nav>
<!-- /.navbar-static-side -->
