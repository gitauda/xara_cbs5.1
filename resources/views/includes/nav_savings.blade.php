 <nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{ URL::to('savingproducts') }}"><i class="fa fa-tags fa-fw"></i> Saving Products</a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->
