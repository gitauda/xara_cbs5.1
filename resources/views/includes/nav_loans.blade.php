 <nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{ URL::to('disbursements') }}">
                            <i class="fa fa-random"></i>
                            Disbursement Options
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('matrices') }}">
                            <i class="fa fa-gavel"></i>
                            Guarantor Matrix
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('loanproducts') }}"><i class="fa fa-tags fa-fw"></i> Loan Products</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('loans') }}"><i class="fa fa-pencil fa-fw"></i> Loan Applications</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('import_repayments') }}"><i class="fa fa-download"></i> Import Repayments</a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->
