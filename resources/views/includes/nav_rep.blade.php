 <nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{ URL::to('reports') }}"><i class="fa fa-file fa-fw"></i> Member Reports</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('sharereports') }}"><i class="fa fa-file fa-fw"></i> Share Reports</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('savingreports') }}"><i class="fa fa-file fa-fw"></i> Saving Reports</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('loanreports') }}"><i class="fa fa-file fa-fw"></i> Loan Reports</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('loanrepayments') }}"><i class="fa fa-file fa-fw"></i> Loan Repayment Reports</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('financialreports') }}"><i class="fa fa-file fa-fw"></i> Financial Reports</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('statutoryreports') }}"><i class="fa fa-file fa-fw"></i> Statutory Reports</a>
                    </li> 
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->
