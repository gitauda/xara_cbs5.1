<nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
           <li>
                <a href="{{ URL::to('fleet_management') }}"><i class="glyphicon glyphicon-user fa-fw"></i>Fleet Management</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-cogs fa-fw"></i>Vehicles<i class="fa fa-caret-down"></i></a>
                <ul class="nav">
                    <li>
                        <a href="#"><i class="fa fa-credit-card fa-fw"></i>Vehicles</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-credit-card fa-fw"></i>Expenses</a>
                    </li>  
                </ul>
            </li> 
            <li>
                <a href="#"><i class="fa fa-cogs fa-fw"></i>Cash collection<i class="fa fa-caret-down"></i></a>
                <ul class="nav">
                    <li>
                        <a href="{{URL::to('fleetCashCollection')}}"><i class="fa fa-credit-card fa-fw"></i>Collection</a>
                    </li>
                    <li>
                        <a href="{{URL::to('fleetCashCollectors')}}"><i class="fa fa-credit-card fa-fw"></i>Collectors</a>
                    </li>
                   
                </ul>
            </li> 
            <li>
                <a href="#"><i class="fa fa-cogs fa-fw"></i>Maintenance<i class="fa fa-caret-down"></i></a>
                <ul class="nav">
                    <li>
                        <a href="{{URL::to('fleetMaintSchedule')}}"><i class="fa fa-credit-card fa-fw"></i>Schedule</a>
                    </li>
                    <li>
                        <a href="{{URL::to('fleetMaintRecords')}}"><i class="fa fa-credit-card fa-fw"></i>Records</a>
                    </li>
                   
                </ul>
            </li> 
            <li>
                <a href="{{URL::to('fleetFuelConsump')}}"><i class="fa fa-cog fa-fw"></i>Fuel consumption</a>
            </li> 

        </ul>
    </div>
</nav>