<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>XARA CBS </title>
    
    {{-- {{ HTML::script('media/js/jquery.js') }}
    {{ HTML::script('media/js/jquery.dataTables.js') }}
    {{ HTML::script('datepicker/js/bootstrap-datepicker.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js') }} --}}
    <!-- Core CSS - Include with every page -->
    {{-- {{ HTML::style('css/bootstrap.min.css') }} --}}
   {{-- {{ HTML::style('font-awesome-4.6.3/css/font-awesome.css') }} --}}
    <!-- Page-Level Plugin CSS - Blank --> 
    <!-- SB Admin CSS - Include with every page -->  
    {{-- {{ HTML::style('css/sb-admin.css') }} --}}
    <!-- datatables css -->
    {{-- {{ HTML::style('media/css/jquery.dataTables.min.css') }} --}}
    {{-- {{ HTML::style('datepicker/css/bootstrap-datepicker.css') }} --}}
    <!-- jquery scripts with datatable scripts -->
    <script src="{{ asset('media/js/jquery.js')}}"></script>
    <script src="{{ asset('media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js')}}"></script>

    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('font-awesome-4.6.3/css/font-awesome.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/sb-admin.css')}}"/>
    <link rel="stylesheet" href="{{asset('media/css/jquery.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('datepicker/css/bootstrap-datepicker.css')}}"/>

   <script type="text/javascript">
      $(document).ready(function() {
        $('#users').DataTable({
            aaSorting: []
        });
       $('#user2').DataTable({
            aaSorting: []
        });

        $('#mobile').DataTable();
        $('#rejected').DataTable();
        $('#app').DataTable();
        $('#disbursed').DataTable();
        $('#amended').DataTable();

    	} );
  </script>

<style type="text/css">

   .right-inner-addon {
    position: relative;
   }
   .right-inner-addon input {
    padding-right: 30px;
   }
   .right-inner-addon i {
    position: absolute;
    right: 0px;
    padding: 10px 12px;
    pointer-events: none;
   }

   .ui-datepicker {
    padding: 0.2em 0.2em 0;
    width: 550px;
   }

   tfoot {
    display: table-header-group;
   }

   tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

   </style>
<script type="text/javascript">
  $(function(){
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-3y',
        endDate: '+0y',
        autoclose: true
    });
  });
</script>
<script type="text/javascript">
  $(function(){
    $('.datepicker2').datepicker({
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months",
        startDate: '-3y',
        endDate: '+0y',
        autoclose: true
    });
  });
</script>


<script type="text/javascript">
$(function(){
$('.datepicker42').datepicker({
    format: " yyyy",
    startView: "years",
    minViewMode: "years",
    startDate: '-3y',
    endDate: '+0y',
    autoclose: true
});
});
</script>
<!-- Restricts formart integer for any number input box -->
<script>
 $(document).ready(function () {
     $(".numbers").forceNumeric();
 });

 // forceNumeric() plug-in implementation
 jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
             //   key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }
</script>
</head>
