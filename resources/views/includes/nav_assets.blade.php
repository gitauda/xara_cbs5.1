<nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{ URL::to('assets') }}">
                            <i class="fa fa-list"></i>
                            Assets
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('assets_allocation') }}">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Asset allocation
                        </a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->