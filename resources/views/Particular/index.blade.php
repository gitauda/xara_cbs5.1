@extends('layouts.accounting')
@section('content')
    <h1>Particulars</h1>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a class="btn btn-info btn-sm" href="{{ route('particulars.create')}}">New</a>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-bordered table-responsive table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Credit Account</th>
                            <th>Debit Account</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($particulars as $particular)
                            <tr>
                                <td> {{ $i }}</td>
                                <td>{{ $particular['name'] }}</td>
                                <td>{{ $particular['credit_account'] }}</td>
                                <td>{{ $particular['debit_account'] }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            Action <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{url('particulars/'.$particular['id'].'/edit/')}}">Update</a>
                                            </li>
                                            <li>
                                                <form action="{{url('particulars/'.$particular['id'])}}" method="post">
                                                    <input type="hidden" name="_method" value="delete">
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection