@extends('layouts.member')
@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3>Members</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(Session::get('notice'))
                <div class="alert alert-success">{{ Session::get('notice') }}</div>
            @endif
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#active-members" aria-controls="remittance" role="tab"
                           data-toggle="tab">Active Members</a>
                    </li>
                    <li role="presentation">
                        <a href="#inactive-members" aria-controls="profile" role="tab" data-toggle="tab">
                            Inactive Members</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="active-members">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="btn btn-info btn-sm" href="{{ URL::to('members/create') }}">New Member</a>
                            </div>
                            <div class="panel-body">
                                <table id="users"
                                       class="table table-condensed table-bordered table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Number</th>
                                        <th>Member Name</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($members as $member)
                                        @if($member->is_active)
                                            <tr>
                                                <td> {{ $i }}</td>
                                                <td>{{ $member->membership_no }}</td>
                                                <td>{{ $member->name }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-info btn-sm dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            @if($member->is_css_active == false)
                                                                <li>
                                                                    <a href="{{URL::to('portal/activate/'.$member->id)}}">Activate</a>
                                                                </li>
                                                            @endif
                                                            @if($member->is_css_active == true)
                                                                <li>
                                                                    <a href="{{URL::to('portal/deactivate/'.$member->id)}}">Deactivate</a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="inactive-members">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="btn btn-info btn-sm" href="{{ URL::to('members/create') }}">New Member</a>
                            </div>
                            <div class="panel-body">
                                <table id="users"
                                       class="table table-condensed table-bordered table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Number</th>
                                        <th>Member Name</th>
                                        <th>Member Branch</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($members as $member)
                                        @if(!$member->is_active)
                                            <tr>
                                                <td> {{ $i }}</td>
                                                <td>{{ $member->membership_no }}</td>
                                                <td>{{ $member->name }}</td> 
                                                <td>{{ $member->branch->name }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-info btn-sm dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            @if($member->is_css_active == false)
                                                                <li>
                                                                    <a href="{{URL::to('portal/activate/'.$member->id)}}">Activate</a>
                                                                </li>
                                                            @endif
                                                            @if($member->is_css_active == true)
                                                                <li>
                                                                    <a href="{{URL::to('portal/deactivate/'.$member->id)}}">Deactivate</a>
                                                                </li>
                                                            @endif
                                                            <li><a href="{{URL::to('css/reset/'.$member->id)}}">Reset
                                                                    Password</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop