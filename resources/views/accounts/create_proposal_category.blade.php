@extends('layouts.accounting')
@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3>New</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form method="POST" action="{{{ URL::to('budget/proposal/create') }}}" accept-charset="UTF-8">
                <fieldset>
                    <div class="form-group">
                        <label for="username">Type</label>
                        <select class="form-control selectable" name="type">
                            <option value="">select type</option>
                            <option>--------------------------</option>
                            <option value="INTEREST">Interest</option>
                            <option value="OTHER INCOME">Other Income</option>
                            <option value="EXPENDITURE">Expenditure</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="username">Name</label>
                        <input class="form-control" placeholder="Name" type="text" name="name" id="name"
                               value="{{{ Input::old('name') }}}" required>
                    </div>
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop
