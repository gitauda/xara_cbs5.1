@extends('layouts.accounting')
@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <h3>New Income</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form method="POST" action="{{{ URL::to('journals') }}}" accept-charset="UTF-8">
                <input type="hidden" name="income" value="income">
                <fieldset>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <div class="right-inner-addon ">
                            <i class="glyphicon glyphicon-calendar"></i>
                            <input class="form-control datepicker" readonly placeholder="Date" type="text" name="date"
                                   id="date" @if(Input::old('date')) value="{{{ Input::old('date') }}}" @else value="{{date('Y-m-d')}}" @endif required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="particulars">Particulars</label>
                        <select class="form-control selectable" name="particular" id="particulars" required>
                            @foreach($particulars as $particular)
                                <option value="{{ $particular->id }}">{{ $particular->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control"
                                  required>{{{ Input::old('description') }}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input class="form-control numbers" placeholder="" type="number" name="amount" id="amount"
                               value="{{{ Input::old('amount') }}}" required>
                    </div>
                    <div class="form-group">
                        <label for="narration">Narration</label>
                        <select class="form-control selectable" name="narration" id="narration" required>
                            <option value="0">Moto Sacco</option>
                            @foreach($members as $member)
                                <option value="{{ $member->id }}">{{ $member->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="user" value="{{ Confide::user()->username }}">
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-primary btn-sm">Submit Entry</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop