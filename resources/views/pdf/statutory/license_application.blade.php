<html><head>
   <style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px;}
   </style><body>

    <div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
    <div class='onerow headerrow'>
        <span><u>APPLICATION FORM FOR A LICENSE</u></span>
    </div>
    <div class='onerow'>
        <span>1.  Name of Sacco Society: ..........................................................................................</span><br><br>
       <div class='subspans'><span>C.S No.  ..............................</span><span>Date of registration  ..............................</span></div>
    </div>
    <div class='onerow'>
        <span>2.  Location of registered office: ................................................................</span>
    </div>
    <div class='onerow'>
        <span>3.  Physical Address of Head Office: L.R. No.....................................................................................</span><br><br>
        <div class='subspans'><span>Street........................................</span>
        <span>Building........................................</span></div>
    </div> 
    <div class='onerow'>
        <span>4.  Postal Address..............................................</span> <span>Postal Code..........................................................</span><br><br>
        <div class='subspans'><span>Telephone no.........................................</span> <span>P.I.N. No........................................</span><br><br>
        <span>Email address.........................................</span></div>
    </div>
    <div class='onerow'>
        <span>5.</span> <div class='spandiv'>Names of places of business in Kenya and the number of years each has been
                    established and has conducted or carried out business</div>
        <table class="table table-bordered onerowtable">
              <tr>
                <td><i>S/No.</i></td>
                <td><i>Name of Branch</i></td>
                <td><i>Year Established</i></td>
                <td><i>No. of Years in Operation</i></td>
              </tr> 
              <tr>
                <td>1</td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>6</td><td></td><td></td><td></td>
              </tr>  
              <tr>
                <td>7</td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>8</td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        </table>
        <div class='bottomdiv'>
            NB: Attach to this application a list of other places of business. Provide name of the
            place of business, state whether it is a satellite, mobile unit, ATM or point of sale etc.
        </div>
    </div><br>

    <div class='onerow'>
        <span>6.  Former name(s), if applicable, by which the Sacco Society has been known</span><br><br>
        <div class='spandiv2'>
            <span>1 ................................................from......................To........................</span><br><br>
            <span>2 ................................................from......................To........................</span><br><br>
            <span>3 ................................................from......................To........................</span><br><br>
            <span>4 ................................................from......................To........................</span><br><br>
        </div>
    </div>

    <div class='onerow'>
        <span>7.   Details of Capital</span><br><br>
        <div class='spandiv2'>
            <span>(a). Paid-up value..........................................................</span><br><br>
            <span>(b). Core capital ..........................................................</span><br><br>
            <span>(c). Institutional capital .................................................</span><br><br>
        </div>
    </div>
   
    <div class='onerow'>
        <span>8. Particulars of Officers:</span><br><br>
        <span style='margin-left:20px;'>a) Directors</span><br><br>
        <table class="table table-bordered onerowtable">
              <tr>
                <td><i>S/No.</i></td>
                <td><i>Present & Former Name</i></td>
                <td><i>DOB</i></td>
                <td><i>Address</i></td>
                <td><i>Date of Appointment</i></td>
                <td><i>Other Directorship</i></td>
              </tr> 
              <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>6</td><td></td><td></td><td></td><td></td><td></td>
              </tr>  
              <tr>
                <td>7</td><td></td><td></td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        </table><br><br>

        <span style='margin-left:20px;'>b) Senior management</span><br><br>
        <table class="table table-bordered onerowtable">
              <tr>
                <td><i>S/No.</i></td>
                <td><i>Present & Former Names</i></td>
                <td><i>Designation</i></td>
                <td><i>DOB</i></td>
                <td><i>Academic/Professional Qualifications</i></td>
                <td><i>YO</i></td>
                <td><i>DOA</i></td>
                <td><i>Previous employment</i></td>
              </tr> 
              <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        </table><br>
          <div style='text-align:center;'>Note: DOB – Date of Birth; YO – Year Obtained; DOA – Date of Appointment.</div>
    </div>
    <div class='onerow'>
      <span>9.   Names of Bankers and their Address</span><br>
      <div class='spandiv2'>
        <span>1.  .............................................................. Box ...............................................</span><br><br>
        <span>2.  .............................................................. Box ...............................................</span><br><br>
        <span>3.  .............................................................. Box ...............................................</span><br><br>
        <span>4.  .............................................................. Box ...............................................</span><br><br>
      </div>
    </div>

    <div class='onerow'>
        <span>10.</span> <div class='spandiv'><span>   Has the Sacco Society ever been put under receivership or made any compromise or
            arrangement with its creditors or otherwise failed to satisfy creditors in full?</span><br><span>If so,give particulars</span><br><br>
        <span style='text-align:center;'>..........................................................................................................................................</span></div>
    </div>
    <div class='onerow'>
        <span>11.</span> <div class='spandiv'><span>   Is the Sacco Society under investigation by an inspector or other authorized officer
          of any government ministry, department or agency, professional association or other
          regulatory body or has any investigation ever taken place in the affairs of the Sacco
          S o c i e t y ?</span><br><span>If so,give particulars</span><br><br>
        <span style='text-align:center;'>..........................................................................................................................................</span></div>
    </div>
    <div class='onerow'>
        <span>12.</span> <div class='spandiv'><span> Is the Sacco Society currently engaged or does it expect to be involved in any
litigation which may have a material effect on the resources of the Sacco Society?</span><br><span>If so,give particulars</span><br><br>
        <span style='text-align:center;'>..........................................................................................................................................</span></div>
    </div>

    <div class='onerow'>
      <span>13.   DECLARATION</span><br>
      <div style='margin-left:25px;'>
        <span>We, the undersigned, being officers of the Sacco Society, declare that to the best of our
              knowledge and belief, the information contained herein and any attachments is complete,
              accurate and true.</span><br><br>
        <div style='margin-left:20px;'>
          <span>a) Chairman (name) ........................................................</span><br><br>
          <span style='margin-left:20px;'>Signature.........................................................Date..........................................</span><br><br><br>
          <span>b) Chief Executive Officer (Name) .........................................</span><br><br>
          <span style='margin-left:20px;'>Signature.........................................................Date..........................................</span>
        </div>
      </div>
    </div><br> <br>
   
    <div class='onerow'>
      <span>Note: This application must be accompanied by all the relevant documents and
            requirements prescribed in the Act and these Regulations.</span>
    </div>
 </body></html>