<html><head>
   <style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px;}
   </style></head><body>

    <div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
    <div class='onerow headerrow'>
        <span><u>STATEMENT OF DEPOSIT RETURN</u></span>
    </div>
    <div class='onerow'>
    <table class="table table-bordered onerowtable">
              <tr>
                <td colspan='2'>Name of Sacco Society</td>
                <td colspan='2'></td>
                <td>CS No.</td>
              </tr> 
              <tr>
                <td colspan='2'>Financial Year:</td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td colspan='2'>Start Date:</td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td colspan='2'>End Date:</td><td colspan='2'></td><td></td>
              </tr>
              <tr><td colspan='5'></td></tr>
              <tr>
                <td>No</td><td>Range</td><td>*Type of Deposit</td><td>No. of A/Cs</td><td>Amount Kshs '000'</td>
              </tr> 
              <tr>
                <td>1</td><td>Less than 50,000</td><td>Non withdraw-able</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td></td><td>Savings</td><td></td><td></td>
              </tr>  
              <tr>
                <td></td><td></td><td>Term</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>2</td><td>50,000 to 100,000</td><td>Non withdraw-able</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td></td><td>Savings</td><td></td><td></td>
              </tr>  
              <tr>
                <td></td><td></td><td>Term</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>3</td><td>100,000 to 300,000</td><td>Non withdraw-able</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td></td><td>Savings</td><td></td><td></td>
              </tr>  
              <tr>
                <td></td><td></td><td>Term</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>4</td><td>300,000 to 1,000,000</td><td>Non withdraw-able</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td></td><td>Savings</td><td></td><td></td>
              </tr>  
              <tr>
                <td></td><td></td><td>Term</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td>Over 1,000,000</td><td>Non withdraw-able</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td></td><td>Savings</td><td></td><td></td>
              </tr>  
              <tr>
                <td></td><td></td><td>Term</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td></td><td>TOTAL</td><td></td><td></td>
              </tr>
              <tr>
                <td colspan='5'>Note:Monthly return to be received on or before the fifteenth day of the following month</td>
              </tr>   
              <tr>
                <td colspan='5'>To include accrued interest and any other form of deposit</td>
              </tr>  
              <tr rowspan='2'>
                <td colspan='5'><u>AUTHORIZATION</u></td>
              </tr> 
              <tr>
                <td colspan='4'>We declare that this return,to the best of our knowledge and belief is correct.</td><td></td>
              </tr>
              <tr>
                <td colspan='5'>
                    <span>...........................................Sign.....................................Date:............................</span><br>
                    <span>Name of Authorizing Officer............................................................</span>
                </td>
              </tr>
              <tr>
                <td colspan='5'>
                    <span>...........................................Sign.....................................Date:............................</span><br>
                    <span>Name of Countersigning Officer............................................................</span>
                </td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        </table>
    </div>
</body></html>