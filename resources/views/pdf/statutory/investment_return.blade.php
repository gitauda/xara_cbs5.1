<html><head><style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px;}
   </style></head><body>
        <div class='onerow topdivrow'>
            <div>
                <strong>
                    {{ strtoupper($organization->name)}}<br>
                </strong>
                {{ $organization->phone}}<br>
                {{ $organization->email}}<br>
                {{ $organization->website}}<br>
                {{ $organization->address}}
            </div>
        </div><br>
        <div class='onerow headerrow'>
            <span>INVESTMENT RETURN</span>
        </div>
        <div>
        <table class="onerowtable">
              <tr>
                <td>Sacco Society</td> <td></td> <td>CS No.......</td><td></td>
              </tr> 
              <tr>
                <td></td><td>Financial Year</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td>Start  Date</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td>End  Date</td><td></td><td></td>
              </tr>
              <tr rowspan=''><td colspan='4'></td></tr>  
              <tr><td>Ref No.</td><td colspan='2'></td><td></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
              <tr>
                <td>1.1</td><td colspan='2'>Core capital</td><td></td>
              </tr>  
              <tr>
                <td>1.2</td><td colspan='2'>Total assets</td><td></td>
              </tr>
              <tr>
                <td>1.3</td><td colspan='2'>Total deposits</td><td></td>
              </tr>
              <tr>
                <td>1.4</td><td colspan='2'>Non earning assets</td><td></td>
              </tr>
              <tr>
                <td>1.5</td><td colspan='2'>Financial assets</td><td></td>
              </tr>
              <tr>
                <td>1.6</td><td colspan='2'>Land and Building</td><td></td>
              </tr>
              <tr>
                <td>2.0</td><td colspan='2'>Land and buildings to total Assets Ratio(1.6/1.2)%</td><td></td>
              </tr>
              <tr>
                <td>2.1</td><td colspan='2'>Minimum land and building to total Assets requirement</td><td>5.0%</td>
              </tr>
              <tr>
                <td>2.2</td><td colspan='2'>Excess (deficiency) (2.0 less 2.1)</td><td>-5%</td>
              </tr>
              <tr>
                <td>3.0</td><td colspan='2'>Financial investments to Core capital(1.5/1.1)%</td><td></td>
              </tr>
              <tr>
                <td>3.1</td><td colspan='2'>Minimum financial investments to Core capital</td><td>40.0%</td>
              </tr>
              <tr>
                <td>3.2</td><td colspan='2'>Excess/(Deficiency)(3.0 less 3.1)</td><td>-40%</td>
              </tr>
              <tr>
                <td>4.0</td><td colspan='2'>Financial investments to Total Deposit Liabilities Ratio(1.5/1.3)%</td><td></td>
              </tr>
              <tr>
                <td>4.1</td><td colspan='2'>Minimum financial investments to total Deposit liabilities</td><td>5.0%</td>
              </tr>
              <tr>
                <td>4.2</td><td colspan='2'>Excess (Deficiency) (4.1 less 4.2)</td><td>-5%</td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        </table>
        </div>
        <div class='onerow'>
          <table>
            <tr><td colspan='4'>
            <span>Note: This return should be received on or before the fifteenth day of the month following end of every quarter.</span><br>
               <span>AUTHORIZATION: </span>
            </td></tr>
            <tr>
                <td colspan='4'>
                    <span>We declare that this return,to the best of our knowledge and belief is correct.</span><br>
                    <span>.................................................................Sign................................................Date:............................................</span><br>
                    <span>Name of Authorizing Officer...................................................</span>
                </td>
              </tr> 
              <tr>
                <td colspan='4'>
                    <span>.................................................................Sign................................................Date:............................................</span><br>
                    <span>Name of Countersigning Officer...................................................</span>
                </td>
              </tr>  
          </table>
        </div>
</body></html>