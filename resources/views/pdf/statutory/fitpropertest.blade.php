<html><head>
   <style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px; padding-top:5px;} .widen{padding:5px 10px;}
   </style><body>

    <div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
    <div class='onerow headerrow'>
        <span><u>FIT AND PROPER TEST</u></span>
    </div><br>
    <div class='onerow' style='text-align:center;'>
        <span>NOTE: Read the declaration on Section 6 below before completing this form. In case the
                space provided is inadequate, use additional paper.</span>
    </div><br>
    <div class='onerow'>
        <span class='headspan'>1. THE SACCO SOCIETY</span><br>
        <div class='spandiv2'>
            <span>a ) N a m e .................................................................</span><br><br>
            <span>b ) C/S NO .................................. Date of Registration.....................................</span>
        </div>
    </div>
    <div class='onerow'>
        <span class='headspan'>2. PERSONAL INFORMATION</span><br>
        <div class='spandiv2'>
            <span> a) Surname .................................................................</span><br><br>
            <span style='margin-left:20px;'> Other names .................................................................</span><br><br>
            <span> b) Previous Names (if any) by which you have been known:........................................................</span><br><br>
            <span> c) Year and place of birth .................................................................</span><br><br>
            <span> d) Personal Identification Number .................................................................</span><br><br>
            <span> e) ID. Card/Passport number and date of issue ........................................................</span><br><br>
            <span> f) Postal Address: .................................................................</span><br><br>
            <span> g) Previous Postal Addresses (if any) .................................................................</span><br><br>
            <span> h) Physical address .................................................................</span><br><br>
            <span> i) Educational Qualification and year obtained .........................................................</span><br>
            <span style='margin-left:20px;'> .....................................................................................</span><br><br>
            <span> j) Professional Qualifications and years obtained .................................................................</span><br><br>
            <span style='margin-left:20px;'> ......................................................................................</span><br><br>
            <span> k) Name(s) of your bankers during the last 5 years .................................................................</span><br><br>
            <span style='margin-left:20px;'> .......................................................................................</span><br><br>
        </div>
    </div><br>
    <div class='onerow'>
        <span class='headspan'>3. EMPLOYMENT/BUSINESS RECORD</span><br>
        <table class="table table-bordered onerowtable">
              <tr>
                <td rowspan='2'><i>S/No.</i></td>
                <td rowspan='2'><i>Name of Employer/Business</i></td>
                <td rowspan='2'><i>Address</i></td>
                <td rowspan='2'><i>Period</i></td>
                <td rowspan='2'><i>Position Held</i></td>
                <td colspan='2' class='widen'><i>Dates</i></td>
                <td rowspan='2'><i>Reasons for Leaving</i></td>
              </tr> 
              <tr><td class='widen'>FROM</td><td class='widen'>TO</td></tr>
              <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        </table><br><br>
    </div><br>

    <div class='onerow'>
        <span class='headspan'>4. DESCRIPTION OF YOUR PAST AND CURRENT ACTIVITIES</span><br>
        <span class='headspan'>4.1              SHAREHOLDING(DIRECTLY OWNED OR THROUGH NOMINEES)</span><br>

        <table class="table table-bordered onerowtable">
              <tr>
                <td rowspan='2'><i>S/No.</i></td>
                <td rowspan='2'><i>Name of Institution</i></td>
                <td rowspan='2'><i>Date of Incorporation</i></td>
                <td rowspan='2'><i>Amount of Shareholding</i></td>
                <td rowspan='2'><i>% of</i></td>
                <td colspan="2" class='widen'><i>Past Shareholding</i></td>
                <td rowspan='2'><i>Remarks</i></td>
              </tr> 
              <tr><td class='widen'>A</td><td class='widen'>B</td></tr>
              <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        </table><br><br>
        <div>
            <span>Key</span><br>
            <span>A: Refers to date of closure or surrender of shares </span><br>
            <span>B: Refers to reasons of closure or surrender. </span><br>
        </div><br><br>

        <span class='headspan'>4.2       DIRECTORSHIP</span><br>

        <table class="table table-bordered onerowtable">
              <tr>
                <td rowspan='2' ><i>S/No.</i></td>
                <td rowspan='2'><i>Name of Institution</i></td>
                <td rowspan='2'><i>Date of Incorporation</i></td>
                <td rowspan='2'><i>Excecutive or Non</i></td>
                <td rowspan='2'><i>Position held incase of executive</i></td>
                <td colspan="2" class='widen'><i>Past Shareholding</i></td>
                <td rowspan='2' ><i>Remarks</i></td>
              </tr> 
              <tr><td class='widen'>A</td><td class='widen'>B</td></tr>
              <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        </table><br><br>
        <div>
            <span>Key</span><br>
            <span>C: Refers to date of retirement </span><br>
            <span>D: Refers to reasons for retirement or resignation </span><br>
        </div>

        <span class='headspan'>4.3       PROFESSIONAL BODIES</span><br>

        <table class="onerowtable">
              <tr>
                <td rowspan="2"><i>S/No.</i></td>
                <td rowspan="2"><i>Name of Body</i></td>
                <td rowspan="2"><i>Member No.</i></td>
                <td rowspan="2"><i>Position</i></td>
                <td colspan="2" class='widen'><i>Past</i></td>
                <td rowspan="2"><i>Remarks</i></td>
              </tr> 
              <tr><td class='widen'>A</td><td class='widen'>B</td></tr>
              <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        </table><br><br>
        <div>
            <span>Key</span><br>
            <span>E: Refers to date of retirement </span><br>
            <span>F: Refers to reasons for retirement or resignation </span><br>
        </div>

        <span class='headspan'>4.4      SOCIAL CLUBS</span><br>

        <table class="table table-bordered onerowtable">
              <tr>
                <td rowspan="2"><i>S/No.</i></td>
                <td rowspan="2"><i>Club Name</i></td>
                <td rowspan="2"><i>Member No.</i></td>
                <td rowspan="2"><i>Position Held</i></td>
                <td colspan="2"><i>Past Club Memberships</i></td>
                <td rowspan="2"><i>Remarks</i></td>
              </tr> 
              <tr><td class='widen'>A</td><td class='widen'>B</td></tr>
              <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr> 
              <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>   
              <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        </table><br><br>
        <div>
            <span>Key</span><br>
            <span>G: Refers to date of retirement </span><br>
            <span>H: Refers to reasons for retirement or resignation </span><br>
        </div>

        <span class='headspan'>4.5      BORROWINGS</span><br>

        <table class="table table-bordered onerowtable">
            <tr>
                <td><i>S/No.</i></td>
                <td><i>Name of Borrower</i></td>
                <td><i>Lending Institution</i></td>
                <td><i>Type of Facility</i></td>
                <td><i>Date of Offer</i></td>
                <td><i>Security Offered</i></td>
                <td><i>Value of security</i></td>
                <td><i>Current outstanding balance</i></td>
                <td><i>Remarks</i></td>
            </tr> 
            <tr>
                <td>1</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr> 
            <tr>
                <td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>   
            <tr>
                <td>3</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>   
            <tr>
                <td>4</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>
            <tr>
                <td>5</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        </table><br><br>
        <div>
            <span>Key</span><br>
            <span>G: Refers to date of retirement </span><br>
            <span>H: Refers to reasons for retirement or resignation </span><br>
        </div><br>
        <span>*Borrower to indicate individual/personal as well as the private Company shareholdings in excess of 5%</span>
    </div>

    <div class='onerow'>
        <span class='headspan'>5. QUESTIONNARE</span><br><br>
        <div class='subdiv'>
            <div>
                <span>5.1 </span><div class='spandiv'>
                  <span>Have you or has any entity with which you are associated as director,
                        shareholder or manager, ever held or applied for a licence or equivalent
                       to carry on any business activity in any country? If so,
                      give particulars...................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div>
            <div>
                <span></span><div class='spandiv'>
                  <span>Do you know whether any such application was rejected or withdrawn after it
                      was made or any authorization revoked?.............................. If so, give particulars</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                <span>5.2 </span><div class='spandiv'>
                  <span>Have you at any time been convicted of any criminal offence in an jurisdiction? .........................
                        If so, give particulars of the court in which you were convicted, the offence, the
                        penalty imposed and the date of
                        conviction..............................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                <span>5.3 </span><div class='spandiv'>
                  <span>Have you, or any entity with which you have been involved, been censured,
                      disciplined, warned as to future conduct, or publicly criticized by any regulatory
                      authority or any professional body in any country? ................ If so, give
                      particulars .............................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                <span>5.4 </span><div class='spandiv'>
                  <span>Have you, or has any entity with which you are involved, or have been
                        associated as a director, shareholder or manager, been the subject of an
                        investigation, in any country, by a government department or agency,
                        professional association or other regulatory body? ................. If so, give
                        particulars.............................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                <span>5.5 </span><div class='spandiv'>
                  <span>Have you ever been dismissed from any employment, or been subject to
                          disciplinary proceedings by your employer or barred from entry of any
                          profession or occupation? .......................................................................If so give
                          particulars...............................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                <span>5.6 </span><div class='spandiv'>
                  <span>Have you failed to satisfy any debt adjudged due and payable by you on an
                        order of court, or have you made any compromise arrangement with your
                        creditors within the last 10 years? ....................................If so, give
                        particulars .......................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                <span>5.7 </span><div class='spandiv'>
                  <span>Have you ever been declared bankrupt by a court or has a bankruptcy petition
                          ever been served on you? ................................................. If so, give particulars. .......................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                  <span>5.8 </span><div class='spandiv'>
                  <span>Have you ever been held liable by a court, for any fraud or other misconduct?
                        ......................If so, give particulars.......................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                  <span>5.9 </span><div class='spandiv'>
                  <span>Has any entity with which you were associated as a director, shareholder or
                        manager in any country made any compromise or arrangement with its creditors,
                        been wound up or otherwise ceased business either while you were associated
                        with it or within one year after you ceased to be associated with it? If so, give
                        p a r t i c u l a r s.............................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                  <span>5.10 </span><div class='spandiv'>
                  <span>Are you presently, or do you, other than in a professional capacity, expect to be
                        e n g a g e d i n a n y l i t i g a t i o n i n a n y c o u n t r y ? . I f s o , g i v e
                        particulars .............................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                  <span>5.11 </span><div class='spandiv'>
                  <span>Indicate the names, addresses, telephone numbers and positions of three
                        individuals of good standing who would be able to provide a reference on your
                        personal and professional integrity. The referees must not be related to you, and
                        should have known you for at least five years.</span><br>
                  <span>i).................................................................................................................</span><br>
                  <span>ii)..................................................................................................................</span><br>
                  <span>iii)..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                  <span>5.12 </span><div class='spandiv'>
                  <span>Is there any additional information which you consider relevant for the
                        consideration of your suitability or otherwise for the position(s) held/to he held?
                        ....................................................................................... if so give particulars .............................................................................................................</span><br>
                  <span>.................................................................................................................</span><br>
                  <span>..................................................................................................................</span>
                </div>
            </div><br><br>
            <div>
                  <span>NOTE: The information given in response to this questionnaire shall be kept confidential
                        by the Authority, except in cases provided for by law. The omission of material facts may
                        represent the provision of misleading information.</span>
            </div>
        </div>
    </div>
    <div class='onerow'>
      <span>6. DECLARATION</span><br><br>
      <span>I am aware that it is an offence to knowingly or recklessly provide any information,
            which is false or misleading in connection with an application for a licence to carry out
            the Sacco deposit-taking business in Kenya. I am also aware that omitting material
            information intentionally or un-intentionally shall be construed to be an offence and may
            lead to rejection of my application and legal action being taken against the offender.
            I certify that the information given above is complete and accurate to the best of my
            knowledge, and that there are no other facts relevant to this application of which the
            Sacco societies Regulatory Authority should be aware.
            I undertake to inform the Authority of any changes material to the applications which
            may arise while the application is under consideration.
            Further, I confirm that I have agreed to fulfil the responsibilities related to this position.</span><br><br>
      <span>WITNESSED BEFORE ME:SIGNED.......................................(Witness)</span><br><br><br>
      <span>COMMISSIONER FOR OATHS/ MAGISTRATE</span><br><br>
      <span>Name....................................................................</span><br><br>
      <span>Signature....................................................................</span><br><br>
      <span>Address....................................................................</span><br><br>
    </div>
    <div class='onerow'>
      <span>NOTES FOR COMPLETION OF APPLICATION FORMS</span><br><br>
      <span>GENERAL</span><br><span></span><br>
      <span>These completion instructions are issued to give further guidance on completion of
            certain items in the application forms for a licence to conduct deposit taking Sacco
            business. These include.</span><br><br>
      <span>(i). Fit and Proper Test—</span><br>
      <span>This should be completed by all persons proposed as directors and senior management. In
            the case of senior management, the following officers: the chief executive officer and the
            deputy chief executive officer if any or the equivalent by whatever title they are called by
            the Sacco Society and the officers in charge of finance and audit functions or any other
            officer as may be determined by the Authority.</span><br>
      <span>(ii). Educational qualification- State only qualifications attained at college level or
                  highest educational qualification.</span><br>      
      <span>(iii). The rest of the items are considered self explanatory and should be completed as
            fully as possible.</span><br>
    </div>
    <div class='onerow'>
      <span>LICENCE No. ......................................................................................................................</span><br><br>
      <div style='text-align:center;'>LICENSE</div><br>
          <span>This LICENCE is granted to................................................................ (Name of
                Sacco Society ) C/S No. of (Address)
                and authorizes the said Sacco Society to conduct deposit-taking Sacco business in
                Kenya.</span><br><br>
          <span>
            This Licence is issued subject to the provisions of the Sacco Societies Act
            No. 14 of 2008 and the regulations issued there under and to any conditions
            endorsed hereon.</span><br<br>>

          <div style='text-align:center;'>CONDITIONS</div><br>
          <span>1. .............................................................................................................................</span><br>
          <span>2. .............................................................................................................................</span><br>
          <span>3. .............................................................................................................................</span><br>
          <span>4. .............................................................................................................................</span><br><br>

          <span>This LICENCE covers the period FROM ___________ _ TO _______ for the Sacco Society‘s head office and all places of business annexed hereto.</span><br>
          <span>The Sacco Societies Regulatory Authority may at any time revoke, amend or
                restrict this licence or vary any terms and/or conditions of its issuance.</span><br><br>
          <span>Issued under the common seal of</span><br><br>
    </div>
    <div class='onerow'>
      <span>THE SACCO SOCIETIES REGULATORY AUTHORITY</span><br>
      <span>Dated this......................................day of ....................................... 20 .........</span>
    </div>
</head></html>