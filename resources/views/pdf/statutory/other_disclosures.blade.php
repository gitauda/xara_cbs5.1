<html><head>
   <style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px; padding-top:5px;} .widen{padding:5px 10px;}
   </style></head><body>

    <div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
    <div class='onerow headerrow'>
        <span><u>OTHER DISCLOSURES</u></span>
    </div><br>
    <div class='onerow'>
    <table class="onerowtable">
              <tr>
                <td>Sacco Society</td><td colspan='2'></td><td></td><td></td>
              </tr> 
              <tr>
                <td>Financial Year</td><td colspan='2'></td><td></td><td></td>
              </tr>
              <tr>
                <td>Start Date</td><td colspan='2'></td><td></td><td></td>
              </tr>
              <tr>
                <td>End Date</td><td colspan='2'></td><td></td><td></td>
              </tr>
              <tr>
                <td colspan='5'>
              </tr> 
              <tr>
                <td rowspan='3'></td><td colspan='2' rowspan='3'>OTHER DISCLOSURES</td><td>31st Dec Current Year</td><td>31st Dec Prior Year</td>
              </tr>
              <tr>
                <td>.</td><td></td>
              </tr>
              <tr>
                <td class='widen'> . </td><td class='widen'>  </td>
              </tr>
              <tr>
                <td>Ref No.</td><td colspan='2'></td><td></td><td></td>
              </tr>  
              <tr>
                <td>1</td><td colspan='2'>NON-PERFORMING LOANS AND ADVANCES</td><td></td><td></td>
              </tr>  
              <tr>
                <td>1.1</td><td colspan='2'>Gross Non-Performing Loans and Advances</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td colspan='2'>Less</td><td></td><td></td>
              </tr>
              <tr>
                <td>1.2</td><td colspan='2'>Interest in Suspense</td><td></td><td></td>
              </tr> 
              <tr>
                <td>1.3</td><td colspan='2'>Total Non-Performing Loans and Advances (1.1-1.2)</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'>Less</td><td></td><td></td>
              </tr>
              <tr>
                <td>1.4</td><td colspan='2'>Allowance for loan loss</td><td></td><td></td>
              </tr>
              <tr>
                <td>1.5</td><td colspan='2'>Net Non-Performing Loans (1.3-1.4)</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>2</td><td colspan='2'>INSIDER LOANS AND ADVANCES</td><td></td><td></td>
              </tr>
              <tr>
                <td>2.1</td><td colspan='2'>Directors</td><td></td><td></td>
              </tr>
              <tr>
                <td>2.2</td><td colspan='2'>Employees</td><td></td><td></td>
              </tr>
              <tr>
                <td>2.3</td><td colspan='2'>Total Insider Loans, Advances and Other Facilities</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>3</td><td colspan='2'>OFF-BALANCE SHEET ITEMS</td><td></td><td></td>
              </tr>
              <tr>
                <td>3.1</td><td colspan='2'>Guarantees and Commitments</td><td></td><td></td>
              </tr>
              <tr>
                <td>3.2</td><td colspan='2'>Other Contingent Liabilities</td><td></td><td></td>
              </tr>
              <tr>
                <td>3.3</td><td colspan='2'>Total Contingent Liabilities</td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>4</td><td colspan='2'>CAPITAL STRENGTH</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.1</td><td colspan='2'>Core capital</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.2</td><td colspan='2'>Institutional Capital</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.3</td><td colspan='2'>Core Capital to Total Assets Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.4</td><td colspan='2'>Minimum Statutory Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.5</td><td colspan='2'>Excess/(Deficiency) (4.3-4.4)</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.6</td><td colspan='2'>Institutional Capital to Total Assets Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.7</td><td colspan='2'>Minimum Statutory Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.8</td><td colspan='2'>Excess/(Deficiency) (4.6-4.7)</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.9</td><td colspan='2'>Core Capital/ Deposit liabilities Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.10</td><td colspan='2'>Minimum Statutory Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>4.11</td><td colspan='2'>Excess/(Deficiency) (4.10-4.11)</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>5</td><td colspan='2'>LIQUIDITY</td><td></td><td></td>
              </tr>
              <tr>
                <td>5.1</td><td colspan='2'>Liquidity Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>5.2</td><td colspan='2'>Minimum Statutory Ratio</td><td></td><td></td>
              </tr>
              <tr>
                <td>5.3</td><td colspan='2'>Excess/(Deficiency) (5.1-5.2)</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>6</td><td colspan='2'>INVESTMENTS</td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>6.1</td><td colspan='2'>Land & Buildings/Total Assets</td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>6.2</td><td colspan='2'>Minimum Statutory Ratio</td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>6.3</td><td colspan='2'>Excess/(Deficiency) (6.1-6.3)</td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>6.4</td><td colspan='2'>Financial Investments/Total Assets</td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>6.5</td><td colspan='2'>Minimum Statutory Ratio</td><td></td><td></td>
              </tr>
              <tr rowspan='2'>
                <td>6.6</td><td colspan='2'>Excess/(Deficiency) (6.4-6.5)</td><td></td><td></td>
              </tr>
        </table><br><br>
        <div>Note: This return should be received within three months after the end of each financial year</div><br><br>
        <table>
            <span>AUTHORIZATION:</span><br><br>
             <tr>
                <td colspan='5'>
                    <span>We declare that this return, to the best of our knowledge and belief is correct.</span>
                </td>
              </tr>
              <tr>
                <td colspan='5'>
                    <span>.................................................................Sign................................................Date:............................................</span><br>
                    <span>Name of Authorizing Officer...................................................</span>
                </td>
              </tr> 
              <tr>
                <td colspan='5'>
                    <span>.................................................................Sign................................................Date:............................................</span><br>
                    <span>Name of Countersigning Officer...................................................</span>
                </td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        </table>

    </div>
    <div class='onerow'>
      <span>GENERAL</span> 
      <div class='spandiv2'>
        <span>a) These completion instructions are issued to ensure uniformity of reporting by all licensed Sacco societies.</span><br>
        <span>b) The accounts should be prepared in accordance with International Financial Reporting Standards.</span><br>
        <span>c) All figures should be shown in thousands of Kenya shillings.</span><br>
        <span>d) All the rows should be published irrespective of whether the licensed Sacco has a figure to report or not.</span><br>
        <span>e) Each return should be signed by at least two authorized signatories before submission to the Authority.</span><br>

      </div>
    </div>
</body></html>