<html><head>
   <style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px; padding-top:5px;} 
   </style></head><body>

    <div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
    <div class='onerow headerrow'>
        <span><u>RISK CLASSIFICATION OF ASSETS AND PROVISIONING</u></span>
    </div><br>
    <div class='onerow'>
    <table class="table table-bordered onerowtable">
              <tr>
                <td>Name of Sacco Society</td><td colspan='4'></td><td>CS No.</td>
              </tr> 
              <tr>
                <td>Financial Year</td><td colspan='4'></td><td></td>
              </tr>
              <tr>
                <td>Start Date</td><td colspan='4'></td><td></td>
              </tr>
              <tr>
                <td>End Date</td><td colspan='4'></td><td></td>
              </tr>
              <tr>
                <td colspan='6'>
              </tr> 
              <tr>
                <td></td><td></td><td colspan='4'>PORTFOLIO AGEING REPORT</td>
              </tr>   
              <tr>
                <td></td><td></td><td>A</td><td>B</td><td>C</td><td>D</td>
              </tr> 
              <tr>
                <td>No.</td><td>Classification</td><td>No. of A/Cs</td><td>Outstanding loan portfolio(KSh)</td><td>Required Provision</td><td>Required Provision Amount(KSh)</td>
              </tr> 
              <tr>
                <td>1</td><td>Performing</td><td></td><td></td><td>1%</td><td></td>
              </tr>
              <tr>
                <td>2</td><td>Watch</td><td></td><td></td><td>5%</td><td></td>
              </tr>
              <tr>
                <td>3</td><td>Substandard</td><td></td><td></td><td>25%</td><td></td>
              </tr>
              <tr>
                <td>4</td><td>Doubtful</td><td></td><td></td><td>50%</td><td></td>
              </tr>
              <tr>
                <td>5</td><td>Loss</td><td></td><td></td><td>100%</td><td></td>
              </tr>
              <tr>
                <td></td><td>Sub-Total</td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='3'>Rescheduled/Renegotiated loans</td><td></td><td></td>
              </tr>
              <tr>
                <td>6</td><td>Performing</td><td></td><td></td><td>1%</td><td></td>
              </tr>
              <tr>
                <td>7</td><td>Watch</td><td></td><td></td><td>5%</td><td></td>
              </tr>
              <tr>
                <td>8</td><td>Substandard</td><td></td><td></td><td>25%</td><td></td>
              </tr>
              <tr>
                <td>9</td><td>Doubtful</td><td></td><td></td><td>50%</td><td></td>
              </tr>
              <tr>
                <td>10</td><td>Loss</td><td></td><td></td><td>100%</td><td></td>
              </tr>
              <tr>
                <td></td><td><i>Sub-Total</i></td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td>GRAND TOTAL</td><td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <td colspan='6'>This return should be received on or before the fifteenth day of the month following end of every quarter</td>
              </tr>
              <tr>
                <td colspan='6'>AUTHORIZATION:</td>
              </tr> 
              <tr>
                <td colspan='6'>
                    <span>We declare that this return,to the best of our knowledge and velief is correct.</span><br>
                    <span>.................................................................Sign................................................Date:............................................</span><br>
                    <span>Name of Authorizing Officer...................................................</span>
                </td>
              </tr> 
              <tr>
                <td colspan='6'>
                    <span>.................................................................Sign................................................Date:............................................</span><br>
                    <span>Name of Countersigning Officer...................................................</span>
                </td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
        </table>
    </div>
    <div class='onerow'>
        <span><u>COMPLETION INSTRUCTIONS FOR RISK CLASSIFICATION AND LOAN LOSS PROVISIONING</u></span>
    </div><br><br>
    <div class='onerow'>
        <span>1. General</span>
        <div class='spandiv2'>
            <span>This return should be completed strictly in accordance with the Regulation on Risk Classification and Loan Loss Provisioning</span><br><br>
            <span>1. Enter in column A the number of accounts under each classification</span><br>
            <span>2. Enter in column B the amount outstanding under each classification categories of Performing; Watch; Sub-standard; Doubtful and Loss.</span><br>
            <span>3. Enter in column C the minimum provisions requirement in percentages in each classification category.</span><br>
            <span>4. Enter in column D provisions required in each classification category</span><br>
            <span>5. Enter in column E the discounted value of securities held in each classification category.</span><br>
            <span>6. Enter the difference between column D and E in column F, but for performing and watch class, enter the amount as is in column D in column F.</span><br>
        </div>
    </div><br><br>
    <div class='onerow'>
        <span>1. Notes for establishment and maintenance of the Allowance for loan loss accoun</span><br>
        <span>Allowance for loan loss account shall be a one time adjustment and shall be established as follows:</span><br>
        <div class='spandiv2'>
            <span>a).Create a contra-asset account to be called ―Allowance for Loan loss‖</span><br><br>
            <span>b).Create the expense account to be known as ―provision for loan loss‖</span><br>
            <span>c).Determine the amount needed in the allowance for loan loss by completing a risk classification form and credit the amount to the Allowance for loan loss</span><br>
            <span>d).Fund the amount needed in the allowance for loan l oss by debiting the same to retained earnings account. This is a one time adjustment</span><br>
            <span>e). Maintain the allowance for loan loss by debiting and crediting provision for loan loss and allowance for loan loss after performing a risk classification and loan loss provisioning</span><br>
            <span>f). To charge off loans, debit allowance for loan loss and credit loans to members with the same amount.</span><br>
            <span>g). To account for recovery of charged off bad debts, debit the bank account and credit allowance for loan loss account.</span><br>
        </div>
    </div><br><br>

</body></html>