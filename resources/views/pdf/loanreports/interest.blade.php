<html><head><style>
     @page { margin: 90px 30px 70px 30px; }
     .header { position: fixed; left: 0px; top: -80px; right: 0px; height: 80px;  text-align: center; }
     .footer { position: fixed; left: 0px; bottom: -60px; right: 0px; height: 50px;  }
     .footer .page:after { content: counter(page, upper-roman); }
     .content { margin-top: 5px;  }

   </style><body style="font-size:13px">
<?php


function asMoney($value) {
  return number_format($value, 2);
}

?>


   <div class="header">
     <table >

      <tr>



          <td>

              <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="{{ $organization->logo }}" style="height: 50px;"/>

          </td>

        <td>
        <strong>
          {{ strtoupper($organization->name)}}<br>
          </strong>
          {{ $organization->phone}} |
          {{ $organization->email}} |
          {{ $organization->website}}<br>
          {{ $organization->address}}


        </td>

        <td>

          <strong><h3>
            @if(!isset($all))
              {{$loanproduct->name}}
            @else
              Total
            @endif
              Interest Report {{$period}}
            </h3></strong>

        </td>


      </tr>


      <tr>

        <hr>
      </tr>



    </table>
   </div>
   <div class="footer">
     <p class="page">Page <?php $PAGE_NUM ?></p>
   </div>


<br>
   <div class="content">


      <table class="table table-bordered" border="1" cellspacing="0" cellpadding="5" style="width:100%">


        <tr>

          <td style="border-bottom:1px solid black;"><strong>Member No #</strong></td>
          <td style="border-bottom:1px solid black;"><strong>Member Name</strong></td>
          @if(!isset($all))
          <td style="border-bottom:1px solid black;"><strong>Loan Number</strong></td>
          @endif
          <td style="border-bottom:1px solid black;"><strong>Interest Sum</strong></td>

        </tr>
        <?php $totalsum = 0;  ?>
        @foreach($intArray as $key => $int)
        <tr>
          <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{$int['member_no']}}</td>
           <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{$int['member_name']}}</td>
           @if(!isset($all))
           <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{$int['loan_number']}}</td>
           @endif
           <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{asMoney($int['totalInt'])}}</td>
           <?php $totalsum += $int['totalInt']; ?>
        </tr>
        @endforeach
        <tr>
          <td <?php if(!isset($all)){echo "colspan='3'";} else{echo "colspan='2'";} ?> style="text-align: right;"> <strong>Total</strong> </td>
          <td>{{asMoney($totalsum)}}</td>
        </tr>

      </table></div></body></html>
