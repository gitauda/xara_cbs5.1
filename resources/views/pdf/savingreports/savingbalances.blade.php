<html><head><style>
        @page {
            margin: 170px 30px;
        }

        .header {
            position: fixed;
            left: 0px;
            top: -150px;
            right: 0px;
            height: 150px;
            text-align: center;
        }

        .footer {
            position: fixed;
            left: 0px;
            bottom: -180px;
            right: 0px;
            height: 50px;
        }

        .footer .page:after {
            content: counter(page, upper-roman);
        }

        .content {
            margin-top: -70px;
        }

    </style><body style="font-size:13px">
<?php
function asMoney($value)
{
    return number_format($value, 2);
}

?>
<div class="header">
    <table>
        <tr>
            <td>
                <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="{{ $organization->logo }}"
                     style="height: 50px;"/>
            </td>
            <td>
                <strong>
                    {{ strtoupper($organization->name)}}<br>
                </strong>
                {{ $organization->phone}} |
                {{ $organization->email}} |
                {{ $organization->website}}<br>
                {{ $organization->address}}
            </td>
            <td>
                <strong><h3>Savings Listing Report {{$period}}</h3></strong>
            </td>
        </tr>
        <tr>
            <hr>
        </tr>
    </table>
</div>
<div class="footer">
    <p class="page">Page <?php $PAGE_NUM ?></p>
</div>
<div class="content">
    <table class="table table-bordered" border = "1" cellspacing="0" cellpadding ='3' style="width:100%">
  
         
        <tr>
            <td style="border-bottom:1px solid black;"><strong>Member #</strong></td>
            <td style="border-bottom:1px solid black;"><strong>Member Name</strong></td>
            <td style="border-bottom:1px solid black;"><strong>Saving Product</strong></td>
            <td style="border-bottom:1px solid black;"><strong>Account Number</strong></td>
            <td style="border-bottom:1px solid black;"><strong>Account Balance</strong></td>
        </tr>
        <?php $balance_total = 0; ?>
        @foreach($savings as $saving)
            @if((int)($saving['balance']) > 0)
                <tr>
                    <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{$saving['member_no']}}</td>
                    <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{$saving['member_name']}}</td>
                    <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{$saving['product_name']}}</td>
                    <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{$saving['account_no']}}</td>
                    <td style="border-bottom:0.1px solid black; border-right:0.1px solid black;">{{asMoney($saving['balance'])}}</td>
                    <?php $balance_total += $saving['balance']; ?>
                </tr>
            @endif
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="border-bottom:0.1px double black; border-right:0.1px solid black;">
                <strong>TOTAL: {{ asMoney($balance_total) }}</strong>
            </td>
        </tr>
 
    </table>
</div>
</body></html>
