<html><head>
    <title>LOAN TRACKSHEET</title>   
   <style>
       table{width:100%; margin:0 auto; text-align:center; font-size:12px;} tr{border:1px solid; margin:0 auto;} 
       h3{margin:0 auto; border-bottom:1px solid;}
        .topdets{width:50%; margin:0 auto; display:inline; float:left;  flex-direction:row; flex-wrap:wrap; font-size:12px; margin-bottom:6px;} 
        .topdets div{width:100%; text-align:; padding:5px; box-sizing:border-box;}
   </style>
 </head><body style="font-size:11px;border: 1px solid blue;">
    <?php
      function asMoney($value) {
        return number_format($value, 2);
      }
      $amountdue=Loantransaction::getAmountDueAt($loanaccount,date('Y-m-d'),'installments');
    ?>   
   <div class="content" style="padding:10px 10px 10px 10px;">    
   <div style='width:100%; text-align:center; padding:5px; box-sizing:border-box;'><h3>Loan tracksheet</h3>  </div>
    <div class='topdets'> <div>Date ...................</div><div>Checked by ..................</div><div>Prepared by ..................</div></div>
   <table id="mobile" style='box-sizing:border-box;' class="ledger_table table-bordered table-responsive table-hover">
        <tbody>
            <tr>
            <th>a/c no</th>
            <th>Member</th>
            <th>Amount disbursed</th>
            <th>Credit period</th>
            <th>Expected</th>
            <th>Repaid</th>
            <th>Arrears</th>
            <th>Principal balance</th>  
            </tr>
            <tr>
                <td>{{ $loanaccount->account_number }}</td>
                <td>{{ $loanaccount->member->name}}</td>
                <td>{{ asMoney($loanaccount->amount_disbursed+$loanaccount->top_up_amount)}}</td>
                <td>{{ $loanaccount->period}}</td>
                <td>{{asMoney($amountdue)}}</td>
                <td>{{asMoney($amountpaid)}}</td>
                <td>{{asMoney($arrears)}}</td>
                <td>{{ asMoney(Loanaccount::getPrincipalBal($loanaccount))}}</td>
            </tr>
        </tbody>
    </table>
   </div>
 </body></html>