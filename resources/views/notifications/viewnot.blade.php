@extends('layouts.system')
@section('content')
<style>
    .toprow,.row2{
        display:flex; flex-direction:row; justify-content:space-around; font-family:Times; letter-spacing:2px;
        flex-wrap:wrap;
    }
    .toprow{background-color:#eee;  width:90%; border-radius:4px; margin:0 auto;} .toprow section{width:20%; padding:6px; box-sizing:border-box; text-align:center;}
    .row2{justify-content:center; margin-top:10px; align-items:center;} .row2 a{margin:2px;} .bold{font-size:17px; font-weight:bold;}
    .donespan{display:inline-block; padding:; border-radius:4px; height:; padding:2px 6px; border-radius:3px; margin:0px 2px;} 
    .rejectedspan{background-color:#e6aaaa;} .approvedspan{background-color:#9ac79a;}
    .rejectbut{background-color:red;} .editbut{background-color:grey;} a{color:black;}
    .titdiv{font-size:17px; padding:5px 2px;  width:90%; margin:0 auto;}
    .disposeform{width:; text-align:center; margin:0 auto;} .disposeform .form-group{padding:5px 2px;}
</style>
<div class='row'>
    <div class="col-lg-12">

        @if (Session::has('flash_message'))

        <div class="alert alert-success">
        {{ Session::get('flash_message') }}
        </div>
        @endif

        @if (Session::has('delete_message'))

        <div class="alert alert-danger">
        {{ Session::get('delete_message') }}
        </div>
        @endif

        @if (Session::get('notice'))
            <div class="alert alert-info">{{ Session::get('notice') }}</div>
        @endif
    </div>	
</div><br><br>
<div class='row'>
    <div class="col-lg-12">
    @if($what==='asset')
            <div class='row titdiv'>Asset purchase</div>
            <div class='row toprow'>
                <section >
                    <div class='bold'>Asset Name</div>
                    <div>{{$asset->asset_name}}</div>
                </section>
                <section >
                    <div class='bold'>Station</div>
                    <div><?php echo Member::where('id',$asset->member_id)->pluck('name');  ?></div>
                </section><div></div>
                <section >
                    <div class='bold'>Quantity</div>
                    <div>{{$asset->quantity}}</div>
                </section>
                <section >
                    <div class='bold'>Purchase price</div>
                    <div>{{$asset->purchase_price}}</div>
                </section>
                <section >
                    <div class='bold'>Warranty Expiry</div>
                    <div>{{$asset->warranty_expiry}}</div>
                </section>
                <section >
                    <div class='bold'>Asset status</div>
                    <div>{{$asset->status}}</div>
                </section>
                <section >
                    <div class='bold'>Salvage value</div>
                    <div>{{$asset->salvage_value}}</div>
                </section>
            </div>
            <div class='row row2'>
                <a class="btn editbut btn-sm" href="{{ URL::to('assets/'.$asset->id.'/superadmin/edit2')}}">Edit</a>
                @if($asset->purchase_rejected==1)
                    <span class='rejectedspan donespan' >Rejected</span>
                @else
                <a class="btn rejectbut btn-sm" href="{{ URL::to('notifications/reject/'.$asset->id.'/asset')}}">Reject</a>
                @endif
                @if($asset->purchase_approved==1)
                    <span class='approvedspan donespan'>Approved</span>
                @else
                <a class="btn btn-success btn-sm" href="{{ URL::to('notifications/approve/'.$asset->id.'/asset')}}">Approve</a>
                @endif
            </div>
    @elseif($what==='asset_disposal')
        <div class='row titdiv'>Asset disposal</div>    
        <div class='row toprow'>
                <?php $requester=User::find($notification->link);?>
                <section >
                    <div class='bold'>Asset Name</div>
                    <div>{{$asset->asset_name}}</div>
                </section>
                <section >
                    <div class='bold'>Quantity</div>
                    <div>{{$notification->confirmation_code}}</div>
                </section>
                <section >
                    <div class='bold'>Purchase price</div>
                    <div>{{$asset->purchase_price}}</div>
                </section>
                <section >
                    <div class='bold'>Disposal price</div>
                    <div>{{$notification->about}}</div>
                </section>
                <section >
                    <div class='bold'>Book value</div>
                    <div>{{$asset->book_value}}</div>
                </section>
                <section >
                    <div class='bold'>Requested by</div>
                    <div>{{$requester->username}}</div>
                </section>
            </div>
            <div class='row row2'>
                <form class="form-inline disposeform" role="form" action="{{ URL::to('assets/dispose/'.$asset->id.'/dispose')}}" method="POST">
                    <input type="hidden"  name="asset" value="{{$asset->id}}">
                    <input type="hidden"  name="price" value="{{$notification->about}}">
                    <input type="hidden"  name="quantity" value="{{$notification->confirmation_code}}">
                    <a class="btn editbut btn-sm" href="#" data-toggle="modal" data-target="#editDisposalModal">Edit</a>
                    <input type="submit" class="btn btn-primary btn-sm" name="btnSubmit" value="Dispose">
                </form>
            </div>
            <div id="editDisposalModal" class="modal fade" role="dialog">
  				<div class="modal-dialog"><div class="modal-content">  
				  <div class="modal-body">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <form class="form-inline disposeform" role="form" action="{{ URL::to('notifications/editnot/'.$notification->id)}}" method="POST">
						<div class="form-group">
							<label>Price for each: </label><br>
							<input type="number" class="form-control input-sm" name="price" value="{{$notification->about}}" style="width: 300px" required>
						</div><br>
						<div class="form-group">
							<label>Quantity: </label><br>
							<input type="number" class="form-control input-sm" name="quantity" value="{{$notification->confirmation_code}}" style="width: 300px" required>
						</div><br>
						<div class="form-group">
							<input type="submit" class="btn btn-primary btn-sm" name="btnSubmit" value="Edit">
						</div>
					</form>
					</div>
				  </div></div>
			</div>

        @endif
    </div>
</div>
@stop