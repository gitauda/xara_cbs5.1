@extends('layouts.accounting')

@section('content')
<?php

function asMoney( $money ){
  return number_format($money, 2);
} ?>
<br>
<br>
<div class="row">
<div class="col-lg-5">
<h4>Select month</h4>
<form class="form-inline" action="{{URL::to('transactions/'.date('m-Y'))}}" method="GET">

  <fieldset class="form-group">
    <label for="month"></label>
    <input type="text" class="form-control datepicker2" name="month" value="{{$month}}">
  </fieldset>


  <button type="submit" class="btn btn-primary mb-2">Refresh</button>
</form>

<br>

</div>

</div>
<div class="panel-heading">
    <a class="btn btn-primary mb-2 pull-right"  href="{{ URL::to('reports/selecttransactionPeriod')}}">Generate Report </a>
  </div>
@if(isset($transactions) && !empty($transactions))

  <br>
  <br>
<div class="row">

<div class="panel panel-default">

  <div class="panel-body">


    <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
      <thead>
        <th>#</th>
        <th>Date</th>
        <th>Description</th>
        <th>Account Debited</th>
        <!--<th></th>-->
        <th>Account Credited</th>
        <th>Amount</th>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        @foreach($transactions as $transaction)
            <tr>
                <td> {{ $i }}</td>
                <td>{{ $transaction->date }}</td>
                <td>{{ $transaction->description }}</td>
                <td>{{ Account::getAccountName($transaction->account_debited) }}</td>
                <td>{{ Account::getAccountName($transaction->account_credited) }}</td>

                <td>
                  {{asMoney($transaction->transaction_amount)}}
                </td>

            </tr>
            <?php $i++; ?>
        @endforeach
        </tbody>
    </table>

  </div>
</div>

</div>
@endif

@endsection
