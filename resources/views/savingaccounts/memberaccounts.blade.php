

@extends('layouts.main')
@section('content')
<style>
  tr{}
</style>
<br/>

<div class="row">
	<div class="col-lg-12">
  <h3>{{$member->name}}</h3>
  <p>Saving Accounts</p>

<hr>
</div>	
</div>


<div class="row">
	<div class="col-lg-12">

    <div class="panel panel-default">
      <div class="panel-heading">
         @if(Auth::user()->user_type != 'teller')

                   <a class="btn btn-info btn-sm" href="{{ URL::to('savingaccounts/create/'.$member->id)}}">new Saving Account</a>
          @endif

          
        </div>
        <div class="panel-body" style='overflow-x:scroll;'>


    <table id="users" class="table table-condensed table-bordered table-responsive table-hover">


      <thead>

        <th>#</th>
        <th>Member Nam</th>
        <th>Savings Product</th>
        <th>Account Number</th>   
         
        <th></th>
         <th></th>

      </thead>
      <tbody>

        <?php $i = 1; ?>
        @foreach($member->savingaccounts as $saving)
          <?php $accountBal=App\Savingaccount::getAccountBalance($saving); ?>
        <tr>
          <td> {{ $i }}</td>
          <td>{{ $saving->member->name }}</td>
          <td>{{ $saving->savingproduct->name }}</td> 
          <td>{{ $saving->account_number }}</td> 
           <td> <a href="{{ URL::to('savingtransactions/show/'.$saving->id)}}" class="btn btn-primary btn-sm">Transactions </a></td>
           <td> <a href="{{ URL::to('savingtransactions/create/'.$saving->id)}}" class="btn btn-primary btn-sm">Transact </a></td>
            <?php if($accountBal<=0){?>
              <td> <a href="{{ URL::to('savingaccounts/destroy/'.$saving->id)}}" class="btn btn-primary btn-sm">Delete</a></td>
            <?php } ?>     
	</tr>  

        <?php $i++; ?>
        @endforeach


      </tbody>


    </table>
  </div>


  </div>

</div>
























@stop