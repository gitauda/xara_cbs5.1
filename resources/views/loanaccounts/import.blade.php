@extends('layouts.loans')
@section('content')
<br><br>
<div class="row">
	<div class="col-lg-12">
    LOAN REPAYMENTS MIGRATION
		<hr>
        @if (Session::get('notice'))
            <div class="alert alert-success">{{ Session::get('notice') }}</div>
        @endif
    <!-- ############################################################  -->
<div class="col-lg-12">
<p><strong>Import Loan Repayments </strong></p>
    <p>&nbsp;</p>
    <form method="post" action="{{URL::to('import_repayments')}}" accept-charset="UTF-8" enctype="multipart/form-data">
      <div class="form-group">
        <label>Upload Repayment (Excel Sheet)</label>
        <input type="file" class="" name="banks" value="{{asset('/Excel/banks.xls')}}" />
    </div>
      <button type="submit" class="btn btn-primary">Import Repayments</button>
    </form>
</div>
<div class="col-lg-12">
    <hr>
  </div>
  </div>
</div>
@stop
