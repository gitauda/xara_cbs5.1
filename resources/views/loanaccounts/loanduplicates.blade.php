@extends('layouts.loans')
@section('content')
<br/>
<div class="row">
	<div class="col-lg-12">
    <h3>Loan Duplicates</h3>
    <hr>
  </div>
</div>

<div class="row">
	<div class="col-lg-5">
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
<table class="table table-bordered table-condensed" style="width: 100%;">
  <thead>
    <th>
      #
    </th>
    <th>
      Loan Product
    </th>
    <th>
      Loan 1
    </th>
    <th>
      Loan 2
    </th>
    <th>
      Action
    </th>
  </thead>

  <tbody>
    @foreach($loans as $loan)
      <th>
        $loan['loanproduct'];
      </th>
      <th>
        $loan['loan1'];
      </th>
      <th>
        $loan['loan2'];
      </th>
      <th>

      </th>
    @endforeach
  </tbody>
</table>

  </div>
</div>

@stop
