@extends('layouts.css')
@section('content')
<style>
    .inpu{width:240px; padding:6px 1px;}
</style> 
<br/>
<div class="row">
	<div class="col-lg-12">
        <h3>New Investment {{$what}}</h3>
        <hr>
        @if(Session::has('wrath'))
            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            <strong>{{ Session::get('wrath')}}</strong> 
            </div>      
        @endif      
    </div>	
</div>
<div class="row">
    <div class="col-lg-4">        
        @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif
        @if($what==='category')
            <form method="POST" action="{{ URL::to('investmentscats') }}" accept-charset="UTF-8">
                <fieldset>
                    <div class="form-group">
                        <label for="username">Category Name</label><br>
                        <input class="inpu" placeholder="" type="text" name="name"
                        value="{{{ Input::old('name') }}}" required>
                        <input type='hidden' name='what' value='category'>
                    </div>
                    <div class="form-group">
                        <label for="username">Category Code</label><br>
                        <input class="inpu" placeholder="" type="text" name="code" 
                        value="{{{ Input::old('code') }}}" required>
                    </div>            
                    <div class="form-group">
                        <label for="username">Category Description</label><br>
                        <textarea name="desc" class="inpu">
                            
                        </textarea>
                    </div>        
                    <div class="row">
                        <div class="col-lg-10">
                                <div class="form-actions form-group">        
                            <button type="submit" class="btn btn-primary btn-sm pull-centre">
                                Create Investment Category
                            </button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        @elseif($what==='class')
            <form method="POST" action="{{ URL::to('investmentscats') }}" accept-charset="UTF-8">
                <fieldset>
                    <div class="form-group">
                        <label for="username">Class Name</label><br>
                        <input class="inpu" placeholder="" type="text" name="name"
                        value="{{{ Input::old('name') }}}" required>
                        <input type='hidden' name='what' value='class'>
                    </div>
                    <div class="form-group">
                        <label for="username">Class Category</label><br>
                        <select name="ccategory" class='inpu' required >
                            <option></option>
                            @foreach($categories as $cat)
                                <option value='{{$cat->id}}'>{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="username">Class Code</label><br>
                        <input class="inpu" placeholder="" type="text" name="code" 
                        value="{{{ Input::old('code') }}}" required>
                    </div>          
                    <div class="form-group">
                        <label for="username">Class Description</label><br>
                        <textarea name="desc" class="form-control">
                            
                        </textarea>
                    </div>        
                    <div class="row">
                        <div class="col-lg-10">
                                <div class="form-actions form-group">        
                            <button type="submit" class="btn btn-primary btn-sm pull-centre">
                                Create Category class
                            </button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
            @elseif($what==='type')
            <form method="POST" action="{{ URL::to('investmentscats') }}" accept-charset="UTF-8">
                <fieldset>
                    <div class="form-group">
                        <label for="username">Type Name</label><br>
                        <input class="inpu" placeholder="" type="text" name="name"
                        value="{{{ Input::old('name') }}}" required>
                        <input type='hidden' name='what' value='type'>
                    </div>
                    <div class="form-group">
                        <label for="username">Type Category</label><br>
                        <select name="tcategory" class="tcategory inpu" required>
                            <option></option>
                            @foreach($categories as $cat)
                                <option value='{{$cat->id}}'>{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="username">Type Class</label><br>
                        <select name="tclass" class="tclass inpu">
                            <option></option>
                            @foreach($classes as $class)
                                <option value='{{$class->id}}'>{{$class->name}}</option>
                            @endforeach
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="username">Type Code</label><br>
                        <input class="inpu" placeholder="" type="text" name="code" 
                        value="{{{ Input::old('code') }}}" required>
                    </div>           
                    <div class="form-group">
                        <label for="username">Type Description</label><br>
                        <textarea name="desc" class="inpu"></textarea>
                    </div>        
                    <div class="row">
                        <div class="col-lg-10">
                                <div class="form-actions form-group">        
                            <button type="submit" class="btn btn-primary btn-sm pull-centre">
                                Create type
                            </button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>  
        @endif
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('.tcategory').change(function(){
            var lpvalue=$(this).val(); var hii=$(this); 
            $.get("{{ url('ajaxinvestmentclasses')}}",
                {lpvalue:lpvalue},
                function(data){
                    $('.tclass').val(data);
                });
        });

    });
</script>
@stop