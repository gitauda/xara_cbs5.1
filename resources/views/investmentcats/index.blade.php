@extends('layouts.css')
@section('content')
<br/>
<div class="row">
  <div class="col-lg-12">
    <h3>
      Investment Categories
    </h3>
    <hr>
  </div>  
</div>
<div class="row">
  <div class="col-lg-12">       
          @if(isset($catdone))
        <div class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{{ $catdone}}}</strong> 
          </div>      
         @endif 
         @if(isset($catupdated))
        <div class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{{ $catupdated}}}</strong> 
          </div>      
         @endif 
         @if(Session::has('puff'))
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{{Session::get('puff')}}}</strong> 
          </div>      
         @endif 
  <div role="tabpanel">
        <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#categories" aria-controls="remittance" role="tab"
                data-toggle="tab">Categories</a>
        </li>
        <li role="presentation">
            <a href="#classes" aria-controls="profile" role="tab" data-toggle="tab">
                Classes</a>
        </li>
        <li role="presentation">
            <a href="#types" aria-controls="profile" role="tab" data-toggle="tab">
                Types</a>
        </li>
    </ul>

  <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="categories">
          <div class="panel panel-default">
            <div class="panel-heading">
                <p> <?php  ?>
                    <a href="{{ URL::to('investmentscats/create/category') }}" class="btn btn-success">
                      New Investment category
                    </a>
                </p>
              </div>
              <div class="panel-body">
            <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
            <thead>
              <th>#</th>
              <th>Category Name</th>
              <th>Category Code</th>
              <th>Description</th>        
              <th></th>
            </thead>
            <tbody>
            <?php $i=1;?>
            @if(isset($cats) && $i<=count($cats))
              @foreach($cats as $invest)
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$invest->name}}</td>
                  <td>{{$invest->code}}</td>
                  <td>{{$invest->description}}</td>            
                  <td>
                    <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      Action <span class="caret"></span>
                    </button>         
                    <ul class="dropdown-menu" role="menu">                    
                      <li><a href="{{URL::to('investmentscats/update/category/'.$invest->id)}}">Edit</a> </li>
                      <li><a href="{{URL::to('investmentscats/delete/category/'.$invest->id)}} onclick="return (confirm('Are you sure you want to delete this investment category?'))">
                          Delete
                          </a>
                      </li>
                    </ul>
                  </div>
                  </td>
                </tr>
                <?php $i++;?>
              @endforeach
            @endif
            </tbody>
          </table></div>
        </div>
      </div>

      <div role="tabpanel" class="tab-pane" id="classes">
          <div class="panel panel-default">
            <div class="panel-heading">
                <p>
                    <a href="{{ URL::to('investmentscats/create/class') }}" class="btn btn-success">
                      New Investment Class
                    </a>
                </p>
              </div>
              <div class="panel-body">
            <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
            <thead>
              <th>#</th>
              <th>Class Name</th>
              <th>Class Category</th>
              <th>Class Code</th>
              <th>Description</th>  
              <th></th>  
            </thead>
            <tbody>
            <?php $i=1;?>
            @if(isset($classes) && $i<=count($classes))
              @foreach($classes as $class)
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$class->name}}</td>
                  <td>{{$class->category}}</td>
                  <td>{{$class->code}}</td>
                  <td>{{$class->description}}</td>            
                  <td>
                    <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      Action <span class="caret"></span>
                    </button>         
                    <ul class="dropdown-menu" role="menu">                    
                      <li><a href="{{URL::to('investmentscats/update/class/'.$class->id)}}">Edit</a> </li>
                      <li><a href="{{URL::to('investmentscats/delete/class/'.$class->id)}} onclick="return (confirm('Are you sure you want to delete this investment category?'))">
                          Delete
                          </a>
                      </li>
                    </ul>
                  </div>
                  </td>
                </tr>
                <?php $i++;?>
              @endforeach
            @endif
            </tbody>
          </table></div>
        </div>
      </div>

      <div role="tabpanel" class="tab-pane" id="types">
          <div class="panel panel-default">
            <div class="panel-heading">
                <p>
                    <a href="{{ URL::to('investmentscats/create/type') }}" class="btn btn-success">
                        New Investment Type
                    </a>
                </p>
              </div>
              <div class="panel-body">
            <table id="users" class="table table-condensed table-bordered table-responsive table-hover">
            <thead>
              <th>#</th>
              <th>Type Name</th>
              <th>Type Category</th>
              <th>Type Class</th>
              <th>Type Code</th>
              <th>Description</th>
              <th></th>      
            </thead>
            <tbody>
            <?php $i=1;?>
            @if(isset($types) && $i<=count($types))
              @foreach($types as $type)
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$type->name}}</td>
                  <td>{{$type->category}}</td>
                  <td>{{$type->class}}</td>
                  <td>{{$type->code}}</td>
                  <td>{{$type->description}}</td>            
                  <td>
                    <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      Action <span class="caret"></span>
                    </button>         
                    <ul class="dropdown-menu" role="menu">                    
                      <li><a href="{{URL::to('investmentscats/update/type/'.$type->id)}}">Edit</a> </li>
                      <li><a href="{{URL::to('investmentscats/delete/type/'.$type->id)}}" onclick="return (confirm('Are you sure you want to delete this investment category?'))">
                          Delete
                          </a>
                      </li>
                    </ul>
                  </div>
                  </td>
                </tr>
                <?php $i++;?>
              @endforeach
            @endif
            </tbody>
          </table></div>
        </div>
      </div>

    </div>
  </div>
  </div>
</div>
@stop