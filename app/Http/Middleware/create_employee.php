<?php

namespace App\Http\Middleware;

use App\Organization;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Zizaco\Entrust\Entrust;

class create_employee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (!Auth::user()){
                $sessionTimeout = 1;
                $organization = Organization::find(1);
                return View::make('login',compact('organization'));
        }else if (! Entrust::can('create_employee') ) // Checks the current user
        {
         return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
        }
        return $next($request);
    }
}
