<?php

namespace App\Http\Middleware;

use App\Organization;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class loggedin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()) {
            $sessionTimeout = 1;
            $organization = Organization::find(1);
            return View::make('login', compact('organization'));
        }
        return $next($request);
    }
}
