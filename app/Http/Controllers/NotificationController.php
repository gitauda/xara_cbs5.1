<?php namespace App\Http\Controllers;

use App\Account;
use App\AccountTransaction;
use App\Asset;
use App\Audit;
use App\Http\Controllers\Controller;
use App\Journal;
use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class NotificationController extends Controller {

	/**
	 * Display a listing of audits
	 *
	 * @return Response
	 */
	public function index()
	{
		$notifications = Notification::where("user_id",Auth::user()->id)->orderBy('id','DESC')->get();

		Audit::logAuditwithfewargs('Notification', 'viewed notifications', '0');

		return View::make('notifications.index', compact('notifications'));
	}

	/**
	 * Show the form for creating a new audit
	 * @return Response
	 */

    public function markasread($id)
	{
		$notification = Notification::findOrFail($id);
		$notification->is_read = 1;
		$notification->update();

		return Redirect::to('notifications/index')->withFlashMessage('Notification successfully marked as read!');
	}

	public function markallasread()
	{
		$notifications = Notification::where('user_id',Auth::user()->id)->get();
		foreach ($notifications as $notification) {
		$notification->is_read = 1;
		$notification->update();
        }
		return Redirect::to('notifications/index')->withFlashMessage('All Notifications successfully marked as read!');
	}

	public function create()
	{
		return View::make('audits.create');
	}

	/**
	 * Store a newly created audit in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Audit::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Audit::create($data);

		return Redirect::route('audits.index');
	}

	/**
	 * Display the specified audit.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$audit = Audit::findOrFail($id);

		return View::make('audits.show', compact('audit'));
	}

	/**
	 * Show the form for editing the specified audit.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$audit = Audit::find($id);

		return View::make('audits.edit', compact('audit'));
	}
	public function editnot($notid)
	{
		$notification=Notification::find($notid);
		$notification->about=Input::get('price');
		$notification->confirmation_code=Input::get('quantity');
		$notification->update();
		 $asset=Asset::find($notification->target); 
		 $what='asset_disposal';
		//return View::make('audits.edit', compact('audit'));
		return View::make('notifications.viewnot', compact('asset','what','notification'));
	}
	public function viewnot($id,$what,$notid)
	{	
		if($what=='asset'){$asset=Asset::find($id);}else if($what=='asset_disposal'){
		$asset= Asset::find($id);}else{$asset=0;}
		$notification=Notification::find($notid);			
		return View::make('notifications.viewnot', compact('asset','what','notification'));
	}
	public function reject($id,$what)
	{	if($what=='asset'){
			$asset=Asset::find($id); 
			$asset->purchase_approved=0; $asset->purchase_rejected=1; 
			$asset->approved_by=Auth::user()->id;  $asset->approved_at=date('Y-m-d H:i:s'); $asset->update();
		}else{$asset=0;}
		return View::make('notifications.viewnot', compact('asset','what'));
	}
	public function approve($id,$what)
	{	if($what=='asset'){
			$asset=Asset::find($id);
			$asset->purchase_approved=1; $asset->purchase_rejected=0; 
			$asset->approved_by=Auth::user()->id;  $asset->approved_at=date('Y-m-d H:i:s'); $asset->update();

				$num=Account::where('name',$asset->asset_name)->count();
				if($num<1){
					$code=Account::getaccountcode('ASSET');
					DB::table('accounts')->insertGetId(
						['category' => 'ASSET', 'code' => $code,'name'=>$asset->asset_name,'active'=>1]
					);
				}
				$newName=$asset->asset_name."_".$asset->id;
				$descNum=Journal::where('description','Registration of asset '.$newName)->count();
				if($descNum<1){
					$debitAc=Account::where('name',$asset->account)->first();
					$creditAc=Account::where('name','Bank Account')->first();
					$amou=(int)$asset->purchase_price*(int)$asset->quantity; 
					$data = [
						'date' => date("Y-m-d"),
						'debit_account' => $debitAc->id,
						'credit_account' => $creditAc->id,
						'description' => "Registration of asset $newName",
						'amount' => $amou,
						'initiated_by' => Auth::user()->username,
						'particulars_id' => 0,
						'narration'=>Auth::user()->id
					];

					$acTransaction = new AccountTransaction;
					$journal = new Journal;
					$acTransaction->createTransaction($data);
					$journal->journal_entry($data);
				}

		}else{$asset=1;}
		return View::make('notifications.viewnot', compact('asset','what'));
	}
	/**
	 * Update the specified audit in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$audit = Audit::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Audit::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$audit->update($data);

		return Redirect::route('audits.index');
	}

	/**
	 * Remove the specified audit from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Audit::destroy($id);

		return Redirect::route('audits.index');
	}

}
