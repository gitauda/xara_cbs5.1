<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Loanaccount;
use App\Loanrepayment;
use App\Loantransaction;
use App\Member;
use App\Organization;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class LoantransactionsController extends Controller {

	/**
	 * Display a listing of loantransactions
	 *
	 * @return Response
	 */
	public function index()
	{
		$loantransactions = Loantransaction::all();

		return View::make('loantransactions.index', compact('loantransactions'));
	}

	/**
	 * Show the form for creating a new loantransaction
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('loantransactions.create');
	}

	/**
	 * Store a newly created loantransaction in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Loantransaction::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Loantransaction::create($data);

		return Redirect::route('loantransactions.index');
	}

	/**
	 * Display the specified loantransaction.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$loantransaction = Loantransaction::findOrFail($id);

		return View::make('loantransactions.show', compact('loantransaction'));
	}

	/**
	 * Show the form for editing the specified loantransaction.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$loantransaction = Loantransaction::find($id);

		return View::make('loantransactions.edit', compact('loantransaction'));
	}

	/**
	 * Update the specified loantransaction in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$loantransaction = Loantransaction::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Loantransaction::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$loantransaction->update($data);

		return Redirect::route('loantransactions.index');
	}

	/**
	 * Remove the specified loantransaction from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Loantransaction::destroy($id);

		return Redirect::route('loantransactions.index');
	}


	public function statement($id){

		$account = Loanaccount::findOrFail($id);

		$transactions = $account->loantransactions()->orderBy('date')->get();
		foreach ($transactions as $transaction) {
		    if (strtolower($transaction->description) == "loan repayment" || strtolower($transaction->description) == "loan clearance") {
		        $transaction->hasRepayment = true;
		        $loanRepayments = Loanrepayment::where('loantransaction_id', $transaction->id)->get();
		        if (!$loanRepayments->isEmpty()) {
		            $transaction->repayments = $loanRepayments;
                } else {
		            $transaction->repayments = Loanrepayment::where('loanaccount_id', $id)
                        ->whereBetween('created_at', array(
                            date('Y-m-d H:i:s', strtotime('-5 seconds', strtotime($transaction->created_at))),
                            date('Y-m-d H:i:s', strtotime('+5 seconds', strtotime($transaction->created_at)))
                        ))->get();
                }
            } else {
                $transaction->hasRepayment = false;
            }
        }
        // return var_dump($transactions->toArray());
		/*
		print_r($transactions);
		$credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('amount');
		$debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('amount');

		$balance = $credit - $debit;
		*/
		$organization = Organization::findOrFail(1);

		$pdf = PDF::loadView('pdf.loanstatement', compact('transactions', 'organization', 'account'))->setPaper('a4')->setOrientation('potrait');
 	
		return $pdf->stream('loanstatement.pdf');
	}

	public function certificate($id){

		$account = Loanaccount::where('id','=',$id)->get()->first();

		$member=Member::where('id','=',$account->member_id)->pluck('name');

		$organization = Organization::where('id','=',1)
		->get()->first();

		$pdf = PDF::loadView('pdf.loancertificate', compact('member', 'organization', 'account'))->setPaper('a5')->setOrientation('landscape');;
 	
		return $pdf->stream('Loan Clearance Certificate.pdf');				
	}

	public function overpayments($id){

			$account = Loanaccount::where('id','=',$id)->get()->first();

			$member=Member::where('id','=',$account->member_id)->pluck('name');

			$organization = Organization::findOrFail(1);

			$pdf = PDF::loadView('pdf.overpayments', compact('member', 'organization', 'account'))->setPaper('a5')->setOrientation('landscape');;
	 	
			return $pdf->stream('Loan Overpayment Claim.pdf');				
		}
		public function loan_tracksheet($id){

			$loanaccount = Loanaccount::where('id','=',$id)->get()->first();

			$member=Member::where('id','=',$loanaccount->member_id)->get();

			$organization = Organization::findOrFail(1);
			$extra=Loantransaction::getExtraAmount($loanaccount,'arrears');
			$unpaid=Loantransaction::getAmountUnpaid($loanaccount); $arrears=(float)$extra+(float)$unpaid;
			$amountpaid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');

			$pdf = PDF::loadView('pdf.tracksheet', compact('member', 'organization', 'loanaccount','amountpaid','arrears'))->setPaper('a4')->setOrientation('landscape');

			return $pdf->stream('Loan Overpayment Claim.pdf');				
		}	

	public function receipt($id){

		$transaction = Loantransaction::findOrFail($id);

		$organization = Organization::findOrFail(1);

		$pdf = PDF::loadView('pdf.loanreports.receipt', compact('transaction', 'organization'))->setPaper('a5')->setOrientation('potrait');;
 	
		return $pdf->stream('receipt.pdf');


	}


}
