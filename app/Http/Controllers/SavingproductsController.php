<?php namespace App\Http\Controllers;

use App\Account;
use App\Charge;
use App\Currency;
use App\Http\Controllers\Controller;
use App\Savingaccount;
use App\Savingposting;
use App\Savingproduct;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SavingproductsController extends Controller {

	/**
	 * Display a listing of savingproducts
	 *
	 * @return Response
	 */
	public function index()
	{
		$savingproducts = Savingproduct::all();

		return View::make('savingproducts.index', compact('savingproducts'));
	}

	/**
	 * Show the form for creating a new savingproduct
	 *
	 * @return Response
	 */
	public function create()
	{

		$accounts = Account::all();
		$charges = Charge::all();
		$currencies = Currency::all();
		return View::make('savingproducts.create', compact('accounts', 'charges', 'currencies'));
	}

	/**
	 * Store a newly created savingproduct in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Savingproduct::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		#$charge_id = array(Input::get('charge_id'));
		$charge_id = Input::get('charge_id');
		$is_special=Input::get('is_special'); 
		$special_date=Input::get('special_date');
		if(empty($is_special)){
			$is_special=0;
		}else{
			$is_special=1;
		}
		if(!isset($special_date)){
		$special_date='none';
		}
		$prod_id = DB::table('savingproducts')->insertGetId(
             array(
                'name' => Input::get('name'),
                'shortname' => Input::get('shortname'),
                'opening_balance' => Input::get('opening_balance'),
                'currency' => Input::get('currency'),
                'Interest_Rate' => Input::get('interestrate'),
                'min_amount' => Input::get('minamount'),
				'type' => Input::get('type'),
				'is_special' => $is_special,
				'special_date' => $special_date 
                )
            );
		$product = Savingproduct::findOrFail($prod_id);
		$fee_income_acc = Input::get('fee_income_acc');
		$saving_control_acc = Input::get('saving_control_acc');
		$cash_account = Input::get('cash_account');
		//save charges
		if($charge_id != null){
			foreach($charge_id as $charg){
					$charge = Charge::findOrFail($charg);
					$product->charges()->attach($charge);
				}
		}
		// create posting rules
		$savingposting = new Savingposting;
		$savingposting->create_post_rules($product, $fee_income_acc, $saving_control_acc, $cash_account);
		return Redirect::route('savingproducts.index');
	}

	/**
	 * Display the specified savingproduct.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$savingproduct = Savingproduct::findOrFail($id);

		return View::make('savingproducts.show', compact('savingproduct'));
	}

	/**
	 * Show the form for editing the specified savingproduct.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$savingproduct = Savingproduct::find($id);
		$currencies = Currency::all();

		return View::make('savingproducts.edit', compact('savingproduct', 'currencies'));
	}
	/**
	*GET THE PRODUCT TO UPDATE
	*
	*/
	public function selectproduct($id){
		$product=Savingproduct::where('id','=',$id)->get()->first();
		$accounts = Account::all();
		$charges = Charge::all();
		$currencies = Currency::all();
		return View::make('savingproducts.selectproduct',compact('product','accounts','charges','currencies'));
	}
	/**
	 * Update the specified savingproduct in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//return $data = Input::all();
		$id=Input::get('product_id');
		$savingproduct = Savingproduct::where('id','=',$id)->get()->first();

		$validator = Validator::make($data = Input::all(), Savingproduct::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$savingproduct->name = Input::get('name');
		$savingproduct->shortname = Input::get('shortname');
		$savingproduct->opening_balance = Input::get('opening_balance');
		$savingproduct->type = Input::get('type');
		$savingproduct->currency = Input::get('currency');
        $savingproduct->Interest_Rate = Input::get('Interest_Rate');
        $savingproduct->min_amount = Input::get('minamount');
		$fee_income_acc = Input::get('fee_income_acc');
		$saving_control_acc = Input::get('saving_control_acc');
		$cash_account = Input::get('cash_account');
		$is_special=Input::get('is_special'); 
		$special_date=Input::get('special_date'); 
		if(empty($is_special)){ $savingproduct->is_special=0;}else{
			$savingproduct->is_special=1;
		}
		if(isset($special_date)){
			//$sd=explode("/",$special_date);  $d=$sd[0]; $m=$sd[1]; $y=$sd[2]; $special_date=$y."-".$m."-".$d;
			$savingproduct->special_date=$special_date;
		}
		// create posting rules
		$savingposting = new Savingposting;
		$savingposting->create_post_rules($savingproduct, $fee_income_acc, $saving_control_acc, $cash_account);
		$savingproduct->update();
		$surface="Saving product update successfully";
		$savingproducts = Savingproduct::all();
		return View::make('savingproducts.index', compact('savingproducts','surface'));
	}
	/**
	 * Remove the specified savingproduct from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$saveid=DB::table('savingaccounts')->where('savingproduct_id','=',$id)
		//->select('id')->get();
		Savingposting::where('savingproduct_id','=',$id)->delete();
		Savingaccount::where('savingproduct_id','=',$id)->delete();
		//Savingtransaction::where('savingaccount_id','=',$saveid->id)->delete();
		Savingproduct::destroy($id);
		$smash="Saving product deleted";
		$savingproducts = Savingproduct::all();
		return View::make('savingproducts.index', compact('savingproducts','smash'));
	}

}
