<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Loanaccount;
use App\Loantransaction;
use App\Member;
use App\Organization;
use App\Savingaccount;
use App\Savingproduct;
use App\Savingtransaction;
use App\Vehicle;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;

class SavingtransactionsController extends Controller
{

    /**
     * Display a listing of savingtransactions
     *
     * @return Response
     */
    public function index()
    {
        $savingtransactions = Savingtransaction::all();

        return View::make('savingtransactions.index', compact('savingtransactions'));
    }

    /**
     * Show the form for creating a new savingtransaction
     *
     * @return Response
     */
    public function create($id)
    {
        $savingaccount = Savingaccount::findOrFail($id);
        $savingproduct=Savingproduct::findOrFail( $savingaccount->savingproduct_id);
        //$credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->sum('amount');
        //$debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'debit')->sum('amount');
        $balance =Savingaccount::getAccountBalance($savingaccount);
        $availableBal = Savingaccount::getFinalAccountBalance($savingaccount);
        $interest_rate=(int)$savingproduct->Interest_Rate/100;
        $dbproductdate=$savingproduct->special_date; 
        $sd=explode(" ",$savingaccount->created_at); 
        $saccountdate=$sd[0];
        $sd2=explode("-",$saccountdate); 
        $sd2year=$sd2[0];
        $pd=explode("-",$dbproductdate);
         $pdyear=$pd[0]; 
         $pdmonth=$pd[1]; 
         $pdday=$pd[2]; 
         $cyear=date('Y');
        $firstyr_specialdate=$sd2year."-".$pdmonth."-".$pdday;
         $dateDue = Loantransaction::dateDue($saccountdate,$firstyr_specialdate);
        if($dateDue==0){
            $monthsDiff=Loantransaction::monthsDiff($firstyr_specialdate,date('Y-m-d'));
            $yearsDiff=round((int)$monthsDiff/12);
            if($yearsDiff>=1){
                $irate=$interest_rate*($yearsDiff+1);
            }else{$irate=$interest_rate;}
        }else{
            $sd3year=(int)$sd2year+1; 
            $secondyr_specialdate=$sd3year."-".$pdmonth."-".$pdday;
            $dateDue2=Loantransaction::dateDue(date('Y-m-d'),$secondyr_specialdate);
            if($dateDue2==1){
                $monthsDiff=Loantransaction::monthsDiff($secondyr_specialdate,date('Y-m-d'));
                $yearsDiff=round((int)$monthsDiff/12);
                if($yearsDiff>=1){
                    $irate=$interest_rate*($yearsDiff+1);
                }else{$irate=$interest_rate;} 
            }else{$irate=0;}
        }
        $interest=$availableBal*$irate;
        $member = $savingaccount->member;
        $vehicles=Vehicle::where('member_id',$member->id)->get();
        $guaranteed=Loanaccount::amountGuarantee($member->id); $loanBalance=Loantransaction::getMemberLoanBalance($savingaccount->member_id);
        $total_balance=$availableBal+(float)$interest;
        if (Auth::user()->user_type == 'member') {
            return View::make('css.membersavings', compact('savingaccount', 'member','vehicles', 'balance','guaranteed','interest','total_balance','loanBalance'));
        } else {
            return View::make('savingtransactions.create',compact('savingaccount', 'member', 'vehicles','balance','guaranteed','interest','total_balance','loanBalance'));
        }
    }

    /**
     * Store a newly created savingtransaction in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), Savingtransaction::$rules);
        #echo '<pre>'; print_r($data); echo '</pre>'; die;
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $date = Input::get('date');
        $transAmount = Input::get('amount');

        $savingaccount = Savingaccount::findOrFail(Input::get('account_id'));
        $guaranteed=Loanaccount::amountGuarantee($savingaccount->member_id);
        $savingsbal=Savingaccount::getUserSavingsBalance($savingaccount->member_id);
        $accountbal=Savingaccount::getFinalAccountBalance($savingaccount);
        $date = Input::get('date');
        $saving_amount = Input::get('saving_amount');
        $management_fee = Input::get('management_fee');
        $bank = Input::get('bank_reference');
        $type = Input::get('type');
        $description = Input::get('description');
        $vehicle = Input::get('vehicle_reg');
        $transacted_by = Input::get('transacted_by');
        $method = Input::get('pay_method');
        $member = Member::findOrFail($savingaccount->member_id);
        if($type=='debit'){
            if((float)$transAmount>(float)$accountbal){
                return Redirect::back()->withErrors('Cannot transact more than the available balance');
            }
        }#changeo


        Savingtransaction::transact($date, $savingaccount, $saving_amount, $management_fee, $type, $description, $transacted_by, $member,$bank,$method,$vehicle);


        return Redirect::to('savingtransactions/show/' . $savingaccount->id);
    }

    /**
     * Display the specified savingtransaction.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {            
        $account = Savingaccount::findOrFail($id);
        $interest=DB::table('savingproducts')->where('id', '=', $account->savingproduct_id)->pluck('Interest_Rate');
        $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('saving_amount');
        $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('saving_amount');

        $balance = $credit - $debit;

        return View::make('savingtransactions.show', compact('account','interest', 'balance'));
    }

    /**
     * Show the form for editing the specified savingtransaction.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $savingtransaction = Savingtransaction::find($id);

        return View::make('savingtransactions.edit', compact('savingtransaction'));
    }

    /**
     * Update the specified savingtransaction in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $savingtransaction = Savingtransaction::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Savingtransaction::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $savingtransaction->update($data);

        return Redirect::route('savingtransactions.index');
    }

    /**
     * Remove the specified savingtransaction from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Savingtransaction::destroy($id);

        return Redirect::route('savingtransactions.index');
    }


    public function receipt($id)
    {

        $transaction = Savingtransaction::findOrFail($id);

        $organization = Organization::findOrFail(1);

        $pdf = PDF::loadView('pdf.receipt', compact('transaction', 'organization'))->setPaper('a6')->setOrientation('potrait');;

        return $pdf->stream('receipt.pdf');


    }


    public function statement($id)
    {

        $account = Savingaccount::findOrFail($id);
        $interest=DB::table('savingproducts')->where('id', '=', $account->savingproduct_id)->pluck('Interest_Rate');

        $transactions = $account->transactions()->orderBy('date')->get();


        $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('saving_amount');
        $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('saving_amount');

        $balance = $credit - $debit;
    $guaranteed =Loanaccount::amountGuarantee($account->member_id);
        $organization = Organization::findOrFail(1);

        $pdf = PDF::loadView('pdf.statement', compact('transactions','guaranteed', 'organization','interest', 'account', 'balance'))->setPaper('a4')->setOrientation('potrait');;

        return $pdf->stream('statement.pdf');


    }


    public function import()
    {

        if (Input::hasFile('saving')) {

            $destination = public_path() . '/uploads/savings/';

            $filename = date('Y-m-d');

            $ext = Input::file('saving')->getClientOriginalExtension();
            $photo = $filename . '.csv';


            $file = Input::file('saving')->move($destination, $photo);

            //$file = public_path().'/uploads/savings/'.$filename;


            $row = 1;

            $saving = array();

            if (($handle = fopen(public_path() . '/uploads/savings/' . $photo, "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    echo '<pre>';

                    $saving[] = array('date' => $data[0], 'member' => $data[1], 'account' => $data[2], 'amount' => $data[3]);
                }

                $i = 1;

                for ($i = 1; $i < count($saving); $i++) {

                    $member = $saving[$i]['member'];
                    $account = $saving[$i]['account'];
                    $amount = $saving[$i]['amount'];
                    $date = $saving[$i]['date'];

                    $member_no = DB::table('members')->where('membership_no', '=', $member)->get();

                    if (empty($member_no)) {

                        return Redirect::to('import')->with('error', 'The member does not exist');
                    }

                    $account_no = DB::table('savingaccounts')->where('account_number', '=', $account)->get();


                    if (empty($account_no)) {

                        return Redirect::to('import')->with('error', 'The saving account does not exist');
                    }


                    Savingtransaction::importSavings($member_no, $date, $account_no, $amount,'','','','','');
                }

                fclose($handle);
            }


        }

        return Redirect::to('/')->with('notice', 'Member savings successfully imported');


    }

    public function importSavings()
    {
        $validator = Validator::make([
            'file' => Input::file('file'),
            'extension' => strtolower(pathinfo(Input::file('file')->getClientOriginalName(), PATHINFO_EXTENSION))
        ], [
            'file' => 'required',
            //'extension' => 'required|in:xls,xlsx,csv'
            //upload csv files only
            'extension' => 'required|in:csv'

        ]);

        if ($validator->fails()) {
            return Redirect::back()->with('errors', $validator->messages());
        }

        $file = Input::file('file');
        $filename = str_random(16) . "." . $file->getClientOriginalExtension();
        $destination = public_path('/migrations/savings');
        $file->move($destination, $filename);

        $error_flag = false;

        Excel::filter('chunk')->selectSheetsByIndex(0)->load(public_path('migrations/savings/') . $filename)->chunk(250, function ($results) {
            include(app_path() . '/views/AfricasTalkingGateway.php');
            foreach ($results as $result) {
                if ($result->account_number != null
                    && $result->date != null
                    && $result->saving_amount != null
                    && $result->type != null
                    && $result->description != null) {
                    $account_number = trim(explode(':', $result->account_number)[1]);
                    $vehicle_reg = trim(explode(':', $result->vehicle_registration)[1]);

                    $account = Savingaccount::where('account_number', $account_number)->first();
                    $vehicle =Vehicle::where('regno', $vehicle_reg)->first();
                    
                    if ($account != null) {
                        $member_no = Member::where('id', $account->member_id)->first();
                        $account_no = Savingaccount::where('account_number', $account_number)->first();

                        Savingtransaction::importSavings($member_no, date('Y-m-d', strtotime($result->date)), $account_no, $result->saving_amount,$result->management_fee, $result->description,$result->bank_ref,$result->transaction_method,$vehicle);
                    } else $error_flag = true;
                } else $error_flag = true;
     }
        });

        if ($error_flag) {
            $umessage = 'Successfully migrated. Some entries were ignored.';
        } else {
            $umessage = 'Successfully migrated.';
        }

        return Redirect::back()->with(compact('umessage'));
    }


    public function void($id)
    {

        Savingtransaction::destroy($id);

        return Redirect::back()->with('notice', ' transaction has been successfully voided');
    }


}
