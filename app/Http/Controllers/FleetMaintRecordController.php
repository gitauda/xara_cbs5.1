<?php

/**
 * FleetMaintRecordController Class
 *
 * Implements actions regarding fleet management
 */
class FleetMaintRecordController extends Controller
{
    /**
    * display a list of maintRecords
    */
    public function index(){
        $maintRecords=FleetMaintRecord::all();
        return View::make('fleetMgnt.fleetMaintRecords.index',compact('maintRecords'));
    }

    /**
    * display the edit page
    */
    
    public function show($id){

        $record = FleetMaintRecord::findorfail($id);
        $vehicles = Vehicle::all();
       Audit::logAuditwithfewargs('fleet_maintenanceRec_view', Confide::User()->username.' viewed record '.$record->code, '0');
       return View::make('fleetMgnt.fleetMaintRecords.show', compact('record','vehicles'));
    }

    public function edit($id){
       $record = FleetMaintRecord::findorfail($id); 
       $vehicles = Vehicle::where('id','!=',$record->vehicle->id)->get();
       return View::make('fleetMgnt.fleetMaintRecords.edit', compact('record','vehicles'));
    }

     /**
    * updates the collections
    */
    public function update($id){

        $record = FleetMaintRecord::findorfail($id); 
        //associate($account);
        $record->garage = Input::get('garage');
        $record->mechanic =  Input::get('mechanic');
        $record->vehicle_id = Input::get('vehicle_id');
        $record->description =  Input::get('description');
        $record->cost = Input::get('cost');
        $record->date = Input::get('maint_date');
        $record->update();

        Audit::logAuditwithfewargs('fleet_maintenanceRec_update', Confide::User()->username.' updated record '.$record->code, '0');

        return Redirect::route('fleetMaintRecords.index')->with('success','Record '.$record->code.' successfully updated');

    }




    /**
     * Displays the form for collection creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {

        $vehicles = Vehicle::all(); 
        
        return View::make('fleetMgnt.fleetMaintRecords.create', compact('vehicles'));
    }

    /**
     * Stores new collection
     *
     * @return  Illuminate\Http\Response
     */
    public function store()
    {
        $lastRec=FleetMaintRecord::orderBy('id','DESC')->first(); 
        $code=Vehicle::unicode($lastRec);
        $record = new FleetMaintRecord;
        //associate($account);
        $record->garage = Input::get('garage');
        $record->mechanic =  Input::get('mechanic');
        $record->vehicle_id = Input::get('vehicle_id');
        $record->description =  Input::get('description');
        $record->cost = Input::get('cost');
        $record->date = Input::get('maint_date');
        $record->code = "mrec_".$code;
        $record->save();

        Audit::logAuditwithfewargs('fleet_maintenanceRec_new',Confide::User()->username.' added a record '.$record->code, '0');

        return Redirect::route('fleetMaintRecords.index')->with('success','Record '.$record->code.' successfully added.');
    }

    /**
    * Delete the collection
    *
    */

    public function destroy($id){

        $record = FleetMaintRecord::findorfail($id);

        $record->delete();

        Audit::logAuditwithfewargs('fleet_maintenanceRec_delete',Confide::User()->username.' deleted record '.$record->code, '0');

        return Redirect::route('fleetMaintRecords.index')->with('success','Record successfully removed');
    }


}