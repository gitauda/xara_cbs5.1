<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Member;
use App\Savingaccount;
use App\Savingproduct;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SavingaccountsController extends Controller {

	/**
	 * Display a listing of savingaccounts
	 *
	 * @return Response
	 */
	public function index()
	{
		$savingaccounts = Savingaccount::all();

		return View::make('savingaccounts.index', compact('savingaccounts'));
	}

	/**
	 * Show the form for creating a new savingaccount
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$member = Member::findOrFail($id);
		$savingproducts = Savingproduct::all(); 
		return View::make('savingaccounts.create', compact('member', 'savingproducts'));
	}
       

	/**
	 * Store a newly created savingaccount in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Savingaccount::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}


		$member = Member::findOrFail(Input::get('member_id'));
		$savingproduct = Savingproduct::findOrFail(Input::get('savingproduct_id'));
                if(!empty(input::get('account_no')))
                 {$acc_no = input::get('account_no');
                    }
                   else{
		$acc_no = $savingproduct->shortname.'000000'.$member->membership_no;
                  }
		$accnoavail=Savingaccount::where('account_number', '=', $acc_no)->count();
                $accavail=Savingaccount::where('savingproduct_id', '=', $savingproduct->id)->where('member_id', '=', $member->id)->count();
                
		if($accavail>0){
			return Redirect::back()->withErrors('Savings  Account for the member number already exists!');
		}
                if($accnoavail>0){
			return Redirect::back()->withErrors('Savings  Account number  number already exists!');
		}

		$savingaccount = new Savingaccount;
		$savingaccount->member()->associate($member); 
		$savingaccount->savingproduct()->associate($savingproduct);
		$savingaccount->account_number = $acc_no;
		$savingaccount->save();
		
		//return Redirect::route('savingaccounts.index');
		//return Redirect::route('member/savingaccounts/{$member->id}');
		return View::make('savingaccounts.memberaccounts', compact('member'));
	}

	/**
	 * Display the specified savingaccount.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$savingaccount = Savingaccount::findOrFail($id);

		return View::make('savingaccounts.show', compact('savingaccount'));
	}

	/**
	 * Show the form for editing the specified savingaccount.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$savingaccount = Savingaccount::find($id);

		return View::make('savingaccounts.edit', compact('savingaccount'));
	}

	/**
	 * Update the specified savingaccount in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$savingaccount = Savingaccount::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Savingaccount::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$savingaccount->update($data);

		return Redirect::route('savingaccounts.index');
	}

	/**
	 * Remove the specified savingaccount from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$savingaccount = Savingaccount::findOrFail($id);
		Savingaccount::destroy($id);  
		$member=Member::findorFail($savingaccount->member_id);
		//return Redirect::route('savingaccounts.index');
		return View::make('savingaccounts.memberaccounts', compact('member'));
	}



	public function memberaccounts($id){
		$member = Member::findOrFail($id);

		return View::make('savingaccounts.memberaccounts', compact('member'));
	}

}
