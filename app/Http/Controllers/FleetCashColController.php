<?php namespace App\Http\Controllers;

use App\Audit;
use App\FleetCashCollection;
use App\Http\Controllers\Controller;
use App\Role;
use App\Vehicle;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

/**
 * FleetCashController Class
 *
 * Implements actions regarding fleet management
 */
class FleetCashColController extends Controller
{
    /**
    * display a list of collections
    */
    public function index(){
        $roles = Role::all(); 
        $collections=FleetCashCollection::all();
        return View::make('fleetMgnt.fleetCashCollection.index',compact('collections'));
    }

    /**
    * display the edit page
    */
    
    public function edit($id){
        $collection = FleetCashCollection::findorfail($id);
        $vehicles = Vehicle::where('id','!=',$collection->vehicle_id)->get(); 
       return View::make('fleetMgnt.fleetCashCollection.edit', compact('collection','vehicles'));
    }


     /**
    * updates the collections
    */
    public function update($id){

        $collection = FleetCashCollection::findorfail($id);
        //associate($account);
        $collection->collector_id = Input::get('collector');
        $collection->vehicle_id = Input::get('vehicle_id');
        $collection->amount =  Input::get('amount');
        $collection->date =  Input::get('date');
        $collection->update();

        Audit::logAuditwithfewargs('fleet_cash_coollection_update',Input::get('amount').' was collected for vehicle '.$collection->vehicle->regno, '0');

        return Redirect::route('fleetCashCollection.index')->with('success','Collection successfully updated');

    }




    /**
     * Displays the form for collection creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {

        $vehicles = Vehicle::all(); 
        
        return View::make('fleetMgnt.fleetCashCollection.create', compact('vehicles'));
    }

    /**
     * Stores new collection
     *
     * @return  Illuminate\Http\Response
     */
    public function store()
    {

        $collection = new FleetCashCollection;
        //associate($account);
        $collection->collector_id = Input::get('collector');
        $collection->vehicle_id = Input::get('vehicle_id');
        $collection->amount =  Input::get('amount');
        $collection->date =  Input::get('date');
        $collection->save();

        Audit::logAuditwithfewargs('fleet_cash_collection',Input::get('amount').' was collected for vehicle '.$collection->vehicle->regno, '0');

        return Redirect::route('fleetCashCollection.index')->with('success','Collection successfully updated');
    }

    /**
    * Delete the collection
    *
    */

    public function destroy($id){

        $collection = FleetCashCollection::find($id);

        $collection->delete();

        Audit::logAuditwithfewargs('fleet_cash_collection_delete',$collection->amount.' collection was removed for vehicle '.$collection->vehicle->regno, '0');

        return Redirect::route('fleetCashCollection.index')->with('success','Collection successfully removed');
    }

    public function show($id){

        $collection = FleetCashCollection::findorfail($id);
        $vehicles=Vehicle::all();
       Audit::logAuditwithfewargs('fleet_cash_collection_view', 'viewed a collection of vehicle '.$collection->vehicle->regno.' of '.$collection->amount, '0');
       return View::make('fleetMgnt.fleetCashCollection.show', compact('collection','vehicles'));
    }

}