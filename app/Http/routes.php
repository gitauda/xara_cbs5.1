<?php
namespace App;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Classes\PHPExcel;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_NamedRange;

Route::get('/', function () {

    $count = count(User::all());

    if ($count == 0) {

        return View::make('signup');
    }

    //Member::dormantDeactivation();
    if (Auth::user()) {
        return  Redirect::to('/dashboard');
    } else {
        return View::make('login');
    }

});

//Links outside filter
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');

//first change before to middleware 
//then create middleware with php artisan make:middleware loggedin
//write ur custom code in the middleware class
//register the middleware in App/Http/Kernel
Route::group(['middleware' => 'loggedin'], function () {
    Route::get('/dashboard', function () {
      if (Auth::user()) { 
        $organization = Organization::find(1);
        $installationdate=date('Y-m-d',strtotime($organization->installation_date));
    
        $splitdate = explode('-', $installationdate);
        //split to obtain month and day from the installation date
        $day=$splitdate[2];
        $month=$splitdate[1];
        $year=date('Y');
        // The due date for annual subscription fee.
        $date =date('d-F-Y',strtotime($day.'-'.$month.'-'.$year));
        $notificationperiod =date('d-F-Y',strtotime($date.'-20 days'));
        //set the dates parameters for comparison purposes to avoid incorrct output which occurs from unformatted date comparison
        $todaydate=date('d-F-Y');
        $not_compare=strtotime($notificationperiod);
        $todaydate_compare=strtotime($todaydate);
        $date_compare=strtotime($date);
        // $notificationperiod =date('Y-m-d',strtotime($notification->created_at));
    
        /*today's date in y-m-d format*/
        $dateformat=date('Y-m-d');
        //obtain the two digit number of today's date for the purpose of using modulus operator to facilitate skipping a day during notifications period.
        $splittodaydate = explode('-', $dateformat);
        $dayofthemonth=$splittodaydate[2];
        $notification=DB::table('notifications')->where('created_at','>=',$dateformat)->orderBy('created_at','DESC')->first();
        //deactivate dormant users
        //Member::dormantDeactivation();
        //end Deactivation ofusers
                if ($todaydate_compare>=$not_compare && $todaydate_compare<=$date_compare) 
            //if today's notification does not exist and the day is even(skip odd day)  
            {
                if(empty($notification)&&$dayofthemonth%2===0)
                {
             $userid= Auth::user()->id;  
                $key = md5(uniqid());
                Notification::notifyUser($userid,"Hello, Please make payments for annual suscription fees before ".$date,"payment reminder","paymentnotification/invoiceshow/".$key."/".$userid,$key);
    
               } 
            }
    
            if (Auth::user()->user_type == 'admin') {
                $members = Member::all();
                //Grab all available loans 
                $loanaccounts =Loanaccount::all(); 
                foreach ($loanaccounts as $loanaccount) {
                    $period = $loanaccount->period;
                    $endpoint = 30 * $period;
                    $end = '+$endpoint';
                    //Take member ID
                    $id = $loanaccount->member_id;
                    //Get all the member details
                    $member = Member::find($id);
                    //Take the repayment period
                    $startdate = date('Y-m-d', strtotime($loanaccount->repayment_start_date));
                    for ($date = $startdate; $date < date('Y-m-d', strtotime($date . "+$endpoint days")); $date = date('Y-m-d', strtotime($date . "+28 days"))) {
                        //Reset execution limit
                        set_time_limit(60);
                        //Get current month
                        $now = date('Y-m-d');
                        $today = date('Y-m-d', strtotime($now));
                        $month = date('m');
                        //Get payment date
                        $paydate = date('Y-m-d', strtotime($date . "+30 days"));
                        $defaultdate = date('Y-m-d', strtotime($date . "+31 days"));
                        //Get Payment month
                        $pay_month = date('m', strtotime($paydate));
                        //When payment month equals current month send an email
                        if ($month == $pay_month && $today <= $paydate) {
                            /*Mail::send( 'emails.notification', array('name'=>$member->name,
                            'pay_date'=>date('d-F-Y',strtotime($date."+30 days"))), function( $message )
                             use ($member){
                                 $message->to($member->email)->subject( 'Loan Repayment Notification' );
                                });  */
                            break;
                        } else if ($month == $pay_month && $today == $defaultdate) {
                            $loanaccount->is_defaulted = TRUE;
                            $loanaccount->save();
                            $defaultrepay =new Loanrepayment;
                            $defaultrepay->loanaccount_id = $loanaccount->id;
                            $defaultrepay->date = $defaultdate;
                            $defaultrepay->principal_paid = 0.00;
                            $defaultrepay->interest_paid = 0.00;
                            $defaultrepay->default_period = date('m-Y', strtotime($date . "+31 days"));
                            $defaultrepay->save();
                        }
                        break;
                    }
                }
                return View::make('dashboard', compact('members'));
            }
    
            if (Auth::user()->user_type == 'teller') {
    
                $members = Member::all();
    
                return View::make('tellers.dashboard', compact('members'));
    
            }
    
            if (Auth::user()->user_type == 'credit') {
    
                $loanaccounts = Loanaccount::all();
    
                return View::make('creditS.dashboard', compact('loanaccounts'));
    
            }
    
            if (Auth::user()->user_type == 'member') {
    
                $loans = LoanProduct::all();
                $member = Member::where('email', Auth::user()->email)->first();
                $products = Product::all();
                //$rproducts = Product::getRemoteProducts();
    
                return View::make('css.memberindex', compact('loans', 'member', 'products'));
    
            }
    
    
        } else {
            return View::make('login');
        }
    });

    Route::resource('banks', 'BanksController');
    Route::post('banks/update/{id}', 'BanksController@update');
    Route::get('banks/delete/{id}', 'BanksController@destroy');
    Route::get('banks/edit/{id}', 'BanksController@edit');
  
    /*
    * departments routes
    */
  
    Route::resource('departments', 'DepartmentsController');
    Route::post('departments/update/{id}', 'DepartmentsController@update');
    Route::get('departments/delete/{id}', 'DepartmentsController@destroy');
    Route::get('departments/edit/{id}', 'DepartmentsController@edit');
  
  
    /*
    * bank branch routes
    */
  
    Route::resource('bank_branch', 'BankBranchController');
    Route::post('bank_branch/update/{id}', 'BankBranchController@update');
    Route::get('bank_branch/delete/{id}', 'BankBranchController@destroy');
    Route::get('bank_branch/edit/{id}', 'BankBranchController@edit');
  
    /*
    * allowances routes
    */
  
    Route::resource('allowances', 'AllowancesController');
    Route::post('allowances/update/{id}', 'AllowancesController@update');
    Route::get('allowances/delete/{id}', 'AllowancesController@destroy');
    Route::get('allowances/edit/{id}', 'AllowancesController@edit');
  
    /*
    * earningsettings routes
    */
  
    Route::resource('earningsettings', 'EarningsettingsController');
    Route::post('earningsettings/update/{id}', 'EarningsettingsController@update');
    Route::get('earningsettings/delete/{id}', 'EarningsettingsController@destroy');
    Route::get('earningsettings/edit/{id}', 'EarningsettingsController@edit');
  
    /*
    * benefits setting routes
    */
  
    Route::resource('benefitsettings', 'BenefitSettingsController');
    Route::post('benefitsettings/update/{id}', 'BenefitSettingsController@update');
    Route::get('benefitsettings/delete/{id}', 'BenefitSettingsController@destroy');
    Route::get('benefitsettings/edit/{id}', 'BenefitSettingsController@edit');
  
    /*
    * reliefs routes
    */
  
    Route::resource('reliefs', 'ReliefsController');
    Route::post('reliefs/update/{id}', 'ReliefsController@update');
    Route::get('reliefs/delete/{id}', 'ReliefsController@destroy');
    Route::get('reliefs/edit/{id}', 'ReliefsController@edit');
  
    /*
    * deductions routes
    */
  
    Route::resource('deductions', 'DeductionsController');
    Route::post('deductions/update/{id}', 'DeductionsController@update');
    Route::get('deductions/delete/{id}', 'DeductionsController@destroy');
    Route::get('deductions/edit/{id}', 'DeductionsController@edit');
  
    /*
    * nontaxables routes
    */
  
    Route::resource('nontaxables', 'NonTaxablesController');
    Route::post('nontaxables/update/{id}', 'NonTaxablesController@update');
    Route::get('nontaxables/delete/{id}', 'NonTaxablesController@destroy');
    Route::get('nontaxables/edit/{id}', 'NonTaxablesController@edit');
  
    /*
    * nssf routes
    */
  
    Route::resource('nssf', 'NssfController');
    Route::post('nssf/update/{id}', 'NssfController@update');
    Route::get('nssf/delete/{id}', 'NssfController@destroy');
    Route::get('nssf/edit/{id}', 'NssfController@edit');
  
    /*
    * nhif routes
    */
  
    Route::resource('nhif', 'NhifController');
    Route::post('nhif/update/{id}', 'NhifController@update');
    Route::get('nhif/delete/{id}', 'NhifController@destroy');
    Route::get('nhif/edit/{id}', 'NhifController@edit');
  
    /*
    * job group routes
    */
  
    Route::resource('job_group', 'JobGroupController');
    Route::post('job_group/update/{id}', 'JobGroupController@update');
    Route::get('job_group/delete/{id}', 'JobGroupController@destroy');
    Route::get('job_group/edit/{id}', 'JobGroupController@edit');
    Route::get('job_group/show/{id}', 'JobGroupController@show');
   
    /**
     * Employee routes
     */
    Route::resource('employees', 'EmployeesController');
    Route::get('employees/show/{id}', 'EmployeesController@show');

    /**CREATE EMPLOYEE MIDDLEWARE */
    Route::group(['middleware' => 'create_employee'], function() {
        Route::get('employees/create', 'EmployeesController@create');
    });

    Route::get('employees/edit/{id}', 'EmployeesController@edit');
    Route::post('employees/update/{id}', 'EmployeesController@update');
    Route::get('employees/delete/{id}', 'EmployeesController@destroy');

   
    /*
    * employee type routes
    */  
    Route::resource('employee_type', 'EmployeeTypeController');
    Route::post('employee_type/update/{id}', 'EmployeeTypeController@update');
    Route::get('employee_type/delete/{id}', 'EmployeeTypeController@destroy');
    Route::get('employee_type/edit/{id}', 'EmployeeTypeController@edit');
    /*
    * employee earnings routes
    */
  
    Route::resource('other_earnings', 'EarningsController');
    Route::post('other_earnings/update/{id}', 'EarningsController@update');
    Route::get('other_earnings/delete/{id}', 'EarningsController@destroy');
    Route::get('other_earnings/edit/{id}', 'EarningsController@edit');
    Route::get('other_earnings/view/{id}', 'EarningsController@view');
    Route::post('createEarning', 'EarningsController@createearning');
  
    /*
    * employee reliefs routes
    */
  
    Route::resource('employee_relief', 'EmployeeReliefController');
    Route::post('employee_relief/update/{id}', 'EmployeeReliefController@update');
    Route::get('employee_relief/delete/{id}', 'EmployeeReliefController@destroy');
    Route::get('employee_relief/edit/{id}', 'EmployeeReliefController@edit');
    Route::get('employee_relief/view/{id}', 'EmployeeReliefController@view');
    Route::post('createRelief', 'EmployeeReliefController@createrelief');
     /*
    * employee allowances routes
    */
  
    Route::resource('employee_allowances', 'EmployeeAllowancesController');
    Route::post('employee_allowances/update/{id}', 'EmployeeAllowancesController@update');
    Route::get('employee_allowances/delete/{id}', 'EmployeeAllowancesController@destroy');
    Route::get('employee_allowances/edit/{id}', 'EmployeeAllowancesController@edit');
    Route::get('employee_allowances/view/{id}', 'EmployeeAllowancesController@view');
    Route::post('createAllowance', 'EmployeeAllowancesController@createallowance');
    Route::post('reloaddata', 'EmployeeAllowancesController@display');
  
    /*
    * employee nontaxables routes
    */
  
    Route::resource('employeenontaxables', 'EmployeeNonTaxableController');
    Route::post('employeenontaxables/update/{id}', 'EmployeeNonTaxableController@update');
    Route::get('employeenontaxables/delete/{id}', 'EmployeeNonTaxableController@destroy');
    Route::get('employeenontaxables/edit/{id}', 'EmployeeNonTaxableController@edit');
    Route::get('employeenontaxables/view/{id}', 'EmployeeNonTaxableController@view');
    Route::post('createNontaxable', 'EmployeeNonTaxableController@createnontaxable');
  
    /*
    * employee deductions routes
    */
  
    Route::resource('employee_deductions', 'EmployeeDeductionsController');
    Route::post('employee_deductions/update/{id}', 'EmployeeDeductionsController@update');
    Route::get('employee_deductions/delete/{id}', 'EmployeeDeductionsController@destroy');
    Route::get('employee_deductions/edit/{id}', 'EmployeeDeductionsController@edit');
    Route::get('employee_deductions/view/{id}', 'EmployeeDeductionsController@view');
    Route::post('createDeduction', 'EmployeeDeductionsController@creatededuction');
 
    /*
    * advance routes
    */
    Route::resource('advance', 'AdvanceController');
    Route::post('deleteadvance', 'AdvanceController@del_exist');
    Route::post('advance/preview', 'AdvanceController@create');
    Route::post('createAccount', 'AdvanceController@createaccount');

    /*
    * occurence settings routes
    */
    Route::resource('occurencesettings', 'OccurencesettingsController');
    Route::post('occurencesettings/update/{id}', 'OccurencesettingsController@update');
    Route::get('occurencesettings/delete/{id}', 'OccurencesettingsController@destroy');
    Route::get('occurencesettings/edit/{id}', 'OccurencesettingsController@edit');
    
    /**
     * Occurence routes
     */
    Route::resource('occurences', 'OccurencesController');
    Route::post('occurences/update/{id}', 'OccurencesController@update');
    Route::get('occurences/delete/{id}', 'OccurencesController@destroy');
    Route::get('occurences/edit/{id}', 'OccurencesController@edit');
    Route::get('occurences/view/{id}', 'OccurencesController@view');
    Route::get('occurences/download/{id}', 'OccurencesController@getDownload');
    Route::post('createOccurence', 'OccurencesController@createoccurence');
    

    /*
    * citizenship routes
    */
  
    Route::resource('citizenships', 'CitizenshipController');
    Route::post('citizenships/update/{id}', 'CitizenshipController@update');
    Route::get('citizenships/delete/{id}', 'CitizenshipController@destroy');
    Route::get('citizenships/edit/{id}', 'CitizenshipController@edit');
  
  
    Route::get('authorizepurchaseorder/{id}','ErpReportsController@authorizepurchaseorder');
    Route::get('approvepurchaseorder/{id}','ErpReportsController@approvepurchaseorder');
    Route::get('reviewpurchaseorder/{id}','ErpReportsController@reviewpurchaseorder');
    Route::get('submitpurchaseorder/{id}','ErpReportsController@submitpurchaseorder');
  
   

    /*
    * payroll routes
    */
    Route::resource('payroll', 'PayrollController');
    Route::post('deleterow', 'PayrollController@del_exist');
    Route::post('showrecord', 'PayrollController@display');
    Route::post('shownet', 'PayrollController@disp');
    Route::post('showgross', 'PayrollController@dispgross');
    Route::post('payroll/preview', 'PayrollController@create');
    Route::get('payrollpreviewprint/{period}', 'PayrollController@previewprint');
    Route::get('unlockpayroll/index', 'PayrollController@unlockindex');
    Route::get('payroll/view/{id}', 'PayrollController@viewpayroll');
    Route::get('unlockpayroll/{id}', 'PayrollController@unlockpayroll');
    Route::post('unlockpayroll', 'PayrollController@dounlockpayroll');
    Route::post('createNewAccount', 'PayrollController@createaccount');
  
    Route::get('payrollcalculator', function(){
      $currency = Currency::find(1);
      return View::make('payroll.payroll_calculator',compact('currency'));
  
    });
  

  
    Route::get('deactives', function(){
  
      $employees = Employee::getDeactiveEmployee();
  
      return View::make('employees.activate', compact('employees'));
  
    } );

    /**
     * Advance reports
     */
    Route::get('advanceReports', function(){
  
        return View::make('employees.advancereports');
    });
  
    /**
     * Payroll reports
     */
    Route::get('payrollReports', function(){
  
        return View::make('employees.payrollreports');
    });

    Route::get('payrollReports/selectYear', function(){
        $branches = Branch::whereNull('organization_id')->orWhere('organization_id',Auth::user()->organization_id)->get();
        $departments = Department::whereNull('organization_id')->orWhere('organization_id',Auth::user()->organization_id)->get();
        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();
        return View::make('pdf.p9Select',compact('employees','branches','departments'));
    });
    
    /** 
     * statutory reports
    */
    Route::get('statutoryReports', function(){
        return View::make('employees.statutoryreports');
    });

    /***************
     * Employee promotions
     */
    Route::get('employee_promotion', function(){
      $promotions = Promotion::whereNull('organization_id')->orWhere('organization_id',Auth::user()->organization_id)->get();
  
      Audit::logaudit('Promotions', 'view', 'viewed promotions');

      return View::make('promotions.index', compact('promotions'));

    });

    //email payslip routes
    //Route::post('employee_promotion', 'EmployeesController@promote_transfer');
    Route::get('email/payslip', 'payslipEmailController@index');
    Route::post('email/payslip/employees', 'payslipEmailController@sendEmail');


    /** 
     * employee leave application 
     * surbodinate leave
     * check payslip
     * employee leave balance
     */
    Route::get('css/leave', function(){
  
        $employeeid = DB::table('employee')
        ->where('organization_id',Auth::user()->organization_id)
        ->where('personal_file_number', '=', Auth::user()->username)
        ->pluck('id');
    
    
        $employee = Employee::findorfail($employeeid);
    
        $leaveapplications = DB::table('leaveapplications')->where('organization_id',Auth::user()->organization_id)->where('employee_id', '=', $employee->id)->get();
    
        return View::make('css.leave', compact('employee', 'leaveapplications'));
    });
    
    Route::get('css/subordinateleave', function(){
    
        $employeeid = DB::table('employee')->where('personal_file_number', '=', Auth::user()->username)->pluck('id');
        $c = Supervisor::where('supervisor_id', $employeeid)->count();
    
        $employee = Employee::findorfail($employeeid);
    
        //$leaveapplications = DB::table('leaveapplications')->where('employee_id', '=', $employee->id)->get();
    
        return View::make('css.approveleave', compact('c','leaveapplications'));
    });
    
    Route::get('css/payslips', function(){
    
        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)
        ->where('personal_file_number', '=', Auth::user()->username)
        ->pluck('id');
    
        $employee = Employee::findorfail($employeeid);
    
        return View::make('css.payslip', compact('employee'));
    });
    
    Route::get('employeeleave/view/{id}', 'LeaveapplicationsController@cssleaveapprove');
    Route::get('supervisorapproval/{id}', 'LeaveapplicationsController@supervisorapprove');
    Route::get('supervisorreject/{id}', 'LeaveapplicationsController@supervisorreject');


    Route::get('css/leaveapply', function(){

        $employeeid = DB::table('employee')
        ->where('organization_id',Auth::user()->organization_id)
        ->where('personal_file_number', '=', Auth::user()->username)
        ->pluck('id');

        $employee = Employee::findorfail($employeeid);
        $leavetypes = Leavetype::where('organization_id',Auth::user()->organization_id)->get();

        return View::make('css.leaveapply', compact('employee', 'leavetypes'));
    });

    Route::get('css/balances', function(){

        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', Auth::user()->username)->pluck('id');

        $employee = Employee::findorfail($employeeid);
        $leavetypes = Leavetype::where('organization_id',Auth::user()->organization_id)->get();

        return View::make('css.balances', compact('employee', 'leavetypes'));
    });


    /**
     * Reports Controller
     */
    Route::get('itax/download', 'ReportsController@getDownload');
  
    Route::get('statement', 'StatementController@index');
    Route::get('statement/report', 'ReportsController@statement');
  
  
    Route::get('reports/negativeleaves', 'ReportsController@negativeleaves');
  
    Route::get('reports/selectEmployeeStatus', 'ReportsController@selstate');
    Route::post('reports/employeelist', 'ReportsController@employees');
    Route::get('employee/select', 'ReportsController@emp_id');
    Route::post('reports/employee', 'ReportsController@individual');
    Route::get('reports/compliance/selectEmployee', 'ReportsController@selEmpDisc');
    Route::post('reports/compliance', 'ReportsController@discipline');
    Route::get('reports/promotion/selectEmployee', 'ReportsController@selPromEmp');
    Route::post('reports/promotion', 'ReportsController@promotion');
    Route::get('payrollReports/selectPeriod', 'ReportsController@period_payslip');
    Route::post('payrollReports/payslip', 'ReportsController@payslip');
    Route::get('payrollReports/selectAllowance', 'ReportsController@employee_allowances');
    Route::post('payrollReports/allowances', 'ReportsController@allowances');
    Route::get('payrollReports/selectEarning', 'ReportsController@employee_earnings');
    Route::post('payrollReports/earnings', 'ReportsController@earnings');
    Route::get('payrollReports/selectOvertime', 'ReportsController@employee_overtimes');
    Route::post('payrollReports/overtimes', 'ReportsController@overtimes');
    Route::get('payrollReports/selectRelief', 'ReportsController@employee_reliefs');
    Route::post('payrollReports/reliefs', 'ReportsController@reliefs');
    Route::get('payrollReports/selectDeduction', 'ReportsController@employee_deductions');
    Route::post('payrollReports/deductions', 'ReportsController@deductions');
    Route::get('payrollReports/selectnontaxableincome', 'ReportsController@employeenontaxableselect');
    Route::post('payrollReports/nontaxables', 'ReportsController@employeenontaxables');
    Route::get('payrollReports/selectPayePeriod', 'ReportsController@period_paye');
    Route::post('payrollReports/payeReturns', 'ReportsController@payeReturns');
    Route::post('payrollReports/p9form', 'ReportsController@p9form');
    Route::get('payrollReports/selectRemittancePeriod', 'ReportsController@period_rem');
    Route::post('payrollReports/payRemittances', 'ReportsController@payeRems');
    Route::get('payrollReports/selectSummaryPeriod', 'ReportsController@period_summary');
    Route::post('payrollReports/payrollSummary', 'ReportsController@paySummary');
    Route::get('payrollReports/selectNssfPeriod', 'ReportsController@period_nssf');
    Route::post('payrollReports/nssfReturns', 'ReportsController@nssfReturns');
    Route::get('payrollReports/selectNhifPeriod', 'ReportsController@period_nhif');
    Route::post('payrollReports/nhifReturns', 'ReportsController@nhifReturns');
    Route::get('payrollReports/selectNssfExcelPeriod', 'ReportsController@period_excel');
    Route::post('payrollReports/nssfExcel', 'ReportsController@export');
    Route::get('reports/selectEmployeeOccurence', 'ReportsController@selEmp');
    Route::post('reports/occurence', 'ReportsController@occurence');
    Route::get('reports/CompanyProperty/selectPeriod', 'ReportsController@propertyperiod');
    Route::post('reports/companyproperty', 'ReportsController@property');
    Route::get('reports/Appraisals/selectPeriod', 'ReportsController@appraisalperiod');
    Route::post('reports/appraisal', 'ReportsController@appraisal');
    Route::get('reports/nextofkin/selectEmployee', 'ReportsController@selempkin');
    Route::post('reports/EmployeeKin', 'ReportsController@nextkin');
    Route::get('advanceReports/selectRemittancePeriod', 'ReportsController@period_advrem');
    Route::post('advanceReports/advanceRemittances', 'ReportsController@payeAdvRems');
    Route::get('advanceReports/selectSummaryPeriod', 'ReportsController@period_advsummary');
    Route::post('advanceReports/advanceSummary', 'ReportsController@payAdvSummary');
  
    /**
     * Currencies
     */
    Route::resource('currencies', 'CurrenciesController');
    Route::get('currencies/edit/{id}', 'CurrenciesController@edit');
    Route::post('currencies/update/{id}', 'CurrenciesController@update');
    Route::get('currencies/delete/{id}', 'CurrenciesController@destroy');
    Route::get('currencies/create', 'CurrenciesController@create');
  
    /**
     * Compliance/ DisciplineController
     */
    Route::resource('compliance', 'DisciplineController');
    Route::get('compliance/edit/{id}', 'DisciplineController@edit');
    Route::post('compliance/update/{id}', 'DisciplineController@update');
    Route::get('compliance/delete/{id}', 'DisciplineController@destroy');
    Route::get('compliance/create', 'DisciplineController@create');
    Route::get('compliance/show/{id}', 'DisciplineController@show');
  
    /**
     * Promotions controller
     */
    Route::resource('promotions', 'PromotionsController');
    Route::get('promotions/edit/{id}', 'PromotionsController@edit');
    Route::post('promotions/update/{id}', 'PromotionsController@update');
    Route::get('promotions/delete/{id}', 'PromotionsController@destroy');
    Route::get('promotions/create', 'PromotionsController@create');
    Route::get('promotions/letters/{id}', 'PromotionsController@promotionletter');
    Route::get('transfer/letters/{id}', 'PromotionsController@transferletter');
    Route::get('promotions/show/{id}', 'PromotionsController@show');
 
    /**
     * Employee template
     */
    Route::get('template/employees', function(){

        $bank_data = Bank::where('organization_id',Auth::user()->organization_id)->get();

        $data = Employee::where('organization_id',Auth::user()->organization_id)->get();

        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();

        $bankbranch_data = BBranch::where('organization_id',Auth::user()->organization_id)->get();

        $branch_data = Branch::where('organization_id',Auth::user()->organization_id)->get();

        $department_data = Department::where('organization_id',Auth::user()->organization_id)->get();

        $employeetype_data = EType::where('organization_id',Auth::user()->organization_id)->get();

        $jobgroup_data = Jobgroup::where('organization_id',Auth::user()->organization_id)->get();

        Excel::create('Employees', function($excel) use($bank_data, $bankbranch_data, $branch_data,
            $department_data, $employeetype_data, $jobgroup_data,$employees, $data) {


            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");

            $excel->sheet('employees', function($sheet) use($bank_data, $bankbranch_data, $branch_data,
                $department_data, $employeetype_data, $jobgroup_data, $data, $employees){

                $sheet->row(1, array('EMPLOYMENT NUMBER','FIRST NAME','SURNAME',
                    'OTHER NAMES',
                    'ID NUMBER',
                    'KRA PIN',
                    'NSSF NUMBER',
                    'NHIF NUMBER',
                    'EMAIL ADDRESS','BASIC PAY'));


                $empdata = array();

                foreach($employees as $d){

                    $empdata[] = $d->personal_file_number.':'.$d->first_name.' '.$d->last_name.' '.$d->middle_name;
                }

                $emplist = implode(", ", $empdata);



                $listdata = array();

                foreach($data as $d){

                    $listdata[] = $d->allowance_name;
                }

                $list = implode(", ", $listdata);
            });
        })->export('xls');
    });

    /*
    *allowance template
    *
    */

    Route::get('template/allowances', function(){

        $data = Allowance::where('organization_id',Auth::user()->organization_id)->get();
        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();


        Excel::create('Allowances', function($excel) use($data, $employees) {

            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");



            $excel->sheet('allowances', function($sheet) use($data, $employees){


                $sheet->row(1, array(
                    'EMPLOYEE', 'ALLOWANCE TYPE', 'FORMULAR', 'INSTALMENTS','AMOUNT','ALLOWANCE DATE',
                ));

                $sheet->setWidth(array(
                    'A'     =>  30,
                    'B'     =>  30,
                    'C'     =>  30,
                    'D'     =>  30,
                    'E'     =>  30,
                    'F'     =>  30,
                ));

                $sheet->getStyle('F2:F1000')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');



                $row = 2;
                $r = 2;

                for($i = 0; $i<count($employees); $i++){

                    $sheet->SetCellValue("YY".$row, $employees[$i]->personal_file_number." : ".$employees[$i]->first_name.' '.$employees[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(new \PHPExcel_NamedRange('names', $sheet, 'YY2:YY'.(count($employees)+1)));



                for($i = 0; $i<count($data); $i++){

                    $sheet->SetCellValue("YZ".$r, $data[$i]->allowance_name);
                    $r++;
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'allowances', $sheet, 'YZ2:YZ'.(count($data)+1)));


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('allowances'); //note this!

                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                    $objValidation = $sheet->getCell('C'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"One Time, Recurring, Instalments"'); //note this!
                }

            });

        })->export('xlsx');



    });

    /*
    *deduction template
    *
    */

    Route::get('template/deductions', function(){

        $data = Deduction::where('organization_id',Auth::user()->organization_id)->get();
        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();


        Excel::create('Deductions', function($excel) use($data, $employees) {

            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");



            $excel->sheet('deductions', function($sheet) use($data, $employees){


                $sheet->row(1, array(
                    'EMPLOYEE', 'DEDUCTION TYPE', 'FORMULAR','INSTALMENTS','AMOUNT','DATE'
                ));


                $sheet->setWidth(array(
                    'A'     =>  30,
                    'B'     =>  30,
                    'C'     =>  30,
                    'D'     =>  30,
                    'E'     =>  30,
                    'F'     =>  30,
                ));

                $sheet->getStyle('F2:F1000')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');

                $row = 2;
                $r = 2;

                for($i = 0; $i<count($employees); $i++){

                    $sheet->SetCellValue("YY".$row, $employees[$i]->personal_file_number." : ".$employees[$i]->first_name.' '.$employees[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'names', $sheet, 'YY2:YY'.(count($employees)+1)
                    )
                );



                for($i = 0; $i<count($data); $i++){

                    $sheet->SetCellValue("YZ".$r, $data[$i]->deduction_name);
                    $r++;
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'deductions', $sheet, 'YZ2:YZ'.(count($data)+1)
                    )
                );


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('deductions'); //note this!



                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                    $objValidation = $sheet->getCell('C'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"One Time, Recurring, Instalments"');

                }
            });
        })->export('xlsx');



    });

    /*
    *earning template
    *
    */

    Route::get('template/earnings', function(){
        $data = Employee::where('organization_id',Auth::user()->organization_id)->get();

        Excel::create('Earnings', function($excel) use($data) {
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");



            $excel->sheet('Earnings', function($sheet) use($data) {

                $sheet->row(1, array(
                    ['EMPLOYEE', 'EARNING TYPE','NARRATIVE', 'FORMULAR', 'INSTALMENTS','AMOUNT','EARNING DATE',]
                ));

                $sheet->setWidth(array(
                    ['A'     =>  30,
                    'B'     =>  30,
                    'C'     =>  30,
                    'D'     =>  30,
                    'E'     =>  30,
                    'F'     =>  30,
                    'G'     =>  30,]
                ));

                $sheet->getStyle('G2:G1000')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');

                $row = 2;

                for($i = 0; $i<count($data); $i++){

                    $sheet->SetCellValue("ZZ".$row, $data[$i]->personal_file_number." : ".$data[$i]->first_name.' '.$data[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(
                    new PHPExcel_NamedRange(
                        'names', $sheet, 'ZZ2:ZZ'.(count($data)+1)
                    )
                );

                $objPHPExcel = new PHPExcel;
                $objSheet = $objPHPExcel->getActiveSheet();

                $objSheet->protectCells('ZZ2:ZZ'.(count($data)+1), 'PHP');

                $objSheet->getStyle('G2:G1000')->getNumberFormat()->setFormatCode('yyyy-mm-dd');


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"Bonus, Commission, Others"'); //note this!

                    $objValidation = $sheet->getCell('D'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"One Time, Recurring, Instalments"'); //note this!
                }
            });
        })->download("xlsx");

    });

    /*
    *Relief template
    *
    */

    Route::get('template/reliefs', function(){

        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();

        $data = Relief::where('organization_id',Auth::user()->organization_id)->get();

        Excel::create('Reliefs', function($excel) use($employees, $data) {

            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");



            $excel->sheet('reliefs', function($sheet) use($employees, $data){
                $sheet->row(1, array( 'EMPLOYEE', 'RELIEF TYPE', 'AMOUNT'));


                $sheet->setWidth(array( 'A' =>  30,'B'  =>  30,'C' =>  30,));

                $row = 2;
                $r = 2;

                for($i = 0; $i<count($employees); $i++){
                    $sheet->SetCellValue("YY".$row, $employees[$i]->personal_file_number." : ".$employees[$i]->first_name.' '.$employees[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'names', $sheet, 'YY2:YY'.(count($employees)+1)
                    ));



                for($i = 0; $i<count($data); $i++){
                    $sheet->SetCellValue("YZ".$r, $data[$i]->relief_name);
                    $r++;
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'reliefs', $sheet, 'YZ2:YZ'.(count($data)+1)
                    ));


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('reliefs'); //note this!



                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                }






            });

        })->export('xlsx');



    });

    
    /* #################### IMPORT EMPLOYEES ################################## */

    Route::post('import/employees', function(){

        if(Input::hasFile('employees')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('employees')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            Input::file('employees')->move($destination, $file);

            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();
                $organization = Organization::find(Auth::user()->organization_id);

                $cres = count($results);
                $cemp = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->count();
                $limit = $organization->payroll_licensed;


                if($limit<$cres){
                    return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                } else if($limit<($cres+$cemp)){
                    return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                }else{

                    foreach ($results as $result) {

                        $employee = new Employee;

                        $employee->personal_file_number = $result->employment_number;

                        $employee->first_name = $result->first_name;
                        $employee->last_name = $result->surname;
                        $employee->middle_name = $result->other_names;
                        $employee->identity_number = $result->id_number;
                        $employee->pin = $result->kra_pin;
                        $employee->social_security_number = $result->nssf_number;
                        $employee->hospital_insurance_number = $result->nhif_number;
                        $employee->email_office = $result->email_address;
                        $employee->basic_pay = str_replace( ',', '', $result->basic_pay);
                        $employee->organization_id = Auth::user()->organization_id;
                        $employee->save();

                    }
                }

            });

        }

     return Redirect::back()->with('notice', 'Employees have been succeffully imported');
    });
    
    /* #################### IMPORT TRIAL EMPLOYEES ################################## */

    Route::post('import/trialemployees', function(){


        if(Input::hasFile('trialemployees')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('trialemployees')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;

            Input::file('trialemployees')->move($destination, $file);

            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();
                $organization = Organization::find(Auth::user()->organization_id);

                $cres = count($results);
                $cemp = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->count();
                $limit = $organization->payroll_licensed;

                /*
                if($limit<$cres){
                  return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                } else if($limit<($cres+$cemp)){
                    return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                }else{*/

                $x = 1;
                if(count($results)>0){
                    foreach ($results as $result) {
                        $emp_number=$result->employment_number; if(!isset($emp_number)){ continue;}//$emp_number=1;}
                        $employee = new Employee;

                        $employee->personal_file_number = $emp_number;

                        $employee->first_name = 'firstname '.$x;
                        $employee->last_name = 'lastname '.$x;
                        $employee->middle_name = 'middlename '.$x;
                        $employee->identity_number = $x;
                        $employee->branch_id = 1;
                        $employee->department_id = 1;
                        $employee->email_office = 'email'.$x.'@lixnet.net';
                        $employee->basic_pay = str_replace( ',', '', $result->basic_pay);
                        $employee->organization_id = Auth::user()->organization_id;
                        $employee->save();
                    }
                    $travelallowance = Allowance::where('allowance_name','Travel Allowance')->first();
                    $actingallowance = Allowance::where('allowance_name','Acting Allowance')->first();
                    $otherallowance  = Allowance::where('allowance_name','Other Allowance')->first();

                    if($result->travel_allowance != ''){

                        $allowance = new EAllowances;

                        $allowance->employee_id = $employee->id;

                        $allowance->allowance_id = $travelallowance->id;

                        $allowance->allowance_amount = str_replace( ',', '', $result->travel_allowance);

                        $allowance->save();
                    }

                    if($result->no >= 1 && $result->no <= 72){

                        $pension = new Pension;

                        $pension->employee_id = $employee->id;
                        $pension->employee_contribution=str_replace(",","",$result->basic_pay) * 0.1;
                        $pension->employer_contribution=str_replace(",","",$result->basic_pay) * 0.1;
                        $pension->employee_percentage=10;
                        $pension->employer_percentage=10;
                        $pension->type='Percentage';
                        $pension->save();
                    }


                    if($result->other_allowance != ''){

                        $allowance = new EAllowances;

                        $allowance->employee_id = $employee->id;

                        $allowance->allowance_id = $otherallowance->id;

                        $allowance->allowance_amount = str_replace( ',', '', $result->other_allowance);

                        $allowance->save();
                    }

                    if($result->acting_allowance != ''){
                        $allowance = new EAllowances;

                        $allowance->employee_id = $employee->id;

                        $allowance->allowance_id = $actingallowance->id;

                        $allowance->allowance_amount = str_replace( ',', '', $result->acting_allowance);

                        $allowance->save();
                    }


                    $deductionid = DB::table('deductions')->where('deduction_name', '=', 'Arrears')->pluck('id');

                    if($result->arrears != ''){

                        $deduction = new EDeduction;

                        $deduction->employee_id = $employee->id;

                        $deduction->deduction_id = $deductionid;

                        $deduction->deduction_amount = str_replace( ',', '', $result->arrears);

                        $deduction->deduction_date = date('Y-m-d');

                        $deduction->save();
                    }



                    $x++;

                }
                //}

            });

        }

        return Redirect::back()->with('notice', 'Employees have been succeffully imported');

    });

    /* #################### IMPORT EARNINGS ################################## */

    Route::post('import/earnings', function(){


        if(Input::hasFile('earnings')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('earnings')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            Input::file('earnings')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();


                foreach ($results as $result) {

                    if($result->employee != null){


                        $name = explode(' : ', $result->employee);



                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');


                        $earning = new Earnings;

                        $earning->employee_id = $employeeid;

                        $earning->earnings_name = $result->earning_type;

                        $earning->narrative = $result->narrative;

                        $earning->formular = $result->formular;



                        if($result->formular == 'Instalments'){
                            $earning->instalments = $result->instalments;
                            $insts = $result->instalments;

                            $a = str_replace( ',', '',$result->amount);
                            $earning->earnings_amount = $a;

                            $earning->earning_date = $result->earning_date;

                            $effectiveDate = date('Y-m-d', strtotime("+".($insts-1)." months", strtotime($result->earning_date)));

                            $First  = date('Y-m-01', strtotime($result->earning_date));
                            $Last   = date('Y-m-t', strtotime($effectiveDate));

                            $earning->first_day_month = $First;

                            $earning->last_day_month = $Last;

                        }else{
                            $earning->instalments = '1';
                            $a = str_replace( ',', '', $result->amount );
                            $earning->earnings_amount = $a;

                            $earning->earning_date = $result->earning_date;

                            $First  = date('Y-m-01', strtotime($result->earning_date));
                            $Last   = date('Y-m-t', strtotime($result->earning_date));


                            $earning->first_day_month = $First;

                            $earning->last_day_month = $Last;

                        }


                        $earning->save();


                    }
                }
            });
        }

        return Redirect::back()->with('notice', 'earnings have been successfully imported');

    });

     /* #################### IMPORT RELIEFS ################################## */

    Route::post('import/reliefs', function(){


        if(Input::hasFile('reliefs')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('reliefs')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            Input::file('reliefs')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {
                    if($result->employee != null){

                        $name = explode(':', $result->employee);


                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');

                        $reliefid = DB::table('relief')->where('relief_name', '=', $result->relief_type)->pluck('id');

                        $relief = new ERelief;

                        $relief->employee_id = $employeeid;

                        $relief->relief_id = $reliefid;

                        $relief->relief_amount = $result->amount;

                        $relief->save();

                    }

                }
            });
        }

        return Redirect::back()->with('notice', 'reliefs have been succeffully imported');

    });

    /* #################### IMPORT ALLOWANCES ################################## */
    Route::post('import/allowances', function(){


        if(Input::hasFile('allowances')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('allowances')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            Input::file('allowances')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {

                    if($result->employee != null){

                        $name = explode(':', $result->employee);

                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');

                        $allowanceid = DB::table('allowances')->where('allowance_name', '=', $result->allowance_type)->pluck('id');

                        $allowance = new EAllowances;

                        $allowance->employee_id = $employeeid;

                        $allowance->allowance_id = $allowanceid;

                        $allowance->formular = $result->formular;



                        if($result->formular == 'Instalments'){
                            $allowance->instalments = $result->instalments;
                            $insts = $result->instalments;

                            $a = str_replace( ',', '',$result->amount);
                            $allowance->allowance_amount = $a;

                            $allowance->allowance_date = $result->allowance_date;

                            $effectiveDate = date('Y-m-d', strtotime("+".($insts-1)." months", strtotime($result->allowance_date)));

                            $First  = date('Y-m-01', strtotime($result->allowance_date));
                            $Last   = date('Y-m-t', strtotime($effectiveDate));

                            $allowance->first_day_month = $First;

                            $allowance->last_day_month = $Last;

                        }else{
                            $allowance->instalments = '1';
                            $a = str_replace( ',', '', $result->amount );
                            $allowance->allowance_amount = $a;

                            $allowance->allowance_date = $result->allowance_date;

                            $First  = date('Y-m-01', strtotime($result->allowance_date));
                            $Last   = date('Y-m-t', strtotime($result->allowance_date));


                            $allowance->first_day_month = $First;

                            $allowance->last_day_month = $Last;

                        }

                        $allowance->save();

                    }

                }
            });
        }

        return Redirect::back()->with('notice', 'allowances have been succefully imported');

    });


    /* #################### IMPORT DEDUCTIONS ################################## */
    Route::post('import/deductions', function(){


        if(Input::hasFile('deductions')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('deductions')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;

            Input::file('deductions')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {

                    if($result->employee != null){


                        $name = explode(':', $result->employee);

                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');

                        $deductionid = DB::table('deductions')->where('deduction_name', '=', $result->deduction_type)->pluck('id');

                        $deduction = new EDeduction;

                        $deduction->employee_id = $employeeid;

                        $deduction->deduction_id = $deductionid;

                        $deduction->formular = $result->formular;

                        $a = str_replace( ',', '', $result->amount );
                        $deduction->deduction_amount = $a;

                        $deduction->deduction_date = $result->date;

                        if($result->formular == 'Instalments'){
                            $deduction->instalments = $result->instalments;
                            $insts = $result->instalments;

                            $effectiveDate = date('Y-m-d', strtotime("+".($insts-1)." months", strtotime($result->date)));

                            $First  = date('Y-m-01', strtotime($result->date));
                            $Last   = date('Y-m-t', strtotime($effectiveDate));

                            $deduction->first_day_month = $First;

                            $deduction->last_day_month = $Last;

                        }else{
                            $deduction->instalments = '1';

                            $First  = date('Y-m-01', strtotime($result->date));
                            $Last   = date('Y-m-t', strtotime($result->date));


                            $deduction->first_day_month = $First;

                            $deduction->last_day_month = $Last;

                        }

                        $deduction->save();

                    }

                }


            });

        }

        return Redirect::back()->with('notice', 'deductions have been succefully imported');
    });

    /* #################### IMPORT BANK BRANCHES ################################## */
    Route::post('import/bankBranches', function(){


        if(Input::hasFile('bbranches')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('bbranches')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            Input::file('bbranches')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {
                    $branch_code=$result->branch_code; $branch_name=$result->branch_name;
                    if(!isset($branch_code) || !isset($branch_name)){continue;}

                    $bbranch = new BBranch;

                    $bbranch->branch_code = $result->branch_code;

                    $bbranch->bank_branch_name = $result->branch_name;

                    $bbranch->bank_id = $result->bank_id;

                    $bbranch->organization_id = $result->organization_id;

                    $bbranch->save();

                }

            });

        }
        return Redirect::back()->with('notice', 'bank branches have been succefully imported');

    });

    /* #################### IMPORT BANKS ################################## */

    Route::post('import/banks', function(){


        if(Input::hasFile('banks')){

            $destination = public_path().'/migrations/';

            $filename = str_random(12);

            $ext = Input::file('banks')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            Input::file('banks')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {
                    $bnk_name=$result->bank_name; if(!isset($bnk_name) || empty($bnk_name)){continue;}
                    $bank = new Bank;

                    $bank->bank_name = $result->bank_name;

                    $bank->bank_code = $result->bank_code;

                    $bank->organization_id = $result->organization_id;

                    $bank->save();

                }

            });

        }

        return Redirect::back()->with('notice', 'banks have been succefully imported');

    });
});


Route::get('reports/employees', array('middleware' => 'loggedin', function(){
  
    return View::make('reports');
}));

Route::get('hrdashboard', array('middleware' => 'loggedin', function(){
    $employees = Employee::getActiveEmployee();
    return View::make('hrdashboard',compact('employees'));
  }));

Route::get('payrolldashboard', array('middleware' => 'loggedin', function(){
    return View::make('payrolldashboard');
  }));




 /***MEMBER ROUTES  */
Route::resource('members', 'MembersController');
Route::post('members/update/{id}', 'MembersController@update');
Route::post('member/update/{id}', 'MembersController@update');
Route::get('members/delete/{id}', 'MembersController@destroy');
Route::get('members/edit/{id}', 'MembersController@edit');
Route::get('member/edit/{id}', 'MembersController@edit');
Route::post('createbank', 'MembersController@createbank');
Route::post('createbankbranch', 'MembersController@createbankbranch');
Route::get('members/show/{id}', 'MembersController@show');
Route::get('members/summary/{id}', 'MembersController@summary');
Route::get('member/show/{id}', 'MembersController@show');
Route::post('deldoc', 'MembersController@deletedoc');
Route::get('members/loanaccounts/{id}', 'MembersController@loanaccounts');
Route::get('memberloans', 'MembersController@loanaccounts2');

Route::get('language/{lang}',
    array(
        'as' => 'language.select',
        'uses' => 'OrganizationsController@language'
    )
);

//============ MEMBER CONFIG ROUTES ===============================//
Route::get('member_config', 'MembersController@config');
Route::post('member_config', 'MembersController@configure');
Route::get('members/loanhistory/{id}', 'MembersController@loanHistory');
Route::get('members/loanhistory/{id}/report', 'MembersController@loanHistoryReport');
//==================END MEMBER CONFIG ROUTES ==========================================//

//=============== SACCO INVESTMENT ROUTES =======================================================//
Route::get('portal', function () {
    $members = Member::all();
    return View::make('css.members', compact('members'));
});

Route::get('saccoinvestments', function () {
    $investment = Investment::all();
    //Calculate the valuationda
    foreach ($investment as $invest) {
        Valuation::calculateValuation($invest->id);
    }
    $organization = Organization::find(1);
    return View::make('css.saccoinvestments', compact('investment', 'organization'));
});

Route::get('saccoinvestments/create', function () {
    $vendor = Vendor::all();
    $organization = Organization::find(1);
    $cats = Investmentcategory::all();
    return View::make('css.createinvestment', compact('vendor', 'organization', 'cats'));
});
#TODO: Move to controller
Route::post('saccoinvestments/create', function () {
    $data = Input::all();
    $invest = new Investment;
    $invest->name = $data['investment'];
    $invest->vendor_id = $data['vendor'];
    $invest->valuation = $data['valuation'];
    $invest->growth_type = $data['growth_type'];
    $invest->growth_rate = $data['growth_rate'];
    $invest->description = $data['desc'];
    $invest->category_id = $data['category'];
    $invest->date = date('Y-m-d');
    $invest->save();
    $out = "The sacco Investment successfully created!!!";
    $investment = Investment::all();
    $organization = Organization::find(1);
    return View::make('css.saccoinvestments', compact('out', 'investment', 'organization'));
});

Route::get('saccoinvestments/edit/{id}', 'InvestmentController@update');
Route::post('saccoinvestments/edit', 'InvestmentController@doupdate');
Route::get('saccoinvestments/delete/{id}', 'InvestmentController@destroy');

Route::get('portal/activate/{id}', 'MembersController@activateportal');
Route::get('portal/deactivate/{id}', 'MembersController@deactivateportal');

Route::get('memtransactions/{id}', 'MembersController@savingtransactions');
//=============== END SACCO INVESTMENT ROUTES ===========================//

/**
 * Saving account routes
 */
Route::resource('savingaccounts', 'SavingaccountsController');
Route::get('savingaccounts/create/{id}', 'SavingaccountsController@create');
Route::get('member/savingaccounts/{id}', 'SavingaccountsController@memberaccounts');


/**
 * Saving transaction routes
 */
Route::get('savingtransactions/show/{id}', 'SavingtransactionsController@show');
Route::resource('savingtransactions', 'SavingtransactionsController');
Route::get('savingtransactions/create/{id}', 'SavingtransactionsController@create');
Route::get('savingtransactions/receipt/{id}', 'SavingtransactionsController@receipt');
Route::get('savingtransactions/statement/{id}', 'SavingtransactionsController@statement');
Route::post('savingtransactions/import', 'SavingtransactionsController@import');

/**
 * Shares routes
 */
Route::resource('shares', 'SharesController');
Route::post('shares/update/{id}', 'SharesController@update');
Route::get('shares/delete/{id}', 'SharesController@destroy');
Route::get('shares/edit/{id}', 'SharesController@edit');
Route::get('shares/show/{id}', 'SharesController@show');

/**
* share transaction routes
*/
Route::get('sharetransactions/show/{id}', 'SharetransactionsController@show');
Route::resource('sharetransactions', 'SharetransactionsController');
Route::get('sharetransactions/create/{id}', 'SharetransactionsController@create');

//==============SAVING PRODUCT ROUTES =============================================//
Route::resource('savingproducts', 'SavingproductsController');
Route::get('savingproducts/update/{id}', 'SavingproductsController@selectproduct');
Route::post('savingproducts/update', 'SavingproductsController@update');
Route::get('savingproducts/delete/{id}', 'SavingproductsController@destroy');
Route::get('savingproducts/edit/{id}', 'SavingproductsController@edit');
Route::get('savingproducts/show/{id}', 'SavingproductsController@show');
//=========== SAVING PRODUCT  ROUTES ==================================//


/*
* Vehicles Routes
*/
Route::resource('vehicles', 'VehiclesController');
Route::post('vehicles/update/{id}', 'VehiclesController@update');
Route::get('vehicles/delete/{id}', 'VehiclesController@destroy');
Route::get('vehicles/edit/{id}', 'VehiclesController@edit');
Route::get('vehicles/show/{id}', 'VehiclesController@show');
Route::get('vehicles/create', 'VehiclesController@create');

/**##########ASSIGN VEHICLE ############## */
Route::resource('assignvehicles', 'AssignVehiclesController');
Route::post('assignvehicles/update/{id}', 'AssignVehiclesController@update');
Route::get('assignvehicles/delete/{id}', 'AssignVehiclesController@destroy');
Route::get('assignvehicles/edit/{id}', 'AssignVehiclesController@edit');
Route::get('assignvehicles/show/{id}', 'AssignVehiclesController@show');
Route::get('assignvehicles/create', 'AssignVehiclesController@create');

/**##########VEHICLE INCOME ############## */

Route::resource('vehicleincomes', 'VehicleIncomesController');
Route::post('vehicleincomes/update/{id}', 'VehicleIncomesController@update');
Route::get('vehicleincomes/delete/{id}', 'VehicleIncomesController@destroy');
Route::get('vehicleincomes/edit/{id}', 'VehicleIncomesController@edit');
Route::get('vehicleincomes/show/{id}', 'VehicleIncomesController@show');
Route::get('vehicleincomes/create', 'VehicleIncomesController@create');

/**########## VEHICLE  EXPENSES ############## */
Route::resource('vehicleexpenses', 'VehicleExpensesController');
Route::post('vehicleexpenses/update/{id}', 'VehicleExpensesController@update');
Route::get('vehicleexpenses/delete/{id}', 'VehicleExpensesController@destroy');
Route::get('vehicleexpenses/edit/{id}', 'VehicleExpensesController@edit');
Route::get('vehicleexpenses/show/{id}', 'VehicleExpensesController@show');
Route::get('vehicleexpenses/create', 'VehicleExpensesController@create');

/**
 * Tlb payment routes
 */
Route::resource('tlbpayments', 'TlbPaymentsController');
Route::get('tlbpayments/edit/{id}', 'TlbPaymentsController@edit');
Route::post('tlbpayments/update/{id}', 'TlbPaymentsController@update');
Route::get('tlbpayments/delete/{id}', 'TlbPaymentsController@destroy');
Route::get('tlbpayments/create', 'TlbPaymentsController@create');
Route::post('reports/tbl', 'ReportsController@tlbrep');

// ============= DISBURSEMENTS ROUTES ============================== //
Route::resource('disbursements', 'DisbursementController');
Route::get('disbursements/create', 'DisbursementController@create');
Route::post('disbursements/create', 'DisbursementController@docreate');
Route::get('disbursements/update/{id}', 'DisbursementController@update');
Route::post('disbursements/update', 'DisbursementController@doupdate');
Route::get('disbursements/delete/{id}', 'DisbursementController@destroy');
// ============= END  DISBURSEMENTS ROUTES ============================== //

// ================= matrices routes ================================= //
Route::resource('matrices', 'MatrixController');
Route::get('matrices/create', 'MatrixController@create');
Route::post('matrices/create', 'MatrixController@docreate');
Route::get('matrices/update/{id}', 'MatrixController@update');
Route::post('matrices/update', 'MatrixController@doupdate');
Route::get('matrices/delete/{id}', 'MatrixController@destroy');
// ================= end matrices routes ================================= //


//=============== LOAN PRODUCT ROUTES ================================ //
Route::resource('loanproducts', 'LoanproductsController');
Route::post('loanproducts/update/{id}', 'LoanproductsController@update');
Route::get('loanproducts/delete/{id}', 'LoanproductsController@destroy');
Route::get('loanproducts/edit/{id}', 'LoanproductsController@edit');
Route::get('loanproducts/show/{id}', 'LoanproductsController@show');
Route::get('memberloanshow/{id}', 'LoanproductsController@memberloanshow');
//=============== LOAN PRODUCT ROUTES ================================ //

// ================== LOAN ROUTES =================================//
Route::resource('loans', 'LoanaccountsController');
Route::get('loans/apply/{id}', 'LoanaccountsController@apply');
Route::get('guarantorapproval', 'LoanaccountsController@guarantor');
Route::post('loans/apply', 'LoanaccountsController@doapply');
Route::post('loans/application', 'LoanaccountsController@doapply2');
Route::get('loanduplicates', 'LoanaccountsController@management');
Route::get('ajaxfetchrduration', function () {
    $lpvalue= Input::get('lpvalue');
    $loanperiod = DB::table('loanproducts')
        ->where('id',$lpvalue)
        ->first();
    return $loanperiod->period;
});
Route::get('ajaxfetchguarantorbal', function () {
    $memberid=Input::get('lpvalue');
    #$member =  Member::where('id','=', $memberid)->first();
    //$savingsbal=Savingaccount::getUserSavingsBalance($memberid);
    /*$amountguaranteed=Loanaccount::amountGuarantee($memberid);
    $savingsbal=(int)$rawsavingsbal-(int)$amountguaranteed;*/
    $savingsbal=Savingaccount::getFinalDepositBalance($memberid);
    if($savingsbal<1){$savingsbal=0;}
    return round($savingsbal,2);
});
Route::get('loanfetch', function () {
    $principal_paid = Loanrepayment::getPrincipalPaid($loanaccount);
    $interest_paid = Loanrepayment::getInterestPaid($loanaccount);
    $amountpaid=(int)$principal_paid + (int)$interest_paid;
    return $amountpaid;
});
Route::get('ajaxfetchmaxamount', function () {
    $lpvalue= Input::get('lpvalue');
    $loanperiod = DB::table('loanproducts')
        ->where('id',$lpvalue)
        ->first();
    return $loanperiod->max_multiplier;
});
Route::get('loans/edit/{id}', 'LoanaccountsController@edit');
Route::get('loanmanagement', 'LoanaccountsController@index');
Route::post('loans/update/{id}', 'LoanaccountsController@update');

Route::get('loans/approve/{id}', 'LoanaccountsController@approve');
Route::post('loans/approve/{id}', 'LoanaccountsController@doapprove');
Route::post('gurantorapprove/{id}', 'LoanaccountsController@guarantorapprove');
Route::post('gurantorreject/{id}', 'LoanaccountsController@guarantorreject');

Route::get('loans/reject/{id}', 'LoanaccountsController@reject');
Route::post('rejectapplication', 'LoanaccountsController@rejectapplication');

Route::get('loans/disburse/{id}', 'LoanaccountsController@disburse');
Route::post('loans/disburse/{id}', 'LoanaccountsController@dodisburse');

Route::get('loans/show/{id}', 'LoanaccountsController@show');
Route::post('loans/update-repayment-period/{id}', 'LoanaccountsController@updateRepaymentPeriod');
Route::get('memberloan/show/{id}', 'LoanaccountsController@show');

Route::post('loans/amend/{id}', 'LoanaccountsController@amend');

Route::get('loans/reject/{id}', 'LoanaccountsController@reject');
Route::post('loans/reject/{id}', 'LoanaccountsController@rejectapplication');


Route::get('loanaccounts/topup/{id}', 'LoanaccountsController@gettopup');
Route::post('loanaccounts/topup/{id}', 'LoanaccountsController@topup');

Route::get('memloans/{id}', 'LoanaccountsController@show2');

// ================== END LOAN  ROUTES =================================//

//==============LOAN REPAYMENT ROUTES ========================= //
Route::resource('loanrepayments', 'LoanrepaymentsController');

Route::get('loanrepayments/create/{id}', 'LoanrepaymentsController@create');
Route::get('memberloanrepayments/create/{id}', 'LoanrepaymentsController@create');
Route::get('loanrepayments/offset/{id}', 'LoanrepaymentsController@offset');
Route::get('repayments_template', 'LoanrepaymentsController@createTemplate');
Route::get('import_repayments', 'LoanrepaymentsController@importView');
Route::post('import_repayments', 'LoanrepaymentsController@importRepayment');
//============== END LOAN REPAYMENT ROUTES =========================
