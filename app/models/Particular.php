<?php namespace App;
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/2/2018
 * Time: 2:01 PM
 */
use Illuminate\Database\Eloquent\Model;

class Particular extends Model
{
public function debitAccount(){
  return $this->belongsTo("Account", "debitaccount_id", "id");
}

public function creditAccount(){
  return $this->belongsTo("Account", "creditaccount_id", "id");
}
}
