<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Tlbpayment extends Model {

	// Add your validation rules here
	public static $rules = [
		 'amount' => 'required',
		 'eq_id' => 'required',
		 'date' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function account(){

		return $this->belongsTo('App\Account');
	}

	public function vehicle(){

		return $this->belongsTo('App\Vehicle');
	}

}