<?php namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Zizaco\Confide\Confide;

class Loanproduct extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function loanpostings(){

		return $this->hasMany('App\Loanposting');
	}


	public function loanaccounts(){

		return $this->hasMany('App\Loanaccount');
	}


	public function charges(){

		return $this->belongsToMany('App\Charge');
	}








	public static function submit($data){


		//$charges = Input::get('charge');

		$loanproduct = new Loanproduct;
		$membership_duration=array_get($data, 'membershipduration');
		if(empty($membership_duration)){$membership_duration=0;}

		$loanproduct->name = array_get($data, 'name');
		$loanproduct->short_name = array_get($data, 'short_name');
		$loanproduct->interest_rate = array_get($data, 'interest_rate');
		$loanproduct->formula = array_get($data, 'formula');
		$loanproduct->amortization = array_get($data, 'amortization');
		$loanproduct->currency = array_get($data, 'currency');
		$loanproduct->period = array_get($data, 'period');
		$loanproduct->auto_loan_limit = array_get($data, 'autoloanlimit');
		$loanproduct->application_form = array_get($data, 'appform');
		$loanproduct->membership_duration=$membership_duration;
		$loanproduct->max_multiplier=3;
		$loanproduct->save();

		Audit::logAudit(date('Y-m-d'), Auth::user()->username, 'loan product creation', 'Loans', '0');


		$loan_id = $loanproduct->id;

		Loanposting::submit($loan_id, $data);
		

		/*
		foreach($charges as $charge){

			$loanproduct->charges()->attach($charge);
		}

		*/

	}

}