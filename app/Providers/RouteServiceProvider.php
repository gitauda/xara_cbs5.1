<?php

namespace App\Providers;

use App\Member;
use App\Organization;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

use Zizaco\Entrust\Entrust;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		parent::boot($router);
		
		$router->filter('limit', function(){

			$organization = Organization::find(1);
		
		
			$members = count(Member::all());
		
			if($organization->licensed <= $members){
		
				return View::make('members.memberlimit');
			}
		
		});
		
		
		$router->filter('license', function(){
		
		$organization = Organization::find(1);
		
		$string = $organization->name;
		$license_key =$organization->license_key;
		$license_code = $organization->license_code;
		
		$validate = $organization->license_key_validator($license_key,$license_code,$string);
		
		if($validate){
		
			return View::make('activate', compact('organization'))->withErrors('License activation failed. License Key not valid');
		
		
			}
		
		});
		
		/*
		|--------------------------------------------------------------------------
		| Authentication Filters
		|--------------------------------------------------------------------------
		|
		| The following filters are used to verify that the user of the current
		| session is logged into this application. The "basic" filter easily
		| integrates HTTP Basic authentication for quick, simple checking.
		|
		*/
		
		$router->filter('process_payroll', function()
		{
		
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('process_payroll') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('manage_earning', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_earning') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('manage_deduction', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_deduction') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('manage_allowance', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_allowance') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		
		$router->filter('view_application', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('view_applications') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		
		$router->filter('amend_application', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('amend_application') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('reject_application', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('reject_application') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});

		$router->filter('leave_mgmt', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('view_application') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('manage_login', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }else{
				return 'mntgodn';
			 }
		});
		
		$router->filter('create_employee', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('create_employee') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});

		$router->filter('manage_organization', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_organization') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('manage_branch', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_branch') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('manage_group', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_group') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		$router->filter('manage_settings', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_settings') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		
		$router->filter('manage_users', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (! Entrust::can('manage_user') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});

		$router->filter('manage_roles', function()
		{
			if (! Entrust::can('manage_role') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});

		$router->filter('manage_leavetypes', function()
		{
			if (!Auth::user())
			{
			   $sessionTimeout = 1;
			   $organization = Organization::find(1);
			   return View::make('login',compact('organization'));
			 }
			else if (!Entrust::can('manage_leave') ) // Checks the current user
			{
				return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Contact your system admin');
			}
		});
		
		

		$router->filter('payment_access_restriction', function()
		{
			$organization = Organization::find(1);
		
				$license_key = $organization->annual_support_key;
		
		
				$valid = $organization->annual_key_validator($license_key, $organization->license_code, $organization->name);
		
		
				if(!$valid){
				
					return Redirect::to('dashboard')->with('notice', 'you do not have access to this resource. Check Billing Information or Contact your system admin');
		
				}
		
		});
		
		
		/* Custom filter */
		$router->filter('loggedin', function () {
			if (!Auth::user()) {
				$sessionTimeout = 1;
				$organization = Organization::find(1);
				return View::make('login', compact('organization'));
			}
		});

        
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
